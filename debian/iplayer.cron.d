#
# Regular cron jobs for the iplayer package
#
# Expected way is to use iSchedule to plan jobs.
# But some task may required to be launched in an autonomous way. For example
# package upgrade is restarting the service. If iSchedule is restarted during
# an upgrade, the postinst script that is executed to restart the service will
# be stopped and will leave the package in half-configured state.
#
PATH=/sbin:/bin:/usr/sbin:/usr/bin
*/15 * * * *    player   sudo apt-get update && sudo apt-get dist-upgrade -y && /usr/bin/iSynchro --plugins && /usr/bin/iSynchro --commands

# Quick rotation in case of unexpected logs verbosity
*/5 * * * *     root    /usr/sbin/logrotate /etc/logrotate.conf
