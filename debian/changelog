ipreso.iplayer (1.3.0-1~rc2) unstable; urgency=medium

  * Do not manage time synchornization anymore. Everything is now
    managed by the ihardwareconfiguration package.
  * Using new separated hwclock scripts

 -- Julien Clariond <julien.clariond@viveris.fr>  Mon, 16 Dec 2019 10:12:31 +0000

ipreso.iplayer (1.2.1-1) unstable; urgency=medium

  * Allow upgrades with dependencies changes (apt-get dist-upgrade).
  * Force usage of iPreso 'stable' repository.

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Fri, 17 Jan 2020 14:56:39 +0000

ipreso.iplayer (1.2.0-1) unstable; urgency=medium

  * Forget the HDMI Audio output (is now managed by iplayer-audio package).
  * Fix shutdown by not restarting automatic login of TTY.

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Fri, 19 Jul 2019 16:16:19 +0000

ipreso.iplayer (1.2.0-1~beta76) unstable; urgency=medium

  * Select HDMI Audio output when available.
  * Do not upgrade package at iplayer start, otherwise upgrade is failing 
    (iSchedule is killed as its apt-get children).

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Sun, 30 Jun 2019 15:18:18 +0000

ipreso.iplayer (1.2.0-1~beta75) unstable; urgency=medium

  * Execute scripts of plugins on iPlayer's upgrade.
  * Rotate logs when they're reaching 50MB.

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Sat, 23 Feb 2019 14:18:19 +0000

ipreso.iplayer (1.2.0-1~beta74) unstable; urgency=medium

  * Always run iSynchro as 'player', to fix license's owner issue.

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Wed, 13 Feb 2019 11:56:00 +0000

ipreso.iplayer (1.2.0-1~beta73) unstable; urgency=medium

  * Updated for Stretch distribution.
  * - Reviewed dependencies
  * - Use /sys/class/net/xxx/address instead of ifconfig to get MAC address
  * - Updated APT sources management (installer exclusively)
  * - Added dependency to rng-tools, to speed up boot process
  * Use new APT repositories (signed repo)
  * Use 'make install' to create package's filetree
  * Auto-detect network card at installation (can be enp1s0, eth0, ...)
  * Create iplayer-dev subpackage to put .h and .so for plugins generation.
    - Headers in /usr/include/iplayer
    - Libraries in /usr/lib/iplayer
  * Fixed our WindowManager for XOrg 64bits.
  * Updated plugins download management.
  * Updated commands download management.
  * Allow hostnames with hash.
  * Only download authenticated packages.
  * Use of rsyslog instead of syslog-ng.
  * Reviewed logrotate configuration.
  * Reviewed packaging with Lintian.
  * Created separated package for libiCommon library.
  * Build with Hardening options.
  * Use debhelper 10.
  * Use systemd.
  * Added preparation script
  * Run systemd service as user 'player'
  * Update of plugins is triggered with cron instead of iSchedule.
  * Fixed OpenSSL parsing with version >= 1.1.0.
  * Trust all CAs from /etc/napafilia/ssl, allowing CA renewing via 
    napafilia-keyring package.
  * Send iSchedule commands outputs to Syslog.
  * Added NVMe disks support
  * Get Composer's hostname from its certificate instead of HTTP query.
  * Fix files retrieval with new Napafilia's certificate.
  * Fix iPlayer startup when no connectivity available.
  * Close window when we cannot set the IPRESO_LAYER property.
  * iSynchro does not try to get out if there is no IP.
  * iLaunch waits for a ready X server before to really start.

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Sat, 25 Mar 2017 16:57:14 +0100

ipreso.iplayer (1.1.3) unstable; urgency=low

  * Fixed #0000403: EVOLUTION - GESTION DE LA PEREMPTION D'UN CONTENU
  * Fixed #0000618: Robustesse telechargements

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Thu, 21 Apr 2016 19:56:15 +0200

ipreso.iplayer (1.1.2) unstable; urgency=low

  * Fixed #0000523: Plusieurs upload d'un meme nom de fichier
  * Fixed #0000508: Affichage d'un Boot Splash de couleur sombre
  * Automatically adapt repository to the current distribution
  * Disabling zlib compression for Stunnel4 (used by syslog-ng)

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Wed, 07 May 2014 16:15:38 +0200

ipreso.iplayer (1.1.1) unstable; urgency=low

  * Fixed 0000483: Problème de lecture de flux RSS AX les thermes

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Fri, 03 Jan 2014 22:41:11 +0100

ipreso.iplayer (1.1.0-quiet) unstable; urgency=low

  * Fixed 0000451: Boot silently

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Thu, 12 Sep 2013 13:46:58 +0200

ipreso.iplayer (1.1.0) unstable; urgency=low

  * Add IPRESO_LAYER management to change windows layer
  * Added iPreso theme for Plymouth
  * Updated syslog-ng version number
  * Force plugins download after composer registration
  * Merging Stable fixes with the trunk
  * Updated dependencies with Wheezy versions

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Mon, 14 Jan 2013 10:47:48 +0100

ipreso.iplayer (1.0.17) unstable; urgency=low

  * Update initd script to not freeze on STOP when iPlayer is not running
  * Added robustness to iZone's loops
  * Force update of syslog-ng
  * Updated syslog-ng configuration for conf.d compliance
  * Updated syslog-ng configuration to catch iWatchdog logs
  * Force plugins download after composer registration

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Fri, 15 Mar 2013 16:00:59 +0100

ipreso.iplayer (1.0.16) unstable; urgency=low

  * Fixed 0000374: AMELIORATION - Watchdog
  * Fixed 0000382: DEFAUT - L'AFFICHAGE D'UN FLUX VIDEO NE SE FAIT PAS

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Thu, 03 Jan 2013 18:52:34 +0100

ipreso.iplayer (1.0.15) unstable; urgency=low

  * Fixed 0000368: DEFAUT - Démarrage de Box peut provoquer plusieurs lancements de iSchedule
  * Fixed 0000335: DEFAUT - Des zones de texte ne s'affichent pas sur le Player
  * Fixed 0000310: DEFAUT - Mauvaise gestion d'erreur sur Media corrompu
  * Fixed 0000259: Longueur de nom de programmation Limité à 32 caractères
  * Fixed 0000256: DEFAUT - Changement d'heure provoque des décalages d'1h dans les Programmations
  * Fixed 0000087: Install & configure syslog-ng to send players logs
  * Fixed multiple hash for Ibidum issuer (caused by openssl algo modification)
  * Added FQDN in /etc/hosts
  * Decreased number of log files

 -- Marc Simonetti <marc.simonetti@ipreso.com>  Thu, 29 Nov 2012 13:18:27 +0100

ipreso.iplayer (1.0.14) unstable; urgency=low

  * Fixed 0000254: DEFECT - IAPPCTRL IS INDEFINITELY WAITING SOME ITEMS

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 24 Mar 2012 14:57:34 +0100

ipreso.iplayer (1.0.13) unstable; urgency=low

  * Rotate all log files
  * Fixed 0000211: Licenses requests are sent over HTTP instead of HTTPS
  * Updated default Composer's IP
  * Partially fixed 0000238: DEFECT - VIDEOS ARE NOT DISPLAYED DURING LONG PERIODS OF TIME

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 27 Feb 2012 21:32:22 +0100

ipreso.iplayer (1.0.12) unstable; urgency=low

  * Fixed 0000088: Review logs criticities
  * Fixed 0000136: Update of the license when Composer is updated
  * Fixed 0000087: Install & configure syslog-ng to send player's logs

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 05 Nov 2011 12:08:13 +0100

ipreso.iplayer (1.0.11) unstable; urgency=low

  * Fixed 0000099: Corrupted Scheduler
  * Fixed 0000078: Display HASH value on TTY6 (on basic installation)

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 24 Jun 2011 17:08:28 +0200

ipreso.iplayer (1.0.10) unstable; urgency=low

  * Fixed 0000078: Display HASH value on TTY6
  * Fixed 0000099: Corrupted Scheduler
  * Fixed 0000115: Emissions are not chaining !
  * Partially fixed 0000088: Review logs criticities
  * Fixed composer's hostname on first boot
  * Moved first hostname request from iSynchro to iLaunch
  * Fixed command length overflow for wallpaper conversion

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 11 Jun 2011 13:12:48 +0200

ipreso.iplayer (1.0.9) unstable; urgency=low

  * Fixed 0000042: Emissions pool is not looping properly
  * Fixed 0000050: Convert iCommon from static to dynamic library
  * Fixed 0000052: Makefile should always have an up-to-date version
  * Fixed 0000066: -Can't open display- after two hours of emissions pool
  * Fixed 0000065: Update of emission during a pool is breaking the loop
  * Fixed 0000045: Player should automatically get Composer's hostname
  * Fixed 0000072: Background default image shall be customizable
  * Fixed iPlayer to manage large files (> 2GB)
  * Fixed 'ignore basic event' behaviour
  * Fixed infinite zones not taggued as infinite
  * Updated wallpaper setter to feh

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 31 May 2011 18:00:00 +0200

ipreso.iplayer (1.0.8) unstable; urgency=low

  * Fixed restart problems due to transition logo.
  * Updated installer:
      - TTY6 is asking for update of network configuration

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 14 May 2011 16:16:56 +0200

ipreso.iplayer (1.0.7) unstable; urgency=low

  * Fixed linking issues with new versions of ld.
  * Removed blind-kill of processes, useless since PGID is set to newly opened
    windows
  * Fixed 0000038: UNSTABILITY OF SHOW POOL

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 10 May 2011 18:16:39 +0200

ipreso.iplayer (1.0.6) unstable; urgency=low

  * Added Emissions pool functionality.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 31 Dec 2010 00:17:42 +0100

ipreso.iplayer (1.0.5) unstable; urgency=low

  * Do not download missing plugins.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 19 Dec 2010 19:21:00 +0100

ipreso.iplayer (1.0.4) unstable; urgency=low

  * Default composer for new players is demo.ipreso.com.
  * iSynchro --commands is executed following iSynchro --plugins

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 19 Dec 2010 18:13:58 +0100

ipreso.iplayer (1.0.3) unstable; urgency=low

  * Fixed postinst script to not parse commented lines.
  * Fixed trac ticket #218: Add Mii-tool in /etc/sudoers file.
  * Fixed trac ticket #217: iPlayer stop only when running

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 18 Dec 2010 15:17:36 +0100

ipreso.iplayer (1.0.2) unstable; urgency=low

  * Updated iplayer revision to 1.0.2.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 18 Dec 2010 11:24:04 +0100

ipreso.iplayer (1.0-2) unstable; urgency=low

  * Fixed typo on Dynamic IP configuration.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 17 Dec 2010 08:34:24 +0100

ipreso.iplayer (1.0-1) unstable; urgency=low

  * Fixed trac ticket #198: do not wait for DHCP reply when link is down.
  * iPreso 1.0, Release Candidate 1

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 15 Dec 2010 07:11:49 +0100

ipreso.iplayer (0.1.20101206) unstable; urgency=low

  * Fixed bug on license request.
  * Check hostname before launching iplayer.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 06 Dec 2010 12:04:32 +0100

ipreso.iplayer (0.1.20101205) unstable; urgency=low

  * Fixed parameters parsing (in playlist files).
  * Send origin (USB Stick, ...) and reason (expiration, renewing, ...) on 
    license requests.
  * Fixed errors when IP is given without gateway.
  * Added '--key' parameter for iWM module, allowing to send KeyPress events.
  * Force update of the license on decrypt errors.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 05 Dec 2010 12:32:18 +0100

ipreso.iplayer (0.1.20100909.2) unstable; urgency=low

  * Added screen output for "iSchedule --show" (used by Cleaner plugin)

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 09 Sep 2010 15:26:24 +0200

ipreso.iplayer (0.1.20100909) unstable; urgency=low

  * Reduced maximum time between two calendar checks.
  * Added transition logo.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 09 Sep 2010 09:43:49 +0200

ipreso.iplayer (0.1.20100614) unstable; urgency=low

  * Last emission is launched if nothing is played (according to the schedule)
    after 15s.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 14 Jun 2010 17:34:09 +0200

ipreso.iplayer (0.1.20100528) unstable; urgency=low

  * Updated revision.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 27 May 2010 16:09:44 +0200

ipreso.iplayer (0.1.20100525) unstable; urgency=low

  * Fixed bug in iSchedule component.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 25 May 2010 10:26:40 +0200

ipreso.iplayer (0.1.20100524) unstable; urgency=low

  * EXDATE parsing for Schedule files.
  * Fixed schedule parsing for complex programmations.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 24 May 2010 10:39:58 +0200

ipreso.iplayer (0.1.20100519) unstable; urgency=low

  * Check on installation if configuration files exist in /etc/ipreso.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 19 May 2010 11:03:28 +0200

ipreso.iplayer (0.1.20100517) unstable; urgency=low

  * Updated changelog with 0.1.20100517 revision

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 17 May 2010 14:45:25 +0200

ipreso.iplayer (0.0.20100402) unstable; urgency=low

  * Removed unused dependencies from iWM Makefile.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 02 Apr 2010 14:34:16 +0200

ipreso.iplayer (0.0.20100330) unstable; urgency=low

  * Added HW Clock sync when ntpdate is synchronizing.
  * iWM returns 'live' resolution, not a cached one.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 30 Mar 2010 12:17:35 +0200

ipreso.iplayer (0.0.20100326.2) unstable; urgency=low

  * Updated Splashy theme because of invalid theme.xml file.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 26 Mar 2010 17:26:08 +0100

ipreso.iplayer (0.0.20100326) unstable; urgency=low

  * Added Splashy theme to the package.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 26 Mar 2010 16:49:14 +0100

ipreso.iplayer (0.0.20100315) unstable; urgency=low

  * Removed Semaphores traces.
  * Manage live-change of composer's host and ip.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 16 Mar 2010 15:13:42 +0100

ipreso.iplayer (0.0.20100305.2) unstable; urgency=low

  * Fixed configuration file with new load-timeout.
  * Removed semaphores traces.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 05 Mar 2010 15:24:51 +0100

ipreso.iplayer (0.0.20100305.1) unstable; urgency=low

  * Fixed Trac Ticket #123 (strange window placement).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 05 Mar 2010 15:18:28 +0100

ipreso.iplayer (0.0.20100215.3) unstable; urgency=low

  * Keep DEBIAN_FRONTEND variable in sudo environment.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 15 Feb 2010 15:23:36 +0100

ipreso.iplayer (0.0.20100215.2) unstable; urgency=low

  * Non-interactive mode set in /etc/environment.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 15 Feb 2010 11:19:42 +0100

ipreso.iplayer (0.0.20100215) unstable; urgency=low

  * Force packages upgrade (ie libc6 dialog).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 15 Feb 2010 10:47:18 +0100

ipreso.iplayer (0.0.20100212.1) unstable; urgency=low

  * Added a command to force license update.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 12 Feb 2010 17:14:31 +0100

ipreso.iplayer (0.0.20100212) unstable; urgency=low

  * Ask license update when manifest file can't be read.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 12 Feb 2010 14:27:36 +0100

ipreso.iplayer (0.0.20100211) unstable; urgency=low

  * Updated iWM to force WID property to not-ready windows.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 11 Feb 2010 11:27:53 +0100

ipreso.iplayer (0.0.20100210.2) unstable; urgency=low

  * Removed debug traces.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 10 Feb 2010 17:16:04 +0100

ipreso.iplayer (0.0.20100210) unstable; urgency=low

  * iWM is adding the property IPRESO_WID to manage windows.
  * Added option to show media in a test-zone.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 10 Feb 2010 16:01:15 +0100

ipreso.iplayer (0.0.20100205) unstable; urgency=low

  * Allow ':' characters in Composer's todo-list.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 05 Feb 2010 17:41:42 +0100

ipreso.iplayer (0.0.20100204.1) unstable; urgency=low

  * iSynchro is relaying messages coming from Composer to plugins.
  * iSynchro sends message to composer's plugins via common interface.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 04 Feb 2010 15:24:59 +0100

ipreso.iplayer (0.0.20100202.1) unstable; urgency=low

  * Disabled plugins are purged on player.
  * Disabled commands are purged on player.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 02 Feb 2010 15:43:19 +0100

ipreso.iplayer (0.0.20100201.2) unstable; urgency=low

  * Don't download disabled plugins.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 01 Feb 2010 19:03:13 +0100

ipreso.iplayer (0.0.20100201.1) unstable; urgency=low

  * Added commands-plugins management.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 01 Feb 2010 11:55:20 +0100

ipreso.iplayer (0.0.20100127.1) unstable; urgency=low

  * Removed power control from Player's core.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 27 Jan 2010 16:02:42 +0100

ipreso.iplayer (0.0.20100124.4) unstable; urgency=low

  * Clean player package, according to Mercurial repository.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 24 Jan 2010 16:17:16 +0100

ipreso.iplayer (0.0.20100124.3) unstable; urgency=low

  * Fixed argument error on iSynchro invocation.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 24 Jan 2010 13:47:28 +0100

ipreso.iplayer (0.0.20100124) unstable; urgency=low

  * When plugin is not found, ask plugin's synchronization to the composer.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 24 Jan 2010 12:02:50 +0100

ipreso.iplayer (0.0.20100122.2) unstable; urgency=low

  * Automatic download of plugins.
  * Removed dependencies to Freeglut3 and VLC

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 22 Jan 2010 17:06:46 +0100

ipreso.iplayer (0.0.20100122.1) unstable; urgency=low

  * Removed dependency to imagemagick.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 22 Jan 2010 10:21:16 +0100

ipreso.iplayer (0.0.20100115) unstable; urgency=low

  * Removed all plugins from the base package.
  * Added binary to check plugin's format.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 16 Jan 2010 10:39:05 +0100

ipreso.iplayer (0.0.20091228.1) unstable; urgency=low

  * Fixed Track Ticket #90: Added new aspect to video plugin.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 27 Dec 2009 18:22:12 +0100

ipreso.iplayer (0.0.20091227.1) unstable; urgency=low

  * Added message to ask composer to update license.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 27 Dec 2009 12:03:41 +0100

ipreso.iplayer (0.0.20091226.2) unstable; urgency=low

  * Ask composer to update license when decrypting fails.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 26 Dec 2009 21:36:37 +0100

ipreso.iplayer (0.0.20091226.1) unstable; urgency=low

  * Automatic update of soon-expired licenses.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 26 Dec 2009 18:55:41 +0100

ipreso.iplayer (0.0.20091221.2) unstable; urgency=low

  * Updated default package repository

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 21 Dec 2009 17:40:52 +0100

ipreso.iplayer (0.0.20091221.1) unstable; urgency=low

  * HTTPs download of packages, and updated switch-off process

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 21 Dec 2009 17:29:22 +0100

ipreso.iplayer (0.0.20091120.1) unstable; urgency=low

  * Postinst script configure APT for HTTPs connections.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 20 Nov 2009 16:59:25 +0100

ipreso.iplayer (0.0.20091117.3) unstable; urgency=low

  * Updated postinst with new tty management in inittab.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 17 Nov 2009 22:51:58 +0100

ipreso.iplayer (0.0.20091117.2) unstable; urgency=low

  * Updated postinst for automatic start X.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 17 Nov 2009 22:12:23 +0100

ipreso.iplayer (0.0.20091117.1) unstable; urgency=low

  * Updated postinst bug to be independant of the USB Stick installation.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 17 Nov 2009 18:14:20 +0100

ipreso.iplayer (0.0.20091116.2) unstable; urgency=low

  * Fixed postinst bug related to sudoers file checking.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 16 Nov 2009 14:46:18 +0100

ipreso.iplayer (0.0.20091116) unstable; urgency=low

  * Added clock synchronization on Composer's clock.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 16 Nov 2009 14:25:26 +0100

ipreso.iplayer (0.0.20091115) unstable; urgency=low

  * Updated expired-licenses checks.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 15 Nov 2009 16:51:23 +0100

ipreso.iplayer (0.0.20091110) unstable; urgency=low

  * Timeout is set to higher values for HTTP Downloads.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 10 Nov 2009 16:28:00 +0100

ipreso.iplayer (0.0.20091105.6) unstable; urgency=low

  * Faster scrolling start.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 14:32:01 +0100

ipreso.iplayer (0.0.20091105.5) unstable; urgency=low

  * Fixed scrolling fade-out bug.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 11:21:38 +0100

ipreso.iplayer (0.0.20091105.4) unstable; urgency=low

  * time_synchro is updated only if already present in updated configuration 
    file

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 10:34:21 +0100

ipreso.iplayer (0.0.20091105.3) unstable; urgency=low

  * Added log traces in postinst script.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 10:30:08 +0100

ipreso.iplayer (0.0.20091105.2) unstable; urgency=low

  * Updated postinst script.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 10:26:44 +0100

ipreso.iplayer (0.0.20091105.1) unstable; urgency=low

  * Fixed Track Ticket #83: cleaner start/stop operations.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Nov 2009 10:09:28 +0100

ipreso.iplayer (0.0.20091103.3) unstable; urgency=low

  * Delayed event in the basic calendar (synchro, apt-get update)

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Nov 2009 18:56:45 +0100

ipreso.iplayer (0.0.20091103.2) unstable; urgency=low

  * Fixed Trac Ticket #78: iBoxes start with the last played program.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Nov 2009 17:32:23 +0100

ipreso.iplayer (0.0.20091103) unstable; urgency=low

  * Added program's name in ALIVE messages debug info.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Nov 2009 13:54:26 +0100

ipreso.iplayer (0.0.20091102.3) unstable; urgency=low

  * Change permissions of /var/log/iplayer.log to 644.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 02 Nov 2009 10:23:53 +0100

ipreso.iplayer (0.0.20091102.2) unstable; urgency=low

  * Change permissions of /var/log/iplayer.log to 640.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 02 Nov 2009 10:21:25 +0100

ipreso.iplayer (0.0.20091102.1) unstable; urgency=low

  * /var/log/iplayer.log readable by player user.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 02 Nov 2009 10:14:19 +0100

ipreso.iplayer (0.0.20091101.2) unstable; urgency=low

  * Added 'End download' message for Composer, when download of a program is
    finished.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 01 Nov 2009 17:49:03 +0100

ipreso.iplayer (0.0.20091101) unstable; urgency=low

  * Fixed Trac Ticket #46: remove license files when expired.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 01 Nov 2009 12:03:12 +0100

ipreso.iplayer (0.0.20091030) unstable; urgency=low

  * Fixed Trac Ticket #35: automatic logs upload.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 30 Oct 2009 19:29:35 +0100

ipreso.iplayer (0.0.20091028.2) unstable; urgency=low

  * Fixed Trac Ticket #8: improved robustness when media is missing (try to
    not invert zone when iWM is locked by a not-loaded window).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 28 Oct 2009 16:47:10 +0100

ipreso.iplayer (0.0.20091028.1) unstable; urgency=low

  * Fixed Trac Ticket #55: bug on /etc/hosts file update.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 28 Oct 2009 15:59:54 +0100

ipreso.iplayer (0.0.20091014.7) unstable; urgency=low

  * Removed iSynchro debug traces.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 16:07:06 +0200

ipreso.iplayer (0.0.20091014.6) unstable; urgency=low

  * Fixed Symlink creation bug.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 16:01:05 +0200

ipreso.iplayer (0.0.20091014.5) unstable; urgency=low

  * Improved robustness when manifest file has not a correct syntax.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 15:52:49 +0200

ipreso.iplayer (0.0.20091014.4) unstable; urgency=low

  * Added debug info to guess why iSynchro doesn't end synchronization on the
    Web.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 15:45:03 +0200

ipreso.iplayer (0.0.20091014.3) unstable; urgency=low

  * Fixed playlist bug with bad item paths.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 12:40:25 +0200

ipreso.iplayer (0.0.20091014.2) unstable; urgency=low

  * Fixed MD5 parsing (on decrypted content, not file on HD).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 12:39:42 +0200

ipreso.iplayer (0.0.20091014.1) unstable; urgency=low

  * ALIVE messages send MD5 of used files, to know if we're up-to-date event
    if program's name doesn't change.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 14 Oct 2009 08:53:58 +0200

ipreso.iplayer (0.0.20091013.8) unstable; urgency=low

  * Removed the ALIVE message after synchronization. It produces double sync
    because of iZoneMgr which doesn't update the current program faster.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 19:30:40 +0200

ipreso.iplayer (0.0.20091013.7) unstable; urgency=low

  * Send ALIVE message event if synchronization fails.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 19:25:56 +0200

ipreso.iplayer (0.0.20091013.6) unstable; urgency=low

  * Send ALIVE message when synchronization is done.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 19:20:34 +0200

ipreso.iplayer (0.0.20091013.5) unstable; urgency=low

  * Fixed Scheduling bug

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 19:20:19 +0200

ipreso.iplayer (0.0.20091013.4) unstable; urgency=low

  * Redirected apt-get output to /dev/null.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 18:22:21 +0200

ipreso.iplayer (0.0.20091013.3) unstable; urgency=low

  * Added APT-GET in sudoers for player user.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 18:19:17 +0200

ipreso.iplayer (0.0.20091013.2) unstable; urgency=low

  * Updated init.d script

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 18:03:35 +0200

ipreso.iplayer (0.0.20091013.1) unstable; urgency=low

  * Updated Packager

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 17:52:16 +0200

ipreso.iplayer (0.0.20091013) unstable; urgency=low

  * Added hourly package update.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 13 Oct 2009 11:36:16 +0200

ipreso.iplayer (0.0.20091009) unstable; urgency=low

  * Added program-change remotly.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 09 Oct 2009 14:48:58 +0200

ipreso.iplayer (0.0.20090919) unstable; urgency=low

  * Debug info for Switch-off

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 17 Sep 2009 09:25:25 +0200

ipreso.iplayer (0.0.20090917) unstable; urgency=low

  * Fixed 'apply parameters' function.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 17 Sep 2009 09:25:25 +0200

ipreso.iplayer (0.0.20090916) unstable; urgency=low

  * License check with Synchro component.
  * Removed 'get license' of the basic calendar.
  * Synchro component sends ALIVE message.
  * Added parameters file for live-updates information.
  * Added Switch-on and Switch-off parameters in Synchro component.
  * Added UPDATE functionality to the Synchro component.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 15 Sep 2009 10:08:54 +0200

ipreso.iplayer (0.0.20090814) unstable; urgency=low

  * Removed loop parameter from the Scroll plugin command line.
  * Resolution-independant scrolling speed.
  * Use QuesoGLC for Clock rendering.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 14 Aug 2009 11:08:24 +0200

ipreso.iplayer (0.0.20090805) unstable; urgency=low

  * Allow custom positionning of Video media.
  * Use QuesoGLC for Scroll rendering.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 05 Aug 2009 15:53:23 +0200

ipreso.iplayer (0.0.20090508) unstable; urgency=low

  * Closes: #16

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 13 May 2009 12:48:13 +0200

ipreso.iplayer (0.0.20090507) unstable; urgency=low

  * Updated Scroll plugin to show unicode characters.
  * Updated Clock plugin to have blinking ':' between hours and minutes.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 07 May 2009 19:52:00 +0200

ipreso.iplayer (0.0.20090422) unstable; urgency=low

  * Updated init.d script.
  * Updated configuration to allow more time to windows to load.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 22 Apr 2009 18:05:52 +0200

ipreso.iplayer (0.0.20090420) unstable; urgency=low

  * Fixed font of the Clock.
  * Fixed Video plugin to play video with length of 1s.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 20 Apr 2009 16:19:45 +0200

ipreso.iplayer (0.0.20090417) unstable; urgency=low

  * Removed blinking clock.
  * Added one more authorized instance of VLC.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 17 Apr 2009 17:23:19 +0200

ipreso.iplayer (0.0.20090412) unstable; urgency=low

  * Set correct permission for the new licenses mechanism.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 12 Apr 2009 17:27:07 +0200

ipreso.iplayer (0.0.20090411) unstable; urgency=low

  * Updated licenses mechanism.
  * Updated Scroll binary with play/pause functions.
  * Updated whole player to have nicer restart procedure.
  * Updated Scheduler to automatically download newer license file.
  * Added Ibidum Certificate to iplayer package.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sat, 11 Apr 2009 20:22:36 +0200

ipreso.iplayer (0.0.20090322) unstable; urgency=low

  * Changed Clock Plugin's name.
  * Updated Video Plugin to manage QuickTime movies.
  * Updated Video Plugin to have custom aspect ratios.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Sun, 22 Mar 2009 16:26:22 +0100

ipreso.iplayer (0.0.20090316) unstable; urgency=low

  * Improved robustness.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 16 Mar 2009 09:08:42 +0100

ipreso.iplayer (0.0.20090312) unstable; urgency=low

  * Added Hour plugin.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 12 Mar 2009 15:50:02 +0100

ipreso.iplayer (0.0.20090311.01) unstable; urgency=low

  * Fixed bug with Xlib.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Wed, 11 Mar 2009 14:14:28 +0100

ipreso.iplayer (0.0.20090310.01) unstable; urgency=low

  * Fixed bugs in Scrolling application.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 10 Mar 2009 08:24:34 +0100

ipreso.iplayer (0.0.20090309) unstable; urgency=low

  * Added iScroll plugin and application.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 09 Mar 2009 11:20:42 +0100

ipreso.iplayer (0.0.20090305) unstable; urgency=low

  * Better shutdown process, allowing to safely restart the daemon.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 05 Mar 2009 14:57:21 +0100

ipreso.iplayer (0.0.20090303.07) unstable; urgency=low

  * Updated iWM : wait that window is moved to the correct desktop before
    releasing the semaphore.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 18:44:34 +0100

ipreso.iplayer (0.0.20090303.06) unstable; urgency=low

  * Added debug information in WM to know why sometimes logo are not displayed.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 18:34:10 +0100

ipreso.iplayer (0.0.20090303.05) unstable; urgency=low

  * Automatically clean the log file when version is upgraded.
  * Replaced 'killall' command by getting PID via ps.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 18:00:22 +0100

ipreso.iplayer (0.0.20090303.04) unstable; urgency=low

  * Updated iWM Semaphore mechanism (improved efficiency).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 17:44:20 +0100

ipreso.iplayer (0.0.20090303.03) unstable; urgency=low

  * Removed from iWM unused trace.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 17:08:14 +0100

ipreso.iplayer (0.0.20090303.02) unstable; urgency=low

  * Set Audio volume to maximum.
  * Last version of iPlayer (with version control)

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 14:41:36 +0100

ipreso.iplayer (0.0.20090303) unstable; urgency=low

  * Added sysklogd dependence.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Tue, 03 Mar 2009 13:42:36 +0100

ipreso.iplayer (0.0.20090302) unstable; urgency=low

  * Added Sudo dependence
  * Added ipreso-adm auto-upgrade

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 02 Mar 2009 17:11:53 +0100

ipreso.iplayer (0.0.20090227) unstable; urgency=low

  * Updated Video plugin to have video on the entire zone.
  * Closes: #2

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 27 Feb 2009 10:32:07 +0100

ipreso.iplayer (0.0.20090223.12) unstable; urgency=low

  * Checking automatic upgrade with this dummy package...

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 18:59:23 +0100

ipreso.iplayer (0.0.20090223.11) unstable; urgency=low

  * Updated /etc/init.d/iplayer, now waiting to have a valid IP address before
    trying to upgrade the iPlayer package.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 18:55:02 +0100

ipreso.iplayer (0.0.20090223.10) unstable; urgency=low

  * Updated /etc/init.d/iplayer with better upgrade management.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 18:30:25 +0100

ipreso.iplayer (0.0.20090223.09) unstable; urgency=low

  * New package with only upgrade validation goal (2nd part).

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 18:12:52 +0100

ipreso.iplayer (0.0.20090223.08) unstable; urgency=low

  * New package with only upgrade validation goal.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 18:00:39 +0100

ipreso.iplayer (0.0.20090223.07) unstable; urgency=low

  * Fixed /etc/init.d/iplayer with no launch if already started.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 17:35:57 +0100

ipreso.iplayer (0.0.20090223.06) unstable; urgency=low

  * Added a package dependence (acpi-support-base) to have Power button shutting
    down the unit when pressed.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 17:23:34 +0100

ipreso.iplayer (0.0.20090223.05) unstable; urgency=low

  * Removed /etc/init.d/iplayer from the configuration file set.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 17:10:54 +0100

ipreso.iplayer (0.0.20090223.04) unstable; urgency=low

  * Automatic upgrade test via Repository website.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 17:03:27 +0100

ipreso.iplayer (0.0.20090223.03) unstable; urgency=low

  * Updated init script: wait for the X server to be running

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 15:32:23 +0100

ipreso.iplayer (0.0.20090223.02) unstable; urgency=low

  * Added Alsa dependancies.
  * Updated init script with 'player' user.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 11:40:35 +0100

ipreso.iplayer (0.0.20090223.01) unstable; urgency=low

  * Fixed SHM Error while iLaunch is started via initd scripts.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Mon, 23 Feb 2009 10:39:16 +0100

ipreso.iplayer (0.0.20090220.05) unstable; urgency=low

  * Updated package creation with correct postinst and postrm operations.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 20 Feb 2009 17:03:03 +0100

ipreso.iplayer (0.0.20090220.04) unstable; urgency=low

  * Updated configuration file, with Debian package paths.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 20 Feb 2009 15:25:13 +0100

ipreso.iplayer (0.0.20090220.02) unstable; urgency=low

  * Fixed postinst script to create symbolic link to /usr/bin

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 20 Feb 2009 14:46:54 +0100

ipreso.iplayer (0.0.20090220.01-1.1) unstable; urgency=low

  * Added Postinst script.

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Fri, 20 Feb 2009 14:38:27 +0100

ipreso.iplayer (0.0.20090219.01-1) unstable; urgency=low

  * Initial release

 -- Marc Simonetti <marc.simonetti@geekcorp.fr>  Thu, 19 Feb 2009 11:52:55 +0100
