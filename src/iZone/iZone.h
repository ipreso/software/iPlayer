// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iZone/iZone.h
 * Description: Manage a zone (playlist, dimensions, ...)
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.31: Original revision
 *****************************************************************************/

#ifndef VERSION
#define IZONE_VERSION     "Version Unknown"
#else
#define IZONE_VERSION     VERSION
#endif
#define IZONE_APPNAME           "iZone (iPlayer Zone)"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

#include "zones.h"

void launchZone ();
void doManage   ();

void iZoneSignalsHandler ();
void signalHandler (int sig);

int killZone (char * name);
int playZone (char * name, int once);
int stopZone (char * name);

void manageDifferences (sZone * zone);

void listPlaylist (char * name);

int loadItem (char * item);
int playItem (int wid);
int waitItem (int wid);
int stopItem (int wid);

int playQueue ();           // New playlist management
int getNextItem (int current);
int preloadNextItem (int current, int * next, int * nextWin, int * status);
int isItemValid (int item);
