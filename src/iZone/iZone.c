// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iZone/iZone.c
 * Description: Manage a zone (playlist, dimensions, ...)
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.31: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#include "iZone.h"
#include "iCommon.h"
#include "log.h"
#include "SHM.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iZone [options]\n"                                                     \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    -c, --conf '<file>'      Set the configuration file of the iPlayer\n" \
"\n"                                                                    \
"iZone <action> [options] -z <zone>\n"                                  \
"    -z, --zone '<name>'      Set the zone targeted by the action\n"   \
"\n"                                                                    \
"  <action>:\n"                                                         \
"    -a, --all                List all media in a queue of the zone\n"  \
"    -k, --kill               Kill the Zone process\n"                  \
"    -p, --play               Run the playlist of the zone\n"           \
"    -q, --queue '<media>'    Add media to the queue\n"                 \
"    -r, --reset              Reset queue of the zone\n"                \
"    -s, --stop               Stop the playlist of the zone\n"          \
"    -u, --update             Update zone information with given options\n" \
"    -x, --execute            Execute the Zone process with given options\n" \
"\n"                                                                    \
"  <options>:\n"                                                        \
"    -f, --format <int>       Set the format information of the zone\n" \
"    -g, --geometry <int>x<int> Set the geometry information of the zone\n" \
"    -i, --hidden <int>       Set the hidden information of the zone\n" \
"    -l, --layer <int>        Set the layer information of the zone\n"  \
"    -o, --offset <int>x<int> Set the offset information of the zone\n" \
"\n"                                                                    \
"    --once                   Play once each media and stop\n"          \
"\n"                                                                    \

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"conf",        required_argument,  0, 'c'},

    {"zone",        required_argument,  0, 'z'},

    {"all",         no_argument,        0, 'a'},
    {"kill",        no_argument,        0, 'k'},
    {"play",        no_argument,        0, 'p'},
    {"queue",       required_argument,  0, 'q'},
    {"reset",       no_argument,        0, 'r'},
    {"stop",        no_argument,        0, 's'},
    {"update",      no_argument,        0, 'u'},
    {"execute",     no_argument,        0, 'x'},

    {"format",      required_argument,  0, 'f'},
    {"geometry",    required_argument,  0, 'g'},
    {"hidden",      required_argument,  0, 'i'},
    {"layer",       required_argument,  0, 'l'},
    {"offset",      required_argument,  0, 'o'},

    {"once",        no_argument,        0, 'b'},

    {0, 0, 0, 0}
};

sConfig     config;
sZone       gZone;
int         gStop;
int         gPlaylist;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char params [128];
    char media [CMDLINE_MAXLENGTH];
    char name [ZONE_MAXLENGTH];
    char action = '\0';
    int  once = 0;

    gPlaylist = 0;
    gStop = 0;

    memset (params, '\0', sizeof (params));
    memset (name, '\0', sizeof (name));
    memset (media, '\0', sizeof (media));
    memset (&gZone, '\0', sizeof (gZone));
    gZone.hidden     = -1;
    gZone.layer      = -1;
    gZone.x          = -1;
    gZone.y          = -1;
    gZone.width      = -1;
    gZone.height     = -1;
    gZone.format     = -1;
    gZone.pid        = -1;
    gZone.status     = STATUS_UNKNOWN;

    // Parse parameters
    while ((c = getopt_long (argc, argv, "ahvbc:z:i:l:o:g:f:kxpsuq:r",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", IZONE_APPNAME, IZONE_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'b':
                once = 1;
                break;
            case 'z':
                strncpy (name, optarg, sizeof (name)-1);
                break;

            case 'i':
                gZone.hidden = atoi (optarg);
                break;
            case 'l':
                gZone.layer = atoi (optarg);
                break;
            case 'o':
                if (sscanf (optarg, "%dx%d", &gZone.x, &gZone.y) != 2)
                {
                    fprintf (stderr, "Offset option is invalid.\n");
                    return EXIT_FAILURE;
                }
                break;
            case 'g':
                if (sscanf (optarg, "%dx%d", &gZone.width, &gZone.height) != 2)
                {
                    fprintf (stderr, "Geometry option is invalid.\n");
                    return EXIT_FAILURE;
                }
                break;
            case 'f':
                gZone.format = atoi (optarg);
                break;

            case 'a':
                action = 'a';
                break;
            case 'k':
                action = 'k';
                break;
            case 'x':
                action = 'x';
                break;
            case 'p':
                action = 'p';
                break;
            case 'q':
                action = 'q';
                strncpy (media, optarg, sizeof (media)-1);
                break;
            case 'r':
                action = 'r';
                break;
            case 's':
                action = 's';
                break;
            case 'u':
                action = 'u';
                break;

            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (params) <= 0)
    {
        // Default configuration file
        strncpy (params, DEFAULT_CONFIGURATIONFILE, sizeof (params)-1);
    }

    if (common_getConfig (params, &config) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }

    if (strlen (name) <= 0)
    {
        fprintf (stderr, "Please give a zone (-z '<zone>')\n");
        return (EXIT_FAILURE);
    }
    switch (action)
    {
        case 'a':
            listPlaylist (name);
            break;
        case 'k':
            killZone (name);
            break;
        case 'x':
            // Check validity of the data, fork the process
            // and manage the zone
            strncpy (gZone.name, name, ZONE_MAXLENGTH);
            launchZone ();
            break;
        case 'p':
            playZone (name, once);
            break;
        case 'q':
            // Enqueue the command line given
            if (!shm_queueMediaInZone (name, media))
            {
                iCrit ("Can't enqueue in '%s'", name);
                return (EXIT_FAILURE);
            }
            iLog ("[%s] Added '%s'", name, media);
            break;
        case 'r':
            // If zone is playing, stop it !
            stopZone (name);
            // Reset the playlist
            if (!shm_resetMediaInZone (name))
            {
                iCrit ("Can't reset the queue '%s'", name);
                return (EXIT_FAILURE);
            }
            iLog ("[%s] Playlist reset", name);
            break;
        case 's':
            stopZone (name);
            break;
        case 'u':
            fprintf (stdout, "TODO\n");
            break;
        default:
            fprintf (stderr, "Nothing to do. Exiting...\n");
    }

    return EXIT_SUCCESS;
}

void launchZone ()
{
    // Check for valid data
    if (!strlen (gZone.name))
    {
        iCrit ("Zone does not have a name ! Exiting.");
        return;
    }
    if ((gZone.hidden != 0 && gZone.hidden != 1)
        || (gZone.layer <= 0 || gZone.layer > LAYER_MAX)
        || (gZone.x < 0 || gZone.x > 100)
        || (gZone.y < 0 || gZone.y > 100)
        || (gZone.width <= 0 || gZone.width > 100)
        || (gZone.height <= 0 || gZone.height > 100))
    {
        iCrit ("Zone '%s' does not have correct parameters. Exiting.",
                gZone.name);
        return;
    }

    // Fork the process
    if (fork ())
        return;
    setsid ();
    gZone.pid       = getpid ();
    gZone.maxloop   = 0;
    gZone.loop      = LOOP_UNKNOWN;

    // Save the zone in Shared memory
    if (!shm_saveZone (&gZone))
    {
        iCrit ("Can't save Zone '%s' in memory. Exiting...", gZone.name);
        return;
    }

    doManage ();
}

void doManage ()
{
    // Be aware of signals !
    iZoneSignalsHandler ();

    iDebug ("Zone '%s' is waiting for an event...", gZone.name);

    gStop = 0;
    while (!gStop)
    {
        if (gPlaylist)
        {
            gStop = playQueue ();
            iDebug ("[%s] playQueue ended. gStop returned %d, gPlaylist is %d",
                    gZone.name, gStop, gPlaylist);
        }
        else
        {
            // Zone without playlist ? Considering the zone as NOK
            shm_setZoneStatus (gZone.name, STATUS_NOK);
            gZone.status = STATUS_NOK;
        }

        if (!gStop)
        {
            // Playlist will loop again, sleep a little to avoid
            // the 100% CPU
            sleep (60);
        }
    }

    // Set the playlist status to 'stop'
    if (!shm_stopZone (gZone.name))
    {
        iError ("Can't stop the queue '%s'", gZone.name);
    }

    // Removing the zone from Shared memory
    if (!shm_delZone (gZone.name))
    {
        iError ("[%s] Unable to remove zone from Memory", gZone.name);
    }
    iLog ("[%s] Zone removed from Memory", gZone.name);
}

int playQueue ()
{
    int currentIndex    = -1;
    int nextIndex       = -1;
    int previousIndex   = -1;
    int currentWin      = 0;
    int nextWin         = 0;
    int prevWin         = 0;

    int loopCount       = 0;
    int loopStatus      = STATUS_OK;

    int status; // For return results

    // Preload first item.
    // If some item cannot be preloaded, loopStatus will be set to NOK
    status = preloadNextItem (currentIndex, &nextIndex, &nextWin, &loopStatus);
    if (status != STATUS_OK)
    {
        // Set loop status to NOK
        loopStatus = STATUS_NOK;
        shm_setZoneStatus (gZone.name, STATUS_NOK);
        gZone.status = STATUS_NOK;
        iError ("[%s] Status KO: No valid media can be preloaded.", gZone.name);
    }
    else
    {
        iLog ("[%s] Status OK: Zone initialization.", gZone.name);
        shm_setZoneStatus (gZone.name, STATUS_OK);
        gZone.status = STATUS_OK;
    }

    // While next item is found (and probably preloaded)
    while (!gStop && gPlaylist && status == STATUS_OK)
    {
        // Current item = Next item, switch all variables
        previousIndex   = currentIndex;
        prevWin         = currentWin;
        currentIndex    = nextIndex;
        currentWin      = nextWin;

        // Load current item if not already done (only 1 item in the playlist)
        if (currentWin <= 0)
        {
            // Preload is not already done (1 item in the playlist ?)
            // - Stop the previous item
            if (prevWin > 0)
            {
                stopItem (prevWin);
                prevWin = 0;
            }
            // - Preload the current item
            currentWin = loadItem (gZone.playlist.items [currentIndex]);
            if (currentWin <= 0)
            {
                // Set loop status to NOK
                loopStatus = STATUS_NOK;
                shm_setZoneStatus (gZone.name, STATUS_NOK);
                gZone.status = STATUS_NOK;
                iError ("[%s] Status KO: Media %d cannot be preloaded.",
                            gZone.name, currentIndex);

                // Stop the loop since only one media
                status = STATUS_NOK;
            }
        }

        // Play current item
        if (!gStop && currentWin > 0)
        {
            iInfo ("[%s] [%d-%d]: '%s'", gZone.name, currentIndex, currentWin,
                                    gZone.playlist.items [currentIndex]);
            shm_setCurrentMedia (gZone.name, currentIndex);
            playItem (currentWin);
        }

        // Stop previous item
        if (previousIndex != currentIndex && prevWin > 0)
        {
            stopItem (prevWin);
            prevWin = 0;
        }

        // Preload next item
        if (!gStop)
        {
            status = preloadNextItem (currentIndex, &nextIndex, &nextWin, &loopStatus);
            if (status != STATUS_OK)
            {
                // Set loop status to NOK
                loopStatus = STATUS_NOK;
                shm_setZoneStatus (gZone.name, STATUS_NOK);
                gZone.status = STATUS_NOK;
                iError ("[%s] Status KO: Media %d cannot be preloaded.",
                            gZone.name, currentIndex);
            }
        }

        // Wait the end of the current item
        if (currentWin > 0 && !gStop)
            waitItem (currentWin);

        // If next item is <= current, a loop has be done
        if (nextIndex <= currentIndex)
        {
            // We'll begin a new loop ! Do the stuff accordingly...
            loopCount++;

            if (loopCount <= 1)
            {
                // We executed the loop for the first time
                // Mark the zone as done, and get the global status of the playlist
                // (if we have to stop now or if we can continue)
                if (!gStop)
                    gStop = shm_replayZone (gZone.name);
            }

            // Set the Zone status to OK if everything went fine
            if (loopStatus == STATUS_OK && gZone.status != STATUS_OK)
            {
                // One entire loop with OK status, set the Zone's status to OK
                iLog ("[%s] Status OK: All items played without problem",
                        gZone.name);
                shm_setZoneStatus (gZone.name, STATUS_OK);
                gZone.status = STATUS_OK;
            }

            // Reset the status to OK for this new loop
            loopStatus = STATUS_OK;
        }
    }

    if (gStop)
        iDebug ("[%s] Stop is required for the Zone", gZone.name);

    // Clean existing windows
    if (prevWin)
    {
        stopItem (prevWin);
        prevWin = 0;
    }
    if (nextWin)
    {
        stopItem (nextWin);
        nextWin = 0;
    }
    if (currentWin)
    {
        stopItem (currentWin);
        currentWin = 0;
    }

    return (gStop);
}

// Return current+1 or loop in the playlist
int getNextItem (int current)
{
    int next;

    if (current < 0 )
        next = 0;
    else
        next = current + 1;

    if (strlen (gZone.playlist.items[next]) <= 0 || next >= ITEMS_MAXNUMBER)
    {
        // Rewind...
        next = 0;
    }

    return (next);
}

int isItemValid (int item)
{
    char buffer [CMDLINE_MAXLENGTH];
    int valid;
    struct tm now;
    time_t now_t;
    int year, month, day;

    // Get the current date-time
    now_t = time (NULL);
    localtime_r (&now_t, &now);

    // Default is to consider item as valid
    valid = 1;

    // Check validity startdate of the item
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    if (getParam (&config, gZone.playlist.items[item], "startdate", 
                    buffer, sizeof (buffer)))
    {
        // Start date found
        if (strlen (buffer) > 0)
        {
            iDebug ("[%s]   - Item %d - Start: %s", gZone.name, item, buffer);

            // Do not use strptime
            //strptime(buffer, "%Y-%m-%d", &startdate);
            if (sscanf (buffer, "%d-%d-%d", &year, &month, &day) == 3)
            {
                // Struct tm is from 1900, and month starts at 0
                year -= 1900;
                month --;

                if (year > now.tm_year ||
                    (year == now.tm_year && month > now.tm_mon) ||
                    (year == now.tm_year && month == now.tm_mon && day > now.tm_mday))
                {
                    // Item's startdate is in the future
                    iLog ("[%s] Item %d not yet valid", gZone.name, item);
                    valid = 0;
                }
            }
        }
        else
        {
            iDebug ("[%s]   - Item %d - Start: No startdate defined",
                        gZone.name, item);
        }
    }

    // Check validity enddate of the item
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    if (getParam (&config, gZone.playlist.items[item], "enddate", 
                    buffer, sizeof (buffer)))
    {
        // End date found
        if (strlen (buffer) > 0)
        {
            iDebug ("[%s]   - Item %d - End: %s", gZone.name, item, buffer);
            if (sscanf (buffer, "%d-%d-%d", &year, &month, &day) == 3)
            {
                // Struct tm is from 1900, and month starts at 0
                year -= 1900;
                month --;

                if (year < now.tm_year ||
                    (year == now.tm_year && month < now.tm_mon) ||
                    (year == now.tm_year && month == now.tm_mon && day < now.tm_mday))
                {
                    // Item's enddate is in the past
                    iLog ("[%s] Item %d is now obsolete", gZone.name, item);
                    valid = 0;
                }
            }
        }
        else
        {
            iDebug ("[%s]   - Item %d - End: No enddate defined", gZone.name, item);
        }
    }

    return (valid);
}

int preloadNextItem (int current, int * next, int * nextWin, int * status)
{
    int valid;
    int firstTriedItem = -1;
    int iNext;
    int iNextWin;

    iDebug ("[%s] Load item following %d...", gZone.name, current);

    // Initialize the status
    *nextWin = 0;           // No window for the next item
    *next = -1;             // Index of next media is unknown

    valid = 0;              // Next item is invalid (nothing yet defined)
    
    // Get first item following current one
    iNext = getNextItem (current);

    // Search for a time-valid AND preloaded window
    while ((firstTriedItem == -1 || firstTriedItem != iNext) &&
            strlen (gZone.playlist.items [iNext]) > 0)
    {
        iDebug ("[%s] - Trying item %d...", gZone.name, iNext);

        // Initialize the first tried item of the loop
        if (firstTriedItem == -1)
            firstTriedItem = iNext;

        // Get validity of this item
        valid = isItemValid (iNext);
        if (valid)
        {
            iDebug ("[%s]   - Dates are OK", gZone.name);

            // If this is a different item as current one, try to preload item
            if (iNext != current)
            {
                iNextWin = loadItem (gZone.playlist.items [iNext]);
                if (iNextWin > 0)
                {
                    iDebug ("[%s]   - Preload OK (win %d)", gZone.name, iNextWin);
                    // Preload is successfull
                    *next       = iNext;
                    *nextWin    = iNextWin;
                    return (STATUS_OK);
                }
                else
                {
                    iDebug ("[%s]   - Preload Failed", gZone.name);
                    // Set loop status to NOK since time-valid item cannot be preloaded
                    if (*status == STATUS_OK)
                        *status = STATUS_NOK;
                }
            }
            else
            {
                iDebug ("[%s]   - No preload, because same as current one", gZone.name);

                // Function is successful, but we do not preload item since
                // identical to current one
                *next = iNext;
                *nextWin = 0;
                return (STATUS_OK);
            }
        }

        // Continue with next item in the playlist
        iNext = getNextItem (iNext);
    }

    // Entire loop of the playlist without finding a valid AND preloadable item
    *next = -1;
    *nextWin = 0;
    return (STATUS_NOK);
}

int loadItem (char * item)
{
    FILE * fp;
    char buffer [CMDLINE_MAXLENGTH];
    char wid [16];
    int status;

    // Launch the command and get the WID returned
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -l '%s'",
                config.path_iappctrl,
                gZone.name,
                item);
    
    //iDebug ("Executing '%s'", buffer);    
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[%s] Unable to launch '%s'", gZone.name, buffer);
        return (0);
    }

    memset (wid, '\0', sizeof (wid));
    if (!fgets (wid, sizeof (wid), fp))
    {
        iError ("[%s] Cannot get result of '%s'", gZone.name, buffer);
        pclose (fp);
        return (0);
    }
//iDebug ("Result is '%s'", wid);
    status = pclose (fp);
    if (status != 0)
    {
        iError ("[%s] Unable to get the Window during load process", gZone.name);
        return (0);
    }
    while (strlen (wid) && (wid [strlen (wid)-1] == '\r' ||
                            wid [strlen (wid)-1] == '\n'))
        wid [strlen (wid)-1] = '\0';

    return (atoi (wid));
}

int playItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -p %d &",
                config.path_iappctrl, gZone.name, wid);
    
    //iDebug ("Executing '%s'", buffer);    
    system (buffer);
    return (1);
}

int waitItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -w %d",
                config.path_iappctrl, gZone.name, wid);
    
    //iDebug ("Executing '%s'", buffer);    
    system (buffer);
    return (1);
}

int stopItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -u %d",
                config.path_iappctrl, gZone.name, wid);
    
    //iDebug ("Executing in stopItem : '%s'", buffer);    
    system (buffer);
    return (1);
}

int killZone (char * name)
{
    sZone zone;

    if (!name)
        return (0);

    // Get PID of the concerned process
    if (shm_getZone (name, &zone))
    {
        // Set the playlist status to 'stop'
        if (!shm_killZone (name))
        {
            iError ("Can't stop the queue '%s'", name);
            return (0);
        }
        if (zone.pid > 0)
        {
            // Send a signal to update its status
            kill (zone.pid, SIGHUP);
            return (1);
        }
        else
        {
            iError ("Can't stop zone '%s' : PID %d is invalid", 
                     zone.name, zone.pid);
            return (0);
        }
    }
    else
    {
        iError ("Can't close zone '%s' : not found.", name);
        return (0);
    }
}

void listPlaylist (char * name)
{
    int i;
    sZone zone;

    if (!name)
        return;

    // Get PID of the concerned process
    if (!shm_getZone (name, &zone))
    {
        fprintf (stderr, "Can't get zone '%s'\n", name);
        return;
    }

    switch (zone.playlist.status)
    {
        case PLAYLIST_STOP:
            fprintf (stdout, "%s : STOP\n", name);
            break;
        case PLAYLIST_PLAY:
            fprintf (stdout, "%s : PLAY\n", name);
            break;
        case PLAYLIST_PAUSE:
            fprintf (stdout, "%s : PAUSE\n", name);
            break;
        default:
            fprintf (stdout, "%s : Unknown status\n", name);
    }    
    switch (zone.status)
    {
        case STATUS_OK:
            fprintf (stdout, "Status: OK\n");
            break;
        case STATUS_NOK:
            fprintf (stdout, "Status: ERROR\n");
            break;
        case STATUS_UNKNOWN:
        default:
            fprintf (stdout, "Status: UNKNOWN\n");
    }
    switch (zone.loop)
    {
        case LOOP_INFINITE:
            fprintf (stdout, "Infinite loop.\n");
            break;
        case LOOP_FIRSTTIME:
            fprintf (stdout, "First loop.\n");
            break;
        case LOOP_NTIMES:
            fprintf (stdout, "No loop > 1.\n");
            break;
        default:
            fprintf (stdout, "Unknown loop status.\n");
    }

    i = 0;
    while (i < ITEMS_MAXNUMBER && strlen (zone.playlist.items[i]))
    {
        if (i == zone.playlist.current)
            fprintf (stdout, "[X] ");
        else
            fprintf (stdout, "[ ] ");
        fprintf (stdout, "%s\n", zone.playlist.items[i]);

        i++;
    }

    if (i == 0)
    {
        fprintf (stdout, "Playlist is empty.\n");
    }
}

int playZone (char * name, int once)
{
    sZone zone;

    if (!name)
        return (0);

    // Get PID of the concerned process
    if (shm_getZone (name, &zone))
    {
        // Set the playlist status to 'play'
        if (!shm_playZone (name, once))
        {
            iError ("Cannot play the queue '%s'", name);
            return (0);
        }

        if (zone.pid > 0)
        {
            // Send a signal to update its status
            kill (zone.pid, SIGHUP);
            return (1);
        }
        else
        {
            iError ("Can't play zone '%s' : PID %d is invalid", 
                     zone.name, zone.pid);
            return (0);
        }
    }
    else
    {
        iError ("Can't play zone '%s' : not found.", name);
        return (0);
    }
}

int stopZone (char * name)
{
    sZone zone;

    if (!name)
        return (0);

    // Get PID of the concerned process
    if (shm_getZone (name, &zone))
    {
        // Set the playlist status to 'stop'
        if (!shm_stopZone (name))
        {
            iError ("Can't stop the queue '%s'", name);
            return (0);
        }

        if (zone.pid > 0)
        {
            // Send a signal to update its status
            kill (zone.pid, SIGHUP);
            return (1);
        }
        else
        {
            iError ("Can't stop zone '%s' : PID %d is invalid", 
                     zone.name, zone.pid);
            return (0);
        }
    }
    else
    {
        iError ("Can't stop zone '%s' : not found.", name);
        return (0);
    }
}

void manageDifferences (sZone * zone)
{
    if (zone->hidden != gZone.hidden)
    {
        iDebug ("[%s] Hidden status changed %d -> %d",
                gZone.name,
                gZone.hidden,
                zone->hidden);
    }
    if (zone->layer != gZone.layer)
    {
        iDebug ("[%s] Layer changed %d -> %d",
                gZone.name,
                gZone.layer,
                zone->layer);
    }
    if (zone->x != gZone.x || zone->y != gZone.y)
    {
        iDebug ("[%s] Offset changed %dx%d -> %dx%d",
                gZone.name,
                gZone.x, gZone.y,
                zone->x, zone->y);
    }
    if (zone->width != gZone.width || zone->height != gZone.height)
    {
        iDebug ("[%s] Geometry changed %dx%d -> %dx%d",
                gZone.name,
                gZone.width, gZone.height,
                zone->width, zone->height);
    }
    if (zone->layer != gZone.layer)
    {
        iDebug ("[%s] Format changed %d -> %d",
                gZone.name,
                gZone.format,
                zone->format);
    }
    if (zone->playlist.status != gZone.playlist.status)
    {
        iDebug ("[%s] Status of the queue changed %d -> %d",
                gZone.name,
                gZone.playlist.status,
                zone->playlist.status);

        switch (zone->playlist.status)
        {
            case PLAYLIST_KILL:
                gStop = 1;
            case PLAYLIST_STOP:
                // Stop the play of the playlist
                gPlaylist = 0;
                // Set the Apps of the zone to STOP: means stop the current one
                shm_setAppsStatus (gZone.name, 0);
                break;
            case PLAYLIST_PLAY:
                gPlaylist = 1;
                break;
        }
        gZone.playlist.status = zone->playlist.status;
    }
    if (memcmp (zone->playlist.items, 
                gZone.playlist.items, 
                sizeof (commandLine) * ITEMS_MAXNUMBER) != 0)
    {
        iDebug ("[%s] Playlist queue changed", gZone.name);
        memcpy (&(gZone.playlist.items), &(zone->playlist.items),
                sizeof (commandLine) * ITEMS_MAXNUMBER);
    }
}

void iZoneSignalsHandler ()
{
    signal (SIGTERM, signalHandler);
    signal (SIGINT, signalHandler);
    signal (SIGHUP, signalHandler);
}

void signalHandler (int sig)
{
    sZone zone;

    switch (sig)
    {
        case SIGTERM:
        case SIGINT:
            iDebug ("[%s] (%d) Got 'Stop' signal", gZone.name, getpid ());
            // Leaving...
            gPlaylist = 0;
            gStop = 1;
            break;
        case SIGHUP:
            // Someone send us this signal because something changed
            // (play status, dimensions, ...)
            iDebug ("[%s] (%d) Got 'HUP' signal", gZone.name, getpid ());
            if (!shm_getZone (gZone.name, &zone))
            {
                iError ("[%s] Unable to get changes", gZone.name);
            }
            else
            {
                manageDifferences (&zone);
            }
            break;
        default:
            iError ("[%s] Unknown signal '%d'", gZone.name, sig);
    }
}
