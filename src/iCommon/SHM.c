// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/SHM.c
 * Description: Provide IPC Shared Memory functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <string.h>
#include <stdio.h>

#include "SHM.h"

int shm_createLock (key_t key)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;

    // Get the shared memory
    while ((shmid = shmget (key, shmsize, 0666)) < 0)
    {
        // Create the shared memory
        if ((shmid = shmget (key, shmsize, IPC_CREAT | 0666)) < 0)
        {
            return (0);
        }
    }
    
    return (shmid);
}

int shm_createConfigSM (key_t key)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;

    // Create the shared memory
    if ((shmid = shmget (key, shmsize, IPC_CREAT | 0666)) < 0)
    {
        return (-1);
    }
    
    return (shmid);
}

int shm_createParamsSM (key_t key)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_PARAMSSIZE;

    // Create the shared memory
    if ((shmid = shmget (key, shmsize, IPC_CREAT | 0666)) < 0)
    {
        return (-1);
    }
    
    return (shmid);
}

sParams * shm_getParamsSM (key_t key)
{
    char * shm;
    int shmid;
    int shmflag;
    int shmsize = SHM_PARAMSSIZE;

    // Get the shared memory
    if ((shmid = shmget (key, shmsize, 0666)) < 0)
        return (NULL);

    if ((shm = (char *)shmat (shmid, NULL, 0)) == (char *)-1)                 
    {   
        iError("Can't get the configuration memory. SHM Error.");            
        return (NULL);
    }

    return ((sParams*)shm);
}

sConfig * shm_getConfigSM (key_t key)
{
    char * shm;
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;

    // Get the shared memory
    if ((shmid = shmget (key, shmsize, 0666)) < 0)
        return (NULL);

    if ((shm = (char *)shmat (shmid, NULL, 0)) == (char *)-1)                 
    {   
        iError("Can't get the configuration memory. SHM Error.");            
        return (NULL);
    }

    return ((sConfig*)shm);
}

void shm_closeParamsSM (sParams * params)
{
    if (!params)
        return;

    shmdt (params);
}

void shm_closeConfigSM (sConfig * config)
{
    if (!config)
        return;

    shmdt (config);
}

int shm_createZonesSM ()
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;

    // Create the shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, IPC_CREAT | 0666)) < 0)
        return (0);
    
    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }
    
    // Blank it
    memset (shm, '\0', SHM_ZONESSIZE);    

    // Close it
    shmdt (shm);
    return (shmid);
}

int shm_saveZone (sZone * zone)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char*)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for an existing zone with the same name, override it
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (zone->name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            iLog ("Overriding zone '%s' in memory", zone->name);
            (*shm)[i].hidden   = zone->hidden;
            (*shm)[i].layer    = zone->layer;
            (*shm)[i].x        = zone->x;
            (*shm)[i].y        = zone->y;
            (*shm)[i].width    = zone->width;
            (*shm)[i].height   = zone->height;
            (*shm)[i].format   = zone->format;
            (*shm)[i].pid      = zone->pid;
            memset (&((*shm)[i].playlist), '\0', SHM_PLAYLISTSIZE);
            (*shm)[i].playlist.current = -1;
            (*shm)[i].maxloop  = zone->maxloop;
            (*shm)[i].loop     = zone->loop;
            (*shm)[i].status   = zone->status;
            shmdt (shm);
            return (1);
        }
    }

    // Write the new zone
    i = 0;
    while (i < ZONE_NBMAX && strlen ((*shm)[i].name) > 0)
        i++;

    if (i >= ZONE_NBMAX)
    {
        iError ("Can't add zone '%s' : maximum already reached", zone->name);
        shmdt (shm);
        return (0);
    }

    strncpy ((*shm)[i].name, zone->name, ZONE_MAXLENGTH);
    (*shm)[i].hidden   = zone->hidden;
    (*shm)[i].layer    = zone->layer;
    (*shm)[i].x        = zone->x;
    (*shm)[i].y        = zone->y;
    (*shm)[i].width    = zone->width;
    (*shm)[i].height   = zone->height;
    (*shm)[i].format   = zone->format;
    (*shm)[i].pid      = zone->pid;
    (*shm)[i].maxloop  = zone->maxloop;
    (*shm)[i].loop     = zone->loop;
    (*shm)[i].status   = zone->status;
    memset (&((*shm)[i].playlist), '\0', SHM_PLAYLISTSIZE);
    (*shm)[i].playlist.current = -1;

    // Close the Shared memory
    shmdt (shm);
    return (1);
}

int shm_getZone (char * name, sZone * zone)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            zone->hidden    = (*shm)[i].hidden;
            zone->layer     = (*shm)[i].layer;
            zone->x         = (*shm)[i].x;
            zone->y         = (*shm)[i].y;
            zone->width     = (*shm)[i].width;
            zone->height    = (*shm)[i].height;
            zone->format    = (*shm)[i].format;
            zone->pid       = (*shm)[i].pid;
            zone->loop      = (*shm)[i].loop;
            zone->maxloop   = (*shm)[i].maxloop;
            zone->status    = (*shm)[i].status;
            memcpy (&zone->playlist, &((*shm)[i].playlist), SHM_PLAYLISTSIZE);
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_getZones (char * zones, int tabSize, int zoneSize)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i, j;
    char * index;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    memset (zones, '\0', zoneSize*tabSize);
    index = zones;
    // List zones
    for (i = 0, j = 0 ; i < ZONE_NBMAX && j < tabSize ; i++)
    {
        if (strlen ((*shm)[i].name))
            strncpy (&(index[j++*zoneSize]), (*shm)[i].name, zoneSize-1);
    }

    // Close the Shared memory
    shmdt (shm);
    return (1);
}

int shm_delZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Delete the zone
            memset (&(*shm)[i], '\0', sizeof (sZone));
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

void shm_listZones ()
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return;

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        fprintf (stderr, "Can't get the Zones memory. SHM Error.\n");
        return;
    }

    // Show all zone characteristics
    printf ("Zones in memory:\n");
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strlen ((*shm)[i].name))
        {
            printf ("[%d]- %s: %d:%d:%dx%d:%dx%d:%d\n",
                        (*shm)[i].pid,
                        (*shm)[i].name,
                        (*shm)[i].hidden,
                        (*shm)[i].layer,
                        (*shm)[i].x,
                        (*shm)[i].y,
                        (*shm)[i].width,
                        (*shm)[i].height,
                        (*shm)[i].format);
        }
        else
        {
            printf ("[%d]- Empty\n", i);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return;
}

int shm_queueMediaInZone (char * name, char * cmdline)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i, j;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Go to the first free cmdline
            j = 0;
            while (j < ITEMS_MAXNUMBER &&
                   strlen ((*shm)[i].playlist.items[j]))
                j++;

            if (j >= ITEMS_MAXNUMBER)
            {
                iError ("Maximum number of media has already been reached.");
                return (0);
            }

            memcpy ((*shm)[i].playlist.items[j], cmdline, CMDLINE_MAXLENGTH);
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_getMediaInZone (char * name, sPlaylist * playlist)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            memcpy (playlist, &((*shm)[i].playlist), sizeof (sPlaylist));
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_resetMediaInZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            memset (&((*shm)[i].playlist), '\0', sizeof (sPlaylist));
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_getZoneStatus (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    int status;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            status = (*shm)[i].status;
            shmdt (shm);
            return (status);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (STATUS_UNKNOWN);
}

int shm_setZoneStatus (char * name, int status)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            (*shm)[i].status = status;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}


int shm_playZone (char * name, int once)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set status to "Playing"
            (*shm)[i].playlist.status   = PLAYLIST_PLAY;
            (*shm)[i].maxloop           = once;
            // Only unknown are affected, because already set are
            // preloaded INFINITE
            if ((*shm)[i].loop == LOOP_UNKNOWN)
                (*shm)[i].loop = LOOP_FIRSTTIME;

            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

// Returns:
// -1 : error
// 0  : we're replaying, but other zones are on the first loop
// 1  : we are the last zone to finish the first loop
int shm_replayZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i, j;
    int loop;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (-1);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set zone as looping
            iDebug ("Zone '%s' is marked as Replaying.", name);
            (*shm)[i].loop = LOOP_NTIMES;

            if ((*shm)[i].maxloop > 0 &&
                (*shm)[i].loop >= (*shm)[i].maxloop)
            {
                j = 0;
                loop = 1;
                // Check the loop status of other zones
                while (j < ZONE_NBMAX && loop)
                {
                    if ((*shm)[j].loop == LOOP_FIRSTTIME)
                    {
                        // First loop for at least one zone
                        loop = 0;
                    }
                    j++;
                }

                if (loop)
                    iLog ("Zone '%s' is the last zone to loop.", name);
                else
                    iDebug ("Zone '%s' is looping.", name);

                shmdt (shm);
                return (loop);
            }

            shmdt (shm);
            return (0);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (-1);
}

// Returns:
// -1 : error
// 0  : ok
int shm_infiniteZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (-1);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set zone as looping
            (*shm)[i].loop = LOOP_INFINITE;
            shmdt (shm);
            return (0);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (-1);
}

int shm_doNotLoopZones ()
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (-1);
    }

    // Mark all zone with maxloop = 1
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        // Set zone as looping
        (*shm)[i].maxloop = 1;
    }

    // Close the Shared memory
    shmdt (shm);
    return (-1);
}

// Returns:
// - 0: at least one zone is not infinite and is on its first loop
// - 1: all zones are infinite or are on their N loop (N>1)
int shm_getLoopStatus ()
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;
    int allZonesHaveLoop = 1;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (-1);
    }

    // Mark all zone with maxloop = 1
    for (i = 0 ; i < ZONE_NBMAX && allZonesHaveLoop ; i++)
    {
        if ((*shm)[i].loop == LOOP_FIRSTTIME)
            allZonesHaveLoop = 0;
    }

    // Close the Shared memory
    shmdt (shm);
    return (allZonesHaveLoop);
}

int shm_stopZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set status to "Playing"
            (*shm)[i].playlist.status = PLAYLIST_STOP;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_killZone (char * name)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set status to "Playing"
            (*shm)[i].playlist.status = PLAYLIST_KILL;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_setCurrentMedia (char * name, int current)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Zones memory. SHM Error.");            
        return (0);
    }

    // Search for the zone with the same name
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strncmp (name, (*shm)[i].name, ZONE_MAXLENGTH) == 0)
        {
            // Set the current index
            (*shm)[i].playlist.current = current;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_createAppsSM ()
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;

    // Create the shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, IPC_CREAT | 0666)) < 0)
        return (0);
    
    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }
    
    // Blank it
    memset (shm, '\0', SHM_APPSSIZE);    

    // Close it
    shmdt (shm);
    return (shmid);
}

int shm_saveApp (sApp * app)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char*)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }

    // Search for an existing app with the same WID, override it
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        if (app->wid == (*shm)[i].wid)
        {
            iLog ("Overriding App with WID '%d' in memory", app->wid);
            memset (&((*shm)[i].zone), '\0', ZONE_MAXLENGTH);
            strncpy ((*shm)[i].zone, app->zone, ZONE_MAXLENGTH-1);
            memset (&((*shm)[i].item), '\0', LINE_MAXLENGTH);
            strncpy ((*shm)[i].item, app->item, LINE_MAXLENGTH-1);
            memset (&((*shm)[i].plugin), '\0', PLUGIN_MAXLENGTH);
            strncpy ((*shm)[i].plugin, app->plugin, PLUGIN_MAXLENGTH-1);
            (*shm)[i].pid = app->pid;
            shmdt (shm);
            return (1);
        }
    }

    // Write the new zone
    i = 0;
    while (i < APPS_NBMAX && (*shm)[i].wid)
        i++;

    if (i >= APPS_NBMAX)
    {
        iError ("Can't add App with WID '%d' : maximum already reached",
                app->wid);
        shmdt (shm);
        return (0);
    }

    (*shm)[i].wid = app->wid;
    memset (&((*shm)[i].zone), '\0', ZONE_MAXLENGTH);
    strncpy ((*shm)[i].zone, app->zone, ZONE_MAXLENGTH-1);
    memset (&((*shm)[i].item), '\0', LINE_MAXLENGTH);
    strncpy ((*shm)[i].item, app->item, LINE_MAXLENGTH-1);
    memset (&((*shm)[i].plugin), '\0', PLUGIN_MAXLENGTH);
    strncpy ((*shm)[i].plugin, app->plugin, PLUGIN_MAXLENGTH-1);
    (*shm)[i].pid = app->pid;

    // Close the Shared memory
    shmdt (shm);
    return (1);
}

int shm_getApp (int wid, sApp * app)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }

    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        
        if (wid == (*shm)[i].wid)
        {
            memset (app->zone, '\0', ZONE_MAXLENGTH);
            strncpy (app->zone, (*shm)[i].zone,ZONE_MAXLENGTH-1);
            memset (app->item, '\0', LINE_MAXLENGTH);
            strncpy (app->item, (*shm)[i].item, LINE_MAXLENGTH-1);
            memset (app->plugin, '\0', PLUGIN_MAXLENGTH);
            strncpy (app->plugin, (*shm)[i].plugin, PLUGIN_MAXLENGTH-1);
            app->pid = (*shm)[i].pid;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_delApp (int wid)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }


    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        
        if (wid == (*shm)[i].wid)
        {
            memset (&(*shm)[i], '\0', sizeof (sApp));
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_setAppStatus (int wid, int status)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }

    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        
        if (wid == (*shm)[i].wid)
        {
            (*shm)[i].status = status;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_getAppStatus (int wid)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i, result;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (-1);
    }

    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        
        if (wid == (*shm)[i].wid)
        {
            result = (*shm)[i].status;
            shmdt (shm);
            return (result);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (-1);
}

int shm_setAppPID (int wid, int pid)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }

    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        if (wid == (*shm)[i].wid)
        {
            (*shm)[i].pid = pid;
            shmdt (shm);
            return (1);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}

int shm_getAppPID (int wid)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i, result;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (-1);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (-1);
    }

    // Search for the apps with the same WID
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        
        if (wid == (*shm)[i].wid)
        {
            result = (*shm)[i].pid;
            shmdt (shm);
            return (result);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (-1);
}

int shm_setAppsStatus (char * zone, int status)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_APPSSIZE;
    sApps * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_APPS, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sApps *)shmat (shmid, NULL, 0)) == (char *)-1)
    {   
        iError("Can't get the Apps memory. SHM Error.");            
        return (0);
    }

    // Search for the apps with the same Zone
    for (i = 0 ; i < APPS_NBMAX ; i++)
    {
        if (strncmp (zone, (*shm)[i].zone, ZONE_MAXLENGTH) == 0)
        {
            shm_setAppStatus ((*shm)[i].wid, status);
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (0);
}
