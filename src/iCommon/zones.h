// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/zones.h
 * Description: Structure to manage iPlayer's Zones
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.30: Original revision
 *****************************************************************************/


#ifndef __ZONES__
#define __ZONES__

#include "config.h"
#include "playlist.h"

//#define ZONE_NBMAX              24 <= defined in config.h

#define LOOP_UNKNOWN            0
#define LOOP_INFINITE           1
#define LOOP_FIRSTTIME          2
#define LOOP_NTIMES             3

typedef struct zone {

    char name [ZONE_MAXLENGTH];

    int hidden;
    int layer;
    int x;
    int y;
    int width;
    int height;
    int format;

    sPlaylist playlist;

    int maxloop;
    int loop;
    int pid;

    int status;

} sZone;

typedef sZone sZones [ZONE_NBMAX];

#endif // __ZONES__
