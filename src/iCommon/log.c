// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/log.c
 * Description: Provide logging functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include <syslog.h>
#include <stdarg.h>

#include "log.h"

void iDebug (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL4|LOG_DEBUG, message, ap);
    va_end (ap);
}

void iInfo (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL4|LOG_INFO, message, ap);
    va_end (ap);
}

void iLog (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL4|LOG_NOTICE, message, ap);
    va_end (ap);
}

void iError (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL4|LOG_ERR, message, ap);
    va_end (ap);
}

void iCrit (char * message, ...)
{
    va_list ap;

    va_start (ap, message);
    vsyslog (LOG_LOCAL4|LOG_CRIT, message, ap);
    va_end (ap);
}
