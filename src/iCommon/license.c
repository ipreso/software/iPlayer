// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/license.c
 * Description: Provide license functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.28: Original revision
 *****************************************************************************/

#include <string.h>
#include <stdio.h>
#include <openssl/md5.h>

#include "license.h"

#define HDPARM  "/sbin/hdparm"
#define NVME    "/usr/sbin/nvme"

char * getMac (sConfig * config, char * mac, int size)
{
    char command [CMDLINE_MAXLENGTH];
    FILE * fp;

    memset (mac, '\0', size);
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "cat /sys/class/net/%s/address",
                config->license_net);
    
    fp = popen (command, "r");
    if (!fp)
        return (NULL);

    if (!fgets (mac, size, fp))
    {
        pclose (fp);
        return (NULL);
    }
    while (mac [strlen(mac)-1] == '\n' ||
           mac [strlen(mac)-1] == '\t' ||
           mac [strlen(mac)-1] == ' ')
        mac [strlen(mac)-1] = '\0';
    
    pclose (fp);
    return (mac);
}

char * getHDD (sConfig * config, char * sn, int size)
{
    char command [CMDLINE_MAXLENGTH];
    FILE * fp;

    memset (sn, '\0', size);
    memset (command, '\0', sizeof (command));

    // Try to get SN from hdparm tool
    snprintf (command, sizeof (command) - 1,
                "sudo %s -i %s 2>/dev/null | grep SerialNo | cut -d'=' -f4",
                HDPARM, config->license_hd);
    
    fp = popen (command, "r");
    if (!fp)
        return (NULL);

    if (!fgets (sn, size, fp))
    {
        pclose (fp);
        iDebug ("Cannot get HDD Serial from %s", HDPARM);
    }
    else
    {
        while (sn [strlen(sn)-1] == '\n' ||
               sn [strlen(sn)-1] == '\t' ||
               sn [strlen(sn)-1] == ' ')
            sn [strlen(sn)-1] = '\0';
        
        pclose (fp);

        if (strlen (sn))
            return (sn);
    }

    // Hdparm can't get SN of the disk.
    // Try to get SN from nvme tool.

    memset (sn, '\0', size);
    memset (command, '\0', sizeof (command));

    snprintf (command, sizeof (command) - 1,
                "sudo %s id-ctrl %s 2>/dev/null | " \
                "grep -Ei '^sn' | " \
                "cut -d':' -f2 | " \
                "awk '{print $1 }'",
                NVME,
                config->license_hd);
    fp = popen (command, "r");
    if (!fp)
        return (NULL);
    if (!fgets (sn, size, fp))
    {
        iDebug ("Cannot get HDD Serial from %s", NVME);
        pclose (fp);
        return (NULL);
    }
    pclose (fp);
    while (sn [strlen(sn)-1] == '\n' ||
           sn [strlen(sn)-1] == '\t' ||
           sn [strlen(sn)-1] == ' ')
        sn [strlen(sn)-1] = '\0';

    return (sn);
}

char * getHash (sConfig * config, char * hash, int size)
{
    char hdserial [64];
    char mac [64];
    char buffer [256];
    MD5_CTX context;
    unsigned char digest[16];

    if (!getHDD (config, hdserial, sizeof (hdserial)))
        return (NULL);
    iDebug ("HDD SN: '%s'", hdserial);

    if (!getMac (config, mac, sizeof (mac)))
        return (NULL);
    iDebug ("MAC: '%s'", mac);

    // Use SN from Hdparm
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer), "%%%s-%s%%", mac, hdserial);

    // Hash the key
    MD5_Init (&context);
    MD5_Update (&context, buffer, strlen (buffer));
    MD5_Final (digest, &context);
    snprintf (buffer, sizeof (buffer)-1, 
           "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
           digest [0],
           digest [1],
           digest [2],
           digest [3],
           digest [4],
           digest [5],
           digest [6],
           digest [7],
           digest [8],
           digest [9],
           digest [10],
           digest [11],
           digest [12],
           digest [13],
           digest [14],
           digest [15]);

    memset (hash, '\0', size);
    strncpy (hash, buffer, size-1);
    return (hash);
}

int license_init (sConfig * config)
{
    char buffer [EVENT_MAXLENGTH];
    char hash [HASH_MAXLENGTH+1];
    FILE * fp;
    int status;

    if (!getHash (config, hash, sizeof (hash)))
        return (-1);

    // If uncrypted key already exists, remove it
    snprintf (buffer, sizeof (buffer) - 1,
                "rm -f '%s/%s'", config->path_tmp, UNCRYPTED_LICENSE);
    system (buffer);

    iInfo ("Hardware ID : '%s'", hash);

    if ((fp = fopen (config->path_license, "r")) == NULL)
    {
        iError ("No license key.");
        return (0);
    }
    else
    {
        fclose (fp);
    }

    // With the Hash key, uncrypt the license's key
    memset (buffer, '\0', sizeof (buffer));                                   
    snprintf (buffer, sizeof (buffer) - 1,
                "echo \"%s\" | %s --homedir '%s' --passphrase-fd 0 --batch " \
                "--no-tty --output '%s/%s' -d '%s' 2>/dev/null", 
                hash,
                GPG_PATH,
                config->path_tmp,
                config->path_tmp,
                UNCRYPTED_LICENSE,
                config->path_license);
    system (buffer);

    // If the output file exists, license's key is correct
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, "%s/%s", 
              config->path_tmp, 
              UNCRYPTED_LICENSE);
    if ((fp = fopen (buffer, "r")) == NULL)
    {
        // Hash key is incorrect
        iError ("License check failed: Host invalid.");
        return (0);
    }
    fclose (fp);

    // Check the file is signed by the CA
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, 
          "openssl smime -verify -in %s/%s -CApath %s -out %s/%s 2>/dev/null", 
              config->path_tmp, UNCRYPTED_LICENSE,
              config->path_ca,
              config->path_tmp, CHECKED_LICENSE);
    if ((fp = popen (buffer, "r")) == NULL)
    {
        iError ("Error while verifying license signature.");
        return (0);
    }
    status = pclose (fp);
    if (status == 4)
    {
        iError ("License has expired.");
        // Remove the license files
        unlink (CHECKED_LICENSE);
        unlink (UNCRYPTED_LICENSE);
        unlink (config->path_license);
        unlink (config->path_certificate);
        snprintf (buffer, sizeof (buffer) - 1,
                  "echo \"License has expired.\" > \"%s\"",
                  REASON_FILE);
        system (buffer);
        return (0);
    }
    if (status != 0)
    {
        iError ("Error while checking License file.");
        unlink (CHECKED_LICENSE);
        unlink (UNCRYPTED_LICENSE);
        unlink (config->path_license);
        unlink (config->path_certificate);
        snprintf (buffer, sizeof (buffer) - 1,
                  "echo \"Invalid license.\" > \"%s\"",
                  REASON_FILE);
        system (buffer);
        return (0);
    }

    return (1);
}

char * getFileContent (sConfig * config, char * filename,
                       char * buffer, int size)
{
    char command [CMDLINE_MAXLENGTH];
    int filled = 1;
    int status;
    FILE * fp = NULL;

    if (!config || !filename)
        return (NULL);

    fp = fopen (filename, "r");
    if (!fp)
    {
        iError ("File '%s' doesn't exist", filename);
        return (NULL);
    }
    fclose (fp);

    // Checking signature
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command)-1,
                "%s smime -verify -in '%s' -CApath '%s' -out '%s.' 2>/dev/null",
                OPENSSL_PATH,
                filename,
                config->path_ca,
                filename);
    //iDebug ("%s", command);
    system (command);

    // Decrypting the file
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof(command)-1,
                "%s smime -decrypt -in '%s.' -inkey '%s' " \
                "-passin 'pass:%s' 2>/dev/null",
                OPENSSL_PATH,
                filename,
                config->path_certificate,
                config->license_pass);
    
    //iDebug ("%s", command);
    fp = popen (command, "r");
    if (!fp)
    {
        iError ("Error while decrypting file '%s' (1)", filename);
        // Force update of our license
        memset (command, '\0', sizeof (command));
        snprintf (command, sizeof (command) - 1,
                    "%s -c '%s' --force-license",
                    config->path_isynchro,
                    config->filename);
        system (command);
        // Ask the composer to download an up-to-date license
        memset (command, '\0', sizeof (command));
        snprintf (command, sizeof (command) - 1,
                    "%s -c '%s' --updlicense",
                    config->path_isynchro,
                    config->filename);
        system (command);
        return (NULL);
    }

    memset (buffer, '\0', size);
    while (filled < size && 
            !feof (fp) && 
            fgets (buffer + filled-1, size - filled, fp))
    {
        filled = strlen (buffer) +1;
    }

    status = pclose (fp);
    if (status != 0)
    {
        iError ("Error while decrypting file '%s' (2)", filename);
        // Force update of our license
        memset (command, '\0', sizeof (command));
        snprintf (command, sizeof (command) - 1,
                    "%s -c '%s' --force-license",
                    config->path_isynchro,
                    config->filename);
        system (command);
        // Ask the composer to download an up-to-date license
        memset (command, '\0', sizeof (command));
        snprintf (command, sizeof (command) - 1,
                    "%s -c '%s' --updlicense",
                    config->path_isynchro,
                    config->filename);
        system (command);
        return (NULL);
    }

    return (buffer);
}

// Return 1 if license is valid
//        0 otherwise
int checkLicenseValidity (sConfig * config)
{
    char command [CMDLINE_MAXLENGTH];
    FILE * fp = NULL;
    char buffer [LINE_MAXLENGTH];
    int size = sizeof (buffer);
    int filled = 1;
    int status;

    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "%s verify -no-CApath -CApath %s -show_chain %s",
                OPENSSL_PATH,
                config->path_ca,
                config->path_certificate);
    fp = popen (command, "r");
    if (!fp)
    {
        iError ("Error while checking validity for '%s' (1)", config->path_certificate);
        return (0);
    }

    memset (buffer, '\0', size);
    while (filled < size && 
            !feof (fp) && 
            fgets (buffer + filled-1, size - filled, fp))
    {
        iDebug ("%s", buffer + filled-1);
        filled = strlen (buffer) +1;
    }

    status = pclose (fp);
    if (status != 0)
    {
        iError ("Error while checking validity for '%s' (2)", config->path_certificate);
        return (0);
    }

    // Certificate is valid
    return (0);
}
