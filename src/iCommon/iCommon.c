// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/iCommon.c
 * Description: Provide common functions to the iPreso Player applications
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.06: Added Program memorization
 *  - 2009.09.09: Added sParams structure operations
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>

#include "iCommon.h"
#include "log.h"
#include "SHM.h"

// Return:
// ID of the Shared memory if everything is fine
// Negative value on errors
int common_loadConfig (const char * filename)
{
    int shmid;
    sConfig config;
    sConfig * pConfig = NULL;
    FILE * fp = fopen (filename, "r");

    if (!fp)
    {
        // File doesn't exist or is not readable
        iError ("Can't read configuration file '%s'", filename);
        return (-1);
    }

    // Create or replace Shared Memory
    if ((shmid = shm_createConfigSM (MEMKEY_CONFIGURATION)) < 0)
    {
        fclose (fp);
        iError ("Can't load configuration file. SHM Error.");
        return (-2);
    }

    // Parse the configuration file
    memset (&config, '\0', sizeof (config));
    strncpy (config.filename, filename, FILENAME_MAXLENGTH-1);
    if (parseConfigurationFile (fp, &config) <= 0)
    {
        fclose (fp);
        iError ("Can't load configuration file. Parse Error.");
        return (-3);
    }
    fclose (fp);

    // Put values in Shared memory
    pConfig = shm_getConfigSM (MEMKEY_CONFIGURATION);
    memcpy (pConfig, &config, sizeof (config));
    shm_closeConfigSM (pConfig);

    return (shmid);
}

// Return:
// 1 if config is sucessfully loaded into 'config' structure
// 0 or negative value in case of error
int common_getConfig (const char * filename, sConfig * config)
{
    int shmid;
    char * shm              = NULL;
    sConfig * configInSM    = NULL;

    // Address of the config cannot be NULL
    if (!config)
        return (0);

    // Look if shared memory is already created
    if ((configInSM = shm_getConfigSM (MEMKEY_CONFIGURATION)) == 0)
    {
        // No, we can parse the file
        if ((shmid = common_loadConfig (filename)) < 0)
            return (0);

        // And try to reopen the configuration
        configInSM = shm_getConfigSM (MEMKEY_CONFIGURATION);
    }

    // Is it the correct configuration file ?
    if (strncmp (configInSM->filename, filename, FILENAME_MAXLENGTH) != 0)
    {
        iError ("Can't load configuration file '%s'", filename);
        iError ("Configuration '%s' is already loaded. Using it.", 
                    configInSM->filename);
    }

    // Copy the content
    memcpy (config, configInSM, SHM_CONFIGSIZE);

    // Detach the shared memory
    shm_closeConfigSM (configInSM);

    return (1);
}

// Return:
// 1 if configuration is successfully parsed into 'config' structure
// 0 or negatie value in case of error
int parseConfigurationFile (FILE * fp, sConfig * config)
{
    int status = 1;
    char name [NAME_MAXLENGTH];
    char value [VALUE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    int iLine = 0;
    char * index = NULL;
    int i, j;

    if (!fp)
    {
        iError ("Bad file handler.");
        return (0);
    }

    iLog ("Parsing configuration file '%s'", config->filename);

    while (status && !feof (fp))
    {
        memset (line, '\0', LINE_MAXLENGTH);
        iLine++;
        if (!fgets ((char *)&line, LINE_MAXLENGTH, fp))
        {
            // Empty line
            continue;
        }

        if (strlen(line) < 1 || line[0] == '\n')
        {
            // Empty line
            continue;
        }

        if (line[0] == '#')
        {
            // Ignore comments
            continue;
        }

        // Parse name=value
        memset (name, '\0', NAME_MAXLENGTH);
        memset (value, '\0', VALUE_MAXLENGTH);
        index = strchr (line, '=');
        if (!index)
        {
            // Only space or tabs ? Or first character is '#' ?
            i = 0;
            while (i < strlen (line) &&
                    line [i] == ' ' || line [i] == '\t' || line [i] == '\n')
                i++;

            if (i < strlen (line) && line [i] != '#')
            {
                iError ("Bad syntax in configuration, line : %d", iLine);
                status = -1;
                break;
            }
            else
            {
                // Emtpy or commented line
                continue;
            }
        }
        i = 0;
        while (i < (index - line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the name
            i++;
        }
        j = index - line - 1;
        while (j > i && (line [j] == ' ' || line [j] == '\t'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (name, &(line [i]), j-i +1);

        i = index - line + 1;
        while (i < strlen(line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the value
            i++;
        }
        j = strlen (line) - 1;
        while (j > i && 
               (line [j] == ' ' || line [j] == '\t' || line [j] == '\n'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (value, &(line [i]), j-i +1);
        // '#' character is forbidden in value field
        index = strchr (value, '#');
        if (index)
        {
            *index = '\0';
            index--;

            // Value is ending at the first non-blank char. before #
            while (index > value && (*index == '\t' || *index == ' '))
            {
                *index = '\0';
                index--;
            }
        }

        if (!saveConfKey (config, name, value))
        {
            status = -2;
            break;
        }
    }

    return (status);
}

int common_saveConfig (const char * filename, sConfig * config)
{
    int shmid;
    char * shm              = NULL;
    sConfig * configInSM    = NULL;

    // Address of the config cannot be NULL
    if (!config)
        return (0);

    // Look if shared memory is already created
    if ((configInSM = shm_getConfigSM (MEMKEY_CONFIGURATION)) == 0)
    {
        // No, we can parse the file
        if ((shmid = common_loadConfig (filename)) < 0)
            return (-1);

        // And try to reopen the configuration
        configInSM = shm_getConfigSM (MEMKEY_CONFIGURATION);
    }

    // Is it the correct configuration file ?
    if (strncmp (configInSM->filename, filename, FILENAME_MAXLENGTH) != 0)
    {
        iError ("Can't save configuration file '%s'", filename);
        iError ("Configuration '%s' is already loaded. Can't override it.", 
                    configInSM->filename);
        return (-2);
    }

    // Copy the content
    memcpy (configInSM, config, SHM_CONFIGSIZE);

    // Detach the shared memory
    shm_closeConfigSM (configInSM);

    return (1);
}

int common_lockIWM (sConfig * config)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    while ((*shm).semaphore && i++ < (*shm).timeout_windowload*100+1)
    {
        // Try every 1/10s
        usleep (1000*100);
    }

    if ((*shm).semaphore)
    {
        shmdt (shm);
        return (0);
    }
    else
    {
        (*shm).semaphore = 1;
        config->semaphore = 1;
        shmdt (shm);
        return (1);
    }
}

int common_unlockIWM (sConfig * config)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    (*shm).semaphore = 0;
    config->semaphore = 0;
    shmdt (shm);
    return (1);
}

int getResolution (sConfig * config, int * width, int * height)
{
    char buffer [CMDLINE_MAXLENGTH];
    FILE * fp;

    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -r", config->path_iwm);
    fp = popen (buffer, "r");
    if (!fp)
        return (0);
    if (fscanf (fp, "%dx%d", width, height) != 2)
    {
        pclose (fp);
        return (0);
    }
    pclose (fp);
    return (1);
}

int getParam (sConfig * config, char * line, 
              char * param, char * value, int size)
{
    int i;
    int found;
    int length;
    char * index;


    if (!line || !param || !value)
        return (0);


    for (i = 0, found = 0 ; !found &&
                            i + strlen (param) < strlen (line) ; i++)
    {
        if (strncmp (line+i, param, strlen (param)) == 0)
        {
            // Check it is not a part of something else
            if (((i > 1 && line [i-1] == ' ') || (i <= 1)) &&
                (i + strlen (param) + 1 < strlen (line) &&
                 line [i + strlen (param)] == '='))
            {
                found = 1;
            }
        }
    }

    if (!found)
        return (0);

    // Value index
    i = i + strlen (param);
    if (i >= strlen (line) || line[i-1] != '=')
        return (0);

    if (line[i] == '\'' || line[i] == '"')
    {
        // Get the next quote
        i++;
        index = strchr (line+i, '\'');
        if (!index)
            index = strchr (line+i, '"');

        // No simple quote, getting the next space
        if (!index)
            index = strchr (line+i, ' ');
    }
    else
    {
        index = strchr (line+i, ' ');
    }

    if (index)
    {
        // Copying only what is between quote, or until the next space
        if (index - (line+i) > size-1)
            length = size-1;
        else
            length = index - (line+i);
    }
    else
    {
        // Copying all the end of the line
        if (strlen (line+i) > size-1)
            length = size - 1;
        else
            length = strlen (line+i);
    }
    memset (value, '\0', size);
    strncpy (value, line+i, length);
    return (1);
}

// Check if Box has an IP on its license interface
// Returns -1 in case of error
//          0 if there is no IP address on the interface
//          1 if there is a configured IP
int checkIP (sConfig * config)
{
    char command [CMDLINE_MAXLENGTH];
    char result [256];
    FILE * fp = NULL;

    memset (command, '\0', sizeof (command));
    memset (result, '\0', sizeof (command));

    // Check if Box has an IP on its interface
    snprintf (command, sizeof (command) - 1,
        "ip address list dev %s | grep inet",
        config->license_net);

    fp = popen (command, "r");
    if (!fp)
        return (-1);

    if (!fgets (result, sizeof(result)-1, fp))
    {
        iError ("Cannot get IP address.");
        pclose (fp);
        return (-1);
    }
    pclose (fp);

    while (result [strlen (result)-1] == '\n' ||
           result [strlen (result)-1] == '\t' ||
           result [strlen (result)-1] == ' ')
        result [strlen (result)-1] = '\0';

    if (strlen (result))
        return (1);
    else
        return (0);
}

int pluginSelect (const struct direct *entry)
{
    char * index;

    if (strcmp (entry->d_name, ".") == 0 ||
        strcmp (entry->d_name, "..") == 0)
        return (0);

    // Check we have .plugin in the filename
    index = strstr (entry->d_name, ".plugin");
    if (!index)
        return (0);

    // Check that it is the extension
    if (index + strlen (index) == entry->d_name + strlen (entry->d_name))
        return (1);
    else
        return (0);
}

int saveConfKey (sConfig * config, char * name, char * value)
{
    iDebug ("- '%s' => '%s'\n", name, value);

    if (strncmp (name, "application_clock", NAME_MAXLENGTH) == 0)
        strncpy (config->application_clock, value, APPLI_MAXLENGTH-1);
    else if (strncmp (name, "application_scroll", NAME_MAXLENGTH) == 0)
        strncpy (config->application_scroll, value, APPLI_MAXLENGTH-1);
    else if (strncmp (name, "desktop_main", NAME_MAXLENGTH) == 0)
        config->desktop_main = atoi (value);
    else if (strncmp (name, "desktop_load", NAME_MAXLENGTH) == 0)
        config->desktop_load = atoi (value);
    else if (strncmp (name, "desktop_wait", NAME_MAXLENGTH) == 0)
        config->desktop_wait = atoi (value);
    else if (strncmp (name, "desktop_blank", NAME_MAXLENGTH) == 0)
        config->desktop_blank = atoi (value);
    else if (strncmp (name, "license_hd", NAME_MAXLENGTH) == 0)
        strncpy (config->license_hd, value, FILENAME_MAXLENGTH);
    else if (strncmp (name, "license_net", NAME_MAXLENGTH) == 0)
        strncpy (config->license_net, value, VALUE_MAXLENGTH);
    else if (strncmp (name, "license_pass", NAME_MAXLENGTH) == 0)
        strncpy (config->license_pass, value, VALUE_MAXLENGTH-1);
    else if (strncmp (name, "licenses_server", NAME_MAXLENGTH) == 0)
        strncpy (config->licenses_server, value, VALUE_MAXLENGTH-1);
    else if (strncmp (name, "path_ca", NAME_MAXLENGTH) == 0)
        strncpy (config->path_ca, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_certificate", NAME_MAXLENGTH) == 0)
        strncpy (config->path_certificate, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_license", NAME_MAXLENGTH) == 0)
        strncpy (config->path_license, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_iappctrl", NAME_MAXLENGTH) == 0)
        strncpy (config->path_iappctrl, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_ischedule", NAME_MAXLENGTH) == 0)
        strncpy (config->path_ischedule, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_isynchro", NAME_MAXLENGTH) == 0)
        strncpy (config->path_isynchro, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_iwm", NAME_MAXLENGTH) == 0)
        strncpy (config->path_iwm, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_izonemgr", NAME_MAXLENGTH) == 0)
        strncpy (config->path_izonemgr, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_izone", NAME_MAXLENGTH) == 0)
        strncpy (config->path_izone, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_playlists", NAME_MAXLENGTH) == 0)
        strncpy (config->path_playlists, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_library", NAME_MAXLENGTH) == 0)
        strncpy (config->path_library, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_resources", NAME_MAXLENGTH) == 0)
        strncpy (config->path_resources, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_plugins", NAME_MAXLENGTH) == 0)
        strncpy (config->path_plugins, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_pkg", NAME_MAXLENGTH) == 0)
        strncpy (config->path_pkg, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_params", NAME_MAXLENGTH) == 0)
        strncpy (config->path_params, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_tmp", NAME_MAXLENGTH) == 0)
        strncpy (config->path_tmp, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "path_actions", NAME_MAXLENGTH) == 0)
        strncpy (config->path_actions, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_layout", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_layout, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_playlist", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_playlist, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_manifest", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_manifest, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_schedule", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_schedule, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_media", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_media, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "playlist_meta", NAME_MAXLENGTH) == 0)
        strncpy (config->playlist_meta, value, FILENAME_MAXLENGTH-1);
    else if (strncmp (name, "time_logs", NAME_MAXLENGTH) == 0)
        config->time_logs = atoi (value);
    else if (strncmp (name, "time_synchro", NAME_MAXLENGTH) == 0)
        config->time_synchro = atoi (value);
    else if (strncmp (name, "timeout_windowclose", NAME_MAXLENGTH) == 0)
        config->timeout_windowclose = atoi (value);
    else if (strncmp (name, "timeout_windowload", NAME_MAXLENGTH) == 0)
        config->timeout_windowload = atoi (value);
    else
    {
        iError ("Unknown element : '%s' !", name);
        return (0);
    }

    return (1);
}

// Return:
// ID of the Shared memory if everything is fine
// Negative value on errors
int common_loadParams (const char * filename, sConfig * config)
{
    int shmid;
    sParams params;
    sParams * pParams = NULL;
    FILE * fp = fopen (filename, "r");

    if (!fp)
    {
        // File doesn't exist or is not readable
        iError ("Can't read configuration file '%s'", filename);
        return (-1);
    }

    // Create or replace Shared Memory
    if ((shmid = shm_createParamsSM (MEMKEY_PARAMS)) < 0)
    {
        fclose (fp);
        iError ("Can't load configuration file. SHM Error.");
        return (-2);
    }

    // Parse the configuration file
    memset (&params, '\0', sizeof (params));
    strncpy (params.filename, filename, FILENAME_MAXLENGTH-1);
    if (parseParametersFile (fp, &params) <= 0)
    {
        fclose (fp);
        iError ("Can't load configuration file. Parse Error.");
        return (-3);
    }
    fclose (fp);

    // Put values in Shared memory
    pParams = shm_getParamsSM (MEMKEY_PARAMS);
    memcpy (pParams, &params, sizeof (params));
    shm_closeParamsSM (pParams);

    return (shmid);
}

// Return:
// 0 if success
// Negative value on errors
int writeParametersFile (sConfig * config, sParams * params)
{
    char line [LINE_MAXLENGTH];
    FILE * fp = fopen (params->filename, "w");
    if (!fp)
    {
        // File is not writable
        iError ("Can't write configuration file '%s'", params->filename);
        return (-1);
    }

    // Write each parameter
    memset (line, '\0', LINE_MAXLENGTH-1);
    snprintf  (line, LINE_MAXLENGTH-1, "#\ncomposer_host = %s\n",
                strlen (params->composer_host) ? params->composer_host : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "composer_ip = %s\n",
                strlen (params->composer_ip) ? params->composer_ip : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_ip = %s\n",
                strlen (params->box_ip) ? params->box_ip : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_gw = %s\n",
                strlen (params->box_gw) ? params->box_gw : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_dns = %s\n",
                strlen (params->box_dns) ? params->box_dns : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "pkg_src = %s\n",
                strlen (params->pkg_src) ? params->pkg_src : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_program = %s\n",
                strlen (params->box_program) ?  params->box_program : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_name = %s\n",
                strlen (params->box_name) ? params->box_name : "#");
    fwrite (line, strlen (line), 1, fp);
    snprintf  (line, LINE_MAXLENGTH-1, "box_coordinates = %s\n",
                strlen (params->box_coordinates) ? params->box_coordinates : "#");
    fwrite (line, strlen (line), 1, fp);

    fclose (fp);

    // Reload it !
    return (common_loadParams (params->filename, config));
}

// Return:
// 1 if params is sucessfully loaded into 'params' structure
// 0 or negative value in case of error
int common_getParams (const char * filename, sConfig * config,
                                             sParams * params)
{
    int shmid;
    char * shm              = NULL;
    sParams * paramsInSM    = NULL;

    // Address of the config cannot be NULL
    if (!params)
        return (0);

    // Look if shared memory is already created
    if ((paramsInSM = shm_getParamsSM (MEMKEY_PARAMS)) == 0)
    {
        // No, we can parse the file
        if ((shmid = common_loadParams (filename, config)) < 0)
            return (0);
        // Apply the parameters to the system

        // And try to reopen the configuration
        paramsInSM = shm_getParamsSM (MEMKEY_PARAMS);

        // Applying parameters to the system
        if (!applyParameters (config, paramsInSM))
        {
            iError ("Can't apply parameters to the system.");
            return (-4);
        }
    }

    // Is it the correct configuration file ?
    if (strncmp (paramsInSM->filename, filename, FILENAME_MAXLENGTH) != 0)
    {
        iError ("Can't load configuration file '%s'", filename);
        iError ("Configuration '%s' is already loaded. Using it.", 
                    paramsInSM->filename);
    }

    // Copy the content
    memcpy (params, paramsInSM, SHM_PARAMSSIZE);

    // Detach the shared memory
    shm_closeParamsSM (paramsInSM);

    return (1);
}

// Return:
// 1 if configuration is successfully parsed into 'params' structure
// 0 or negatie value in case of error
int parseParametersFile (FILE * fp, sParams * params)
{
    int status = 1;
    char name [NAME_MAXLENGTH];
    char value [VALUE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    int iLine = 0;
    char * index = NULL;
    int i, j;

    if (!fp)
    {
        iError ("Bad file handler.");
        return (0);
    }

    iLog ("Parsing configuration file '%s'", params->filename);

    while (status > 0 && !feof (fp))
    {
        memset (line, '\0', LINE_MAXLENGTH);
        iLine++;
        if (!fgets ((char *)&line, LINE_MAXLENGTH, fp))
        {
            // Empty line
            continue;
        }

        if (strlen(line) < 1 || line[0] == '\n')
        {
            // Empty line
            continue;
        }

        if (line[0] == '#')
        {
            // Ignore comments
            continue;
        }

        // Parse name=value
        memset (name, '\0', NAME_MAXLENGTH);
        memset (value, '\0', VALUE_MAXLENGTH);
        index = strchr (line, '=');
        if (!index)
        {
            // Only space or tabs ? Or first character is '#' ?
            i = 0;
            while (i < strlen (line) &&
                    line [i] == ' ' || line [i] == '\t' || line [i] == '\n')
                i++;

            if (i < strlen (line) && line [i] != '#')
            {
                iError ("Bad syntax in configuration, line : %d", iLine);
                status = -1;
                break;
            }
            else
            {
                // Emtpy or commented line
                continue;
            }
        }
        i = 0;
        while (i < (index - line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the name
            i++;
        }
        j = index - line - 1;
        while (j > i && (line [j] == ' ' || line [j] == '\t'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (name, &(line [i]), j-i +1);

        i = index - line + 1;
        while (i < strlen(line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the value
            i++;
        }
        j = strlen (line) - 1;
        while (j > i && 
               (line [j] == ' ' || line [j] == '\t' || line [j] == '\n'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (value, &(line [i]), j-i +1);
        // '#' character is forbidden in value field
        index = strchr (value, '#');
        if (index)
        {
            *index = '\0';
            index--;

            // Value is ending at the first non-blank char. before #
            while (index > value && (*index == '\t' || *index == ' '))
            {
                *index = '\0';
                index--;
            }
        }

        if (!saveParamKey (params, name, value))
        {
            status = -2;
            break;
        }
    }

    return (status);
}

// Return:
// 1 if configuration is successfully parsed into 'params' structure
// 0 or negatie value in case of error
int parseNewParameters (char * buffer, sParams * params)
{
    int status = 1;
    char name [NAME_MAXLENGTH];
    char value [VALUE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    int iLine = 0;
    char * index = NULL;
    int i, j;
    int posIndex = 0;
    char * posNewLine = NULL;

    if (!buffer)
    {
        iError ("Bad string buffer.");
        return (0);
    }

    iDebug ("Parsing parameters data...");

    while (status > 0 && posIndex < strlen (buffer))
    {
        memset (line, '\0', LINE_MAXLENGTH);
        iLine++;

        posNewLine = strchr (buffer+posIndex, '\n');
        if (posNewLine == NULL)
            posNewLine = buffer + strlen (buffer);

        if (posNewLine == buffer+posIndex)
        {
            posIndex++;
            continue;
        }
        strncpy (line, buffer+posIndex, posNewLine - (buffer+posIndex));

        if (strlen(line) < 1 || line[0] == '\n')
        {
            // Empty line
            posIndex++;
            continue;
        }

        if (line[0] == '#')
        {
            // Ignore comments
            posIndex += posNewLine - (buffer+posIndex);
            continue;
        }

        if (strlen (line) > 2 &&
            (line[0] == 'S' || line[0] == 'D' || line[0] == 'C') &&
            line [1] == ':')
        {
            // Status, Details or Command
            posIndex += posNewLine - (buffer+posIndex);
            continue;
        }

        // Parse name=value
        memset (name, '\0', NAME_MAXLENGTH);
        memset (value, '\0', VALUE_MAXLENGTH);
        index = strchr (line, '=');
        if (!index)
        {
            // Only space or tabs ? Or first character is '#' ?
            i = 0;
            while (i < strlen (line) &&
                    line [i] == ' ' || line [i] == '\t' || line [i] == '\n')
                i++;

            if (i < strlen (line) && line [i] != '#')
            {
                iError ("Bad syntax in configuration, line : %d", iLine);
                status = -1;
                break;
            }
            else
            {
                // Emtpy or commented line
                posIndex += posNewLine - (buffer+posIndex);
                continue;
            }
        }
        i = 0;
        while (i < (index - line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the name
            i++;
        }
        j = index - line - 1;
        while (j > i && (line [j] == ' ' || line [j] == '\t'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (name, &(line [i]), j-i +1);

        i = index - line + 1;
        while (i < strlen(line) && (line [i] == ' ' || line [i] == '\t'))
        {
            // Strip blank characters at the beginning of the value
            i++;
        }
        j = strlen (line) - 1;
        while (j > i && 
               (line [j] == ' ' || line [j] == '\t' || line [j] == '\n'))
        {
            // Strip blank characters at the end of the name
            j--;
        }
        strncpy (value, &(line [i]), j-i +1);
        // '#' character is forbidden in value field
        index = strchr (value, '#');
        if (index)
        {
            *index = '\0';
            index--;

            // Value is ending at the first non-blank char. before #
            while (index > value && (*index == '\t' || *index == ' '))
            {
                *index = '\0';
                index--;
            }
        }

        if (!saveParamKey (params, name, value))
        {
            status = -2;
            break;
        }
        posIndex += posNewLine - (buffer+posIndex);
    }

    return (status);
}

int saveParamKey (sParams * params, char * name, char * value)
{
    iDebug ("- '%s' => '%s'\n", name, value);

    if (strncmp (name, "composer_host", NAME_MAXLENGTH) == 0)
        strncpy (params->composer_host, value, HOST_MAXLENGTH-1);
    else if (strncmp (name, "composer_ip", NAME_MAXLENGTH) == 0)
        strncpy (params->composer_ip, value, IP_MAXLENGTH-1);
    else if (strncmp (name, "box_ip", NAME_MAXLENGTH) == 0)
        strncpy (params->box_ip, value, IP_MAXLENGTH-1);
    else if (strncmp (name, "box_gw", NAME_MAXLENGTH) == 0)
        strncpy (params->box_gw, value, IP_MAXLENGTH-1);
    else if (strncmp (name, "box_dns", NAME_MAXLENGTH) == 0)
        strncpy (params->box_dns, value, IP_MAXLENGTH-1);
    else if (strncmp (name, "box_name", NAME_MAXLENGTH) == 0)
        strncpy (params->box_name, value, HOST_MAXLENGTH-1);
    else if (strncmp (name, "box_program", NAME_MAXLENGTH) == 0)
        strncpy (params->box_program, value, NAME_MAXLENGTH-1);
    else if (strncmp (name, "box_coordinates", NAME_MAXLENGTH) == 0)
        strncpy (params->box_coordinates, value, COORD_MAXLENGTH-1);
    else if (strncmp (name, "pkg_src", NAME_MAXLENGTH) == 0)
        strncpy (params->pkg_src, value, HOST_MAXLENGTH-1);
    else
    {
        iError ("Unknown element : '%s' !", name);
        return (0);
    }

    return (1);
}

int applyParameters (sConfig * config, sParams * params)
{
    char hash [HASH_MAXLENGTH+1];
    char buffer [CMDLINE_MAXLENGTH];
    memset (hash, '\0', sizeof (hash));

    if (!getHash (config, hash, sizeof (hash)))
        strncpy (hash, "Unknown_Box", sizeof (hash)-1);

    if (!params)
        return (0);

    // /etc/hosts file:
    // * Association for Localhost
    //  - if no localhost line, add it
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "if [ -z \"`grep localhost /etc/hosts`\" ] ; then "           \
              "    echo \"127.0.0.1    localhost iBox\" > /etc/hosts ; "    \
              "fi");
    system (buffer);
    //  - if no iBox line, add it
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "if [ -z \"`grep iBox /etc/hosts`\" ] ; then "                \
              "    echo \"127.0.0.1    localhost iBox\" > /etc/hosts ; "    \
              "fi");
    system (buffer);
    //  - add the association localhost / Player FQDN (<hash>.ipreso.com)
    //    (used by rsyslog to send correct hostname)
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "sed -r 's/^127.0.0.1.*/127.0.0.1    %s.ipreso.com localhost iBox/g' /etc/hosts > /tmp/hosts ; " \
              "cat /tmp/hosts > /etc/hosts",
              hash);
    system (buffer);

    // * Association with Composer
    //  - if a line with the Composer name or "composer" already exists, delete it
    //  - if a line with the IP of the Composer already exists, delete it
    //  - add the association Composer IP / Composer FQDN / Composer
    snprintf (buffer, sizeof (buffer) - 1,
              "sed -r '/%s/I d' /etc/hosts | "                      \
              "sed -r '/%s/I d' | "                                 \
              "sed -r '/composer/I d' > /tmp/hosts ; "              \
              "cat /tmp/hosts > /etc/hosts ; "                      \
              "echo \"%s    players.%s composer\" >> /etc/hosts ; ",
                params->composer_host,
                params->composer_ip,
                params->composer_ip,
                params->composer_host);
    system (buffer);
    
    // iBox network configuration:
    //  - Kill potential DHCP Client
    //  - if no IP, DHCP will serve all our need.
    //  - if IP exists, modify with the 'ip' binary our network configuration
    //    and write the DNS in the /etc/resolv.conf file.
    if (!strlen (params->box_ip))
    {
        // If link is up, wait to get a reply.
        // If link is down, launch dhclient in background, no need
        // to wait for an IP...
        snprintf (buffer, sizeof (buffer) - 1,
                  "sudo pkill dhclient 2>/dev/null ; "              \
                  "sudo /sbin/mii-tool %s | grep -q \"no link\" 2>/dev/null 1>&2 ; "      \
                  "if [ \"$?\" -ne \"0\" ]; then "                  \
                  "sudo dhclient %s 2>/dev/null 1>&2 ; "            \
                  "else "                                           \
                  "`sudo dhclient %s 2>/dev/null 1>&2 &` ; "        \
                  "fi ;",
                  config->license_net,
                  config->license_net,
                  config->license_net);
    }
    else
    {
        snprintf (buffer, sizeof (buffer) - 1,
                  "sudo pkill dhclient 2>/dev/null ; "              \
                  "sudo ip addr flush dev %s ; "                    \
                  "sudo ip addr add %s dev %s ; "                   \
                  "if [ -n \"%s\" ]; then "                         \
                  "sudo ip route add default via %s dev %s ; "      \
                  "fi; "                                            \
                  "if [ -n \"%s\" ]; then "                         \
                  "echo \"nameserver %s\" > /etc/resolv.conf ; "    \
                  "fi; ",
                  config->license_net,
                  params->box_ip, config->license_net,
                  params->box_gw,
                  params->box_gw, config->license_net,
                  params->box_dns,
                  params->box_dns);
    }
    system (buffer);

    // Ip changed ? /etc/hosts changed ? We restart or reload syslog-ng
    // and stunnel, in order to reconnect to the correct remote logging server
    snprintf (buffer, sizeof (buffer) - 1,
                "sudo service rsyslog restart 2>&1 1>/dev/null");

    iLog ("Parameters applied to the system.");
    return (1);
}

int common_setProgram (sConfig * config, sProgram * newProgram)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    memcpy (&((*shm).currentProgram), newProgram, sizeof (sProgram));
    memcpy (&(config->currentProgram), newProgram, sizeof (sProgram));
    shmdt (shm);
    return (1);
}
int common_setDWProgram (sConfig * config, sProgram * newProgram)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    memcpy (&((*shm).dwProgram), newProgram, sizeof (sProgram));
    memcpy (&(config->dwProgram), newProgram, sizeof (sProgram));
    shmdt (shm);
    return (1);
}

int common_flushProgram (sConfig * config)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    memset (&((*shm).currentProgram), '\0', sizeof (sProgram));
    memset (&(config->currentProgram), '\0', sizeof (sProgram));
    shmdt (shm);
    return (1);
}
int common_flushDWProgram (sConfig * config)
{
    int shmid;
    int shmflag;
    int shmsize = SHM_CONFIGSIZE;
    sConfig * shm;
    int i = 0;

    if ((shmid = shmget (MEMKEY_CONFIGURATION, shmsize, 0666)) < 0)
        return (0);

    if ((char *)(shm = (sConfig *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Config memory. SHM Error.");
        return (0);
    }

    memset (&((*shm).dwProgram), '\0', sizeof (sProgram));
    memset (&(config->dwProgram), '\0', sizeof (sProgram));
    shmdt (shm);
    return (1);
}

int common_getFileMD5 (const char * path, char * buffer, int size)
{
    char command [CMDLINE_MAXLENGTH];
    FILE * fp;

    memset (buffer, '\0', size);
    memset (command, '\0', sizeof (command));
    
    snprintf (command, sizeof (command) - 1,
                "%s '%s' | cut -f1 -d' '",
                CMD_MD5SUM, path);

    fp = popen (command, "r");
    if (!fp)
        return (0);

    if (!fgets (buffer, size, fp))
    {
        pclose (fp);
        return (0);
    }

    while (buffer [strlen (buffer) - 1] == '\n' ||
           buffer [strlen (buffer) - 1] == '\t' ||
           buffer [strlen (buffer) - 1] == ' ')
        buffer [strlen (buffer) - 1] = '\0';

    pclose (fp);
    return (1);
}

int common_getStringMD5 (const char * string, int stringSize,
                         char * buffer, int bufferSize)
{
    MD5_CTX context;
    unsigned char digest[16];

    if (!string || !buffer)
        return (0);

    memset (buffer, '\0', bufferSize);

    MD5_Init (&context);
    MD5_Update (&context, string, stringSize);
    MD5_Final (digest, &context);
    snprintf (buffer, bufferSize,
           "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
           digest [0],
           digest [1],
           digest [2],
           digest [3],
           digest [4],
           digest [5],
           digest [6],
           digest [7],
           digest [8],
           digest [9],
           digest [10],
           digest [11],
           digest [12],
           digest [13],
           digest [14],
           digest [15]);

    return (1);
}
