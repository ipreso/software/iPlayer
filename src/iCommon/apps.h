// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/apps.h
 * Description: Structure to manage iPlayer's Zones
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.30: Original revision
 *****************************************************************************/


#ifndef __APPS__
#define __APPS__

#include "config.h"
#include "zones.h"

#define APPS_NBMAX          (2*ZONE_NBMAX)  // Max 2 apps/zone (current+cache)

typedef struct app {

    char zone   [ZONE_MAXLENGTH];   // Zone where the application stand
    char item   [LINE_MAXLENGTH];   // Line of the playlist, with file and 
                                    // options
    char plugin [PLUGIN_MAXLENGTH]; // Plugin using the application
    int wid;                        // Window ID of the application
    int status;
    int pid;                        // PID of iAppCtrl playing this application

} sApp;

typedef sApp sApps [APPS_NBMAX];

#endif // __APPS__
