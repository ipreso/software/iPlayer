// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/playlist.h
 * Description: Structure to manage Playlist in a zone
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.31: Original revision
 *****************************************************************************/


#ifndef __PLAYLIST__
#define __PLAYLIST__


#define ITEMS_MAXNUMBER         128

#define PLAYLIST_KILL           -1
#define PLAYLIST_STOP           0
#define PLAYLIST_PAUSE          1
#define PLAYLIST_PLAY           2

#include "config.h"

typedef char commandLine [CMDLINE_MAXLENGTH];

typedef struct playlist {

    commandLine items [ITEMS_MAXNUMBER];

    // Status :
    // - 0 : Stopped
    // - 1 : In pause
    // - 2 : Playing
    int status;

    // Item currently played (or last one if stopped)
    int current;

} sPlaylist;

#endif // __PLAYLIST__
