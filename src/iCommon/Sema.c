// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/Sema.c
 * Description: Provide IPC Semaphores functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.02.17: Original revision
 *****************************************************************************/

#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <fcntl.h> 
#include <sys/stat.h>
#include <time.h>

#include "Sema.h"
#include "log.h"

int sema_createLock (char * name)
{
    return (sema_createLockMaxWait (name, 30));
}

int sema_createLockMaxWait (char * name, int seconds)
{
    struct timespec ts;
    sem_t * sSem;

    if (!name)
        return (0);

    if (strncmp (name, "/iplayer.iwm", strlen ("/iplayer.iwm")) == 0)
    {
        //iDebug ("[%d] Bypassing lock of semaphore %s", getpid (), name);
        return (1);
    }

    // Get the semaphore
    sSem = sem_open (name, O_CREAT, 0644, 1);
    if (sSem == SEM_FAILED)
    {
        iError ("[%d] Unable to open semaphore [%s]", getpid (), name);
        return (0);
    }

    if (clock_gettime (CLOCK_REALTIME, &ts) == -1)
    {
        iError ("[%d] Unable to get current time for semaphore [%s]", 
                getpid (), name);
        return (0);
    }
    
    // Maximum time to wait
    ts.tv_sec += seconds;

    // Wait the semaphore to be available and lock it
    if (sem_timedwait (sSem, &ts) == -1)
    {
        iError ("[%d] Unable to lock semaphore [%s] after %d seconds", 
                getpid (), name, seconds);
        return (0);
    }

    //iDebug ("[%d] +++ Locked semaphore %s", getpid (), name);
    return (1);
}

int sema_unlock (char * name)
{
    sem_t * sSem;

    if (!name)
        return (0);

    if (strcmp (name, "/iplayer.iwm") == 0)
    {
        //iDebug ("[%d] Bypassing unlock of semaphore %s", getpid (), name);
        return (1);
    }


    // Get the semaphore
    sSem = sem_open (name, O_CREAT, 0644, 1);
    if (sSem == SEM_FAILED)
    {
        iError ("[%d] Unable to open semaphore [%s]", getpid (), name);
        return (0);
    }

    // Unlock it
    if (sem_post (sSem) == -1)
    {
        iError ("[%d] Unable to unlock semaphore [%s]", getpid(), name);
        return (0);
    }
    //iDebug ("[%d] +++ Unlock semaphore %s", getpid (), name);

    return (1);
}
