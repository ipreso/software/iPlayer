// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/license.h
 * Description: Provide license functions
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.28: Original revision
 *****************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include "config.h"
#include "log.h"

#define GPG_PATH            "/usr/bin/gpg"
#define OPENSSL_PATH        "/usr/bin/openssl"
#define UNCRYPTED_LICENSE   "foo"
#define CHECKED_LICENSE     "bar"

int license_init (sConfig * config);
char * getFileContent (sConfig * config, char * filename,
                       char * buffer, int size);
char * getHash (sConfig * config, char * buffer, int size);
char * getMac  (sConfig * config, char * buffer, int size);
char * getHDD  (sConfig * config, char * buffer, int size);

int checkLicenseValidity (sConfig * config);
