// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iCommon/iCommon.h
 * Description: Provide common functions to the iPreso Player applications
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.06: Added Program memorization
 *  - 2009.09.09: Added sParams structure operations
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include <sys/types.h>
#include <sys/dir.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <dirent.h> 
#include "config.h"
#include "SHM.h"
#include "license.h"

#define CMD_MD5SUM          "/usr/bin/md5sum"

int common_loadConfig       (const char * filename);
int common_loadParams       (const char * filename, sConfig * config);
int common_getConfig        (const char * filename, sConfig * config);
int common_getParams        (const char * filename, sConfig * config,
                                                    sParams * params);
int common_saveConfig       (const char * filename, sConfig * config);
int parseConfigurationFile  (FILE * fp, sConfig * config);
int parseParametersFile     (FILE * fp, sParams * params);
int saveConfKey             (sConfig * config, char * name, char * value);
int saveParamKey            (sParams * params, char * name, char * value);
int common_lockIWM          (sConfig * config);
int common_unlockIWM        (sConfig * config);
int common_setProgram       (sConfig * config, sProgram * newProgram);
int common_setDWProgram     (sConfig * config, sProgram * newProgram);
int common_flushProgram     (sConfig * config);
int common_flushDWProgram   (sConfig * config);
int common_getFileMD5       (const char * path, char * buffer, int size);
int common_getStringMD5     (const char * string, int stringSize,
                             char * buffer, int bufferSize);
int getParam                (sConfig * config, char * line, 
                             char * param, char * value, int size);
int getResolution           (sConfig * config, int * width, int * height);
int checkIP                 (sConfig * config);

int pluginSelect (const struct direct *entry);

int writeParametersFile     (sConfig * config, sParams * params);
int parseNewParameters      (char * buffer, sParams * params);
int applyParameters         (sConfig * config, sParams * params);
