// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iSynchro/iSynchro.h
 * Description: Manage communications with Composer
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.04.15: Added Schedule management
 *  - 2009.10.06: Added the program-change management
 *  - 2009.09.03: Original revision
 *****************************************************************************/

#ifndef VERSION
#define ISYNCHRO_VERSION     "Version Unknown"
#else
#define ISYNCHRO_VERSION     VERSION
#endif

#define ISYNCHRO_APPNAME            "iSynchro"
#define ISYNCHRO_APPDETAIL          "iPlayer Synchronizer"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

#define PATH_WGET                   "/usr/bin/wget"

#define LICENSE_SERVER              "new.certs.ipreso.com"
#define MAX_COMMANDS_TODO       5
#define HTTPACTION_MAXLENGTH    64
#define HTTPPOST_MAXLENGTH      512
#define HTTPREPLY_MAXLENGTH     8196

#define STATUS_UNKNOWN      0
#define STATUS_OK           1
#define STATUS_FAILED       2
#define STATUS_ERROR        3
#define STATUS_PENDING      4
#define STATUS_OUTOFDATE    5
#define STATUS_OFF          6

char * statusStrings [] = {
    "Unknown",
    "Ok",
    "Failed",
    "Error",
    "Pending",
    "OutOfDate"
};

typedef struct {

    char cmd    [HTTPACTION_MAXLENGTH];
    char post   [HTTPPOST_MAXLENGTH];

} cmdToDo;

typedef cmdToDo cmdsToDo [MAX_COMMANDS_TODO];

int checkLicense ();
int updateLicense ();
int forceUpdateLicense ();
int requestUpdate ();
int updateHostname ();

int requestLicense ();
int requestLicenseToComposer ();
int requestLicenseToIbidum ();
int requestLicenseToNapafilia ();

int sendAlive ();
int sendSubscribe ();
int sendUnsubscribe (char * composerIp, char * composerName);
int sendUpdate ();
int sendConfirm (char * action);
int sendConfirmUpdate ();
int sendEndDownload (char * program, char * msg);
int sendUpdLicense ();
int sendGetPlugins ();
int sendGetCommands ();
int sendLogs (char * file);
int sendPluginMsg (char * msg);

int executePlugin (char * command);
int updateProgram (char * program);
int updateEmission (char * program);
int changeCalendar ();
int synchroProgram (char * program);
int downloadProgram (char * program);
int synchroCalendar ();
int synchroPrograms (char * programs);
int createFiletree (char * program);
int createDirectory (char * path);
int downloadFiles (char * program, char * message, int size);
int downloadPackages (char * packages);

int HTTPRequest (char * url, char * post, char * reply, int replySize);
int HTTPRequestNoCheck (char * url, char * post, char * reply, int replySize, int nocheck);
int HTTPRequestNoCert (char * url, char * post, char * reply, int replySize);
int HTTPPostFile (char * url, char * file, char * reply, int replySize);
int HTTPDownload (char * file, char * hash, char * outputFile);
int HTTPDownloadPkg (char * file, char * hash, char * outputFile);

int getReplyStatus (char * reply);
int getReplyDetails (char * reply, char * details, int detailsSize);
int getReplyTodo (char * reply);

int getManifestFile (char * program);
int getLayoutFile (char * program);
int getPlaylistFile (char * program);
int getScheduleFile ();

int processTodoList ();
int processCurrentCmd ();

char * getScheduleMD5 (char * buffer, int size);
int compareMD5MetaFiles (char * program);

int getLogoHash (char * buffer, int size);
int updateWallpaper ();
int HTTPDownloadWP (char * outputFile);

int getPlayerStatus (char * output, int size);
int getNbZonesFromLayout (char * layout);
int getNbZonesFromMemory ();

int deleteSymlink (char * path);
int createSymlink (char * target, char * link);

int checkMd5Sum (char * path, char * expected);
