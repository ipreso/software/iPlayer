// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iSynchro/iSynchro.c
 * Description: Manage communications with Composer
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.04.15: Added Schedule management
 *  - 2009.10.06: Added the program-change management
 *  - 2009.09.03: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>
#include <sys/stat.h>
#include <sys/shm.h>

#include "iSynchro.h"
#include "iCommon.h"
#include "license.h"
#include "log.h"
#include "SHM.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iSynchro [options]\n"                                                  \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    -c, --conf '<file>'      Set the configuration file of the iPlayer\n" \
"\n"                                                                    \
"    --calendar               Update the schedule\n"                    \
"    --license                Update license key if needed with last generated\n" \
"    --force-license          Update license key with last generated\n" \
"    --plugins                Update plugins thanks to the composer\n"  \
"    --commands               Update commands thanks to the composer\n" \
"    --updlicense             Ask the composer to update the license\n" \
"    --hostname               Update composer's hostname\n"             \
"    --program '<program>'    Download then play 'program'\n"           \
"    --update '<program>'     Update emission 'program' and restart it\n" \
"    --programs '<program1[;program2]>' Download then play 'program'\n" \
"    --download '<program>'   Download 'program' meta and media files\n"\
"    --send '<plugin>/<msg>'  Send a message to a plugin on the Composer\n" \
"\n"                                                                    \

static struct option long_options[] =
{
    {"license",     no_argument,        0, 'a'},
    {"plugins",     no_argument,        0, 'b'},
    {"conf",        required_argument,  0, 'c'},
    {"commands",    no_argument,        0, 'd'},
    {"force-license", no_argument,      0, 'e'},
    {"calendar",    no_argument,        0, 'f'},
    {"download",    required_argument,  0, 'g'},
    {"help",        no_argument,        0, 'h'},
    {"hostname",    no_argument,        0, 'i'},
    {"program",     required_argument,  0, 'p'},
    {"programs",    required_argument,  0, 'q'},
    {"update",      required_argument,  0, 'r'},
    {"send",        required_argument,  0, 's'},
    {"updlicense",  no_argument,        0, 'u'},
    {"version",     no_argument,        0, 'v'},
    {0, 0, 0, 0}
};

sConfig     config;
sParams     params;

int         currentTodo;
int         lastTodo;
cmdsToDo    todoList;

sProgram dwProg;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char args [128];
    char program [NAME_MAXLENGTH*32];
    char msg [CMDLINE_MAXLENGTH];
    currentTodo = 0;
    lastTodo = 0;
    char action = '\0';

    memset (args, '\0', sizeof (args));
    memset (program, '\0', sizeof (program));
    memset (msg, '\0', sizeof (msg));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "hvabefig:udc:r:s:p:q:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s (%s) %s\n", 
                                ISYNCHRO_APPNAME,
                                ISYNCHRO_APPDETAIL,
                                ISYNCHRO_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (args, optarg, sizeof (args)-1);
                break;
            case 's':
                action = 's';
                strncpy (msg, optarg, sizeof (msg)-1);
                break;
            case 'r':
                action = 'r';
                strncpy (program, optarg, sizeof (program)-1);
                break;
            case 'p':
                action = 'p';
                strncpy (program, optarg, sizeof (program)-1);
                break;
            case 'q':
                action = 'q';
                strncpy (program, optarg, sizeof (program)-1);
                break;
            case 'g':
                action = 'g';
                strncpy (program, optarg, sizeof (program)-1);
                break;
            case 'f':
                action = 'f';
                break;
            case 'i':
                action = 'i';
                break;
            case 'a':
                action = 'a';
                break;
            case 'b':
                action = 'b';
                break;
            case 'd':
                action = 'd';
                break;
            case 'e':
                action = 'e';
                break;
            case 'u':
                action = 'u';
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (args) <= 0)
    {
        // Default configuration file
        strncpy (args, DEFAULT_CONFIGURATIONFILE, sizeof (args)-1);
    }

    if (common_getConfig (args, &config) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }
    if (common_getParams (config.path_params, &config, &params) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }

    if (action == 'a')
    {
        updateLicense ();
        return (EXIT_SUCCESS);
    }
    if (action == 'e')
    {
        forceUpdateLicense ();
        return (EXIT_SUCCESS);
    }

    if (!checkLicense ())
    {
        // Try to download license files
        if (!requestLicense ())
        {
            fprintf (stdout, "Error, exiting...\n");
            iCrit ("No valid license available at this time. Exiting.");
            return (EXIT_FAILURE);
        }
    }

    switch (action)
    {
        case 'b':
            sendGetPlugins ();
            break;
        case 'd':
            sendGetCommands ();
            break;
        case 'i':
            updateHostname ();
            break;
        case 'p':
            // Get a new program and play it !
            memset (&dwProg, '\0', sizeof (sProgram));
            strncpy (dwProg.name, program, NAME_MAXLENGTH-1);
            common_setDWProgram (&config, &dwProg);
            synchroProgram (program);
            common_flushDWProgram (&config);
            break;
        case 'r':
            // Get update of an emission and play it again !
            // (if belongs to emission pool, only restart emission)
            memset (&dwProg, '\0', sizeof (sProgram));
            strncpy (dwProg.name, program, NAME_MAXLENGTH-1);
            common_setDWProgram (&config, &dwProg);
            updateEmission (program);
            common_flushDWProgram (&config);
            break;
        case 'q':
            synchroPrograms (program);
            break;
        case 'g':
            // Get a new program without playing it
            memset (&dwProg, '\0', sizeof (sProgram));
            strncpy (dwProg.name, program, NAME_MAXLENGTH-1);
            common_setDWProgram (&config, &dwProg);
            downloadProgram (program);
            common_flushDWProgram (&config);
            break;
        case 'f':
            // Get the schedule
            synchroCalendar ();
            break;
        case 's':
            sendPluginMsg (msg);
            break;
        case 'u':
            sendUpdLicense ();
            break;
        default:
            // Send ALIVE
            if (strlen (params.composer_host))
                sendAlive ();
            else
            {
                iDebug ("TODO: Alive in local mode.");
                return (EXIT_FAILURE);
            }
    }

    // Before exiting, executing the Todo list !
    processTodoList ();

    return (EXIT_SUCCESS);
}

int sendGetPlugins ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to get plugins.");
        return (0);
    }

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/plugins",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Sent GETPLUGINS to %s", params.composer_host);

    if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||         
        strlen (output) <= strlen ("S:OK\n"))                         
    {
        iError ("Wrong format for reply:");
        iError ("%s", output);
        return (0);                                                   
    }
    
    return (downloadPackages (output + strlen ("S:OK\n")));
}

int sendGetCommands ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to get commands.");
        return (0);
    }

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/commands",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Sent GETCOMMANDS to %s", params.composer_host);

    if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||         
        strlen (output) <= strlen ("S:OK\n"))                         
    {
        iError ("Wrong format for reply:");
        iError ("%s", output);
        return (0);                                                   
    }
    
    return (downloadPackages (output + strlen ("S:OK\n")));
}

int forceUpdateLicense ()
{
    return (requestUpdate ());
}

int updateHostname ()
{
    char buffer [CMDLINE_MAXLENGTH];
    char hostname [HOST_MAXLENGTH];
    FILE * fp = NULL;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to request the Composer's hostname.");
        return (0);
    }

    // Get the hostname from the remote certificate CommonName
    memset (buffer, '\0', sizeof (buffer));
    memset (hostname, '\0', sizeof (hostname));
    snprintf (buffer, sizeof (buffer) - 1,
            "openssl s_client -connect %s:443 "\
                               " -showcerts "\
                               " -CApath %s "\
                               " -cert %s 2>/dev/null </dev/null | "\
                "openssl x509 -noout -subject | "\
                "sed -r 's/^.*CN = ([^ ]+),.*$/\\1/g'",
            params.composer_ip,
            config.path_ca,
            config.path_certificate);

    //iDebug ("=> %s", buffer);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("Unable to get Hostname for composer '%s' (no1)", params.composer_ip);
        return (0);
    }
    if (!fgets (hostname, sizeof (hostname), fp))
    {
        iError ("Unable to get Hostname for composer '%s' (no2)", params.composer_ip);
        pclose (fp);
        return (0);
    }
    if (pclose (fp) != 0)
    {
        iError ("Unable to get Hostname for composer '%s' (no3)", params.composer_ip);
        return (0);
    }

    while (strlen (hostname) && (hostname [strlen (hostname)-1] == '\n' ||
                                 hostname [strlen (hostname)-1] == '\r' ||
                                 hostname [strlen (hostname)-1] == ' '))
        hostname [strlen (hostname)-1] = '\0';

    memset (params.composer_host, '\0', HOST_MAXLENGTH);
    strncpy (params.composer_host, hostname, HOST_MAXLENGTH);

    if (writeParametersFile (&config, &params) >= 0)
    {
        iDebug ("Composer '%s' - Hostname => '%s'", params.composer_ip, params.composer_host);
        return (1);
    }
    else
    {
        iError ("Cannot write new Composer Hostname into parameters file.");
        return (0);
    }
    return (1);
}

int updateLicense ()
{
    FILE * file = NULL;
    char expirationDate [DATE_MAXLENGTH];
    char preventionDate [DATE_MAXLENGTH];
    char nowDate [DATE_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (expirationDate, '\0', DATE_MAXLENGTH);
    memset (preventionDate, '\0', DATE_MAXLENGTH);
    memset (nowDate, '\0', DATE_MAXLENGTH);

    // Get expiration date
    snprintf (buffer, CMDLINE_MAXLENGTH - 1,
                "%s x509 -in %s -enddate -noout | "                         \
                "cut -d= -f2 | "                                            \
                "awk '{print \""                                            \
                        "date --date=\\\"\"$0\"\\\" --rfc-3339=seconds\"}' | " \
                "sh",
                OPENSSL_PATH,
                config.path_certificate);

    file = popen (buffer, "r");
    if (!file)
    {
        iError ("Unable to check the license's expiration (no1)");
        return (0);
    }
    if (!fgets (expirationDate, sizeof (expirationDate), file))
    {
        iError ("Unable to check the license's expiration (no2)");
        return (0);
    }
    if (pclose (file) != 0)
    {
        iError ("Unable to check the license's expiration (no3)");
        return (0);
    }
    while (strlen (expirationDate) &&
            (expirationDate [strlen (expirationDate)-1] == '\r' ||
             expirationDate [strlen (expirationDate)-1] == '\n'))
        expirationDate [strlen (expirationDate)-1] = '\0';

    // Get prevention date
    snprintf (buffer, CMDLINE_MAXLENGTH - 1,
                "date --date=\"10 days\" --rfc-3339=seconds");

    file = popen (buffer, "r");
    if (!file)
    {
        iError ("Unable to check the license's expiration (no4)");
        return (0);
    }
    if (!fgets (preventionDate, sizeof (preventionDate), file))
    {
        iError ("Unable to check the license's expiration (no5)");
        return (0);
    }
    if (pclose (file) != 0)
    {
        iError ("Unable to check the license's expiration (no6)");
        return (0);
    }
    while (strlen (preventionDate) &&
            (preventionDate [strlen (preventionDate)-1] == '\r' ||
             preventionDate [strlen (preventionDate)-1] == '\n'))
        preventionDate [strlen (preventionDate)-1] = '\0';

    if (strcmp (preventionDate, expirationDate) >= 0)
    {
        // Get current date
        snprintf (buffer, CMDLINE_MAXLENGTH - 1,
                    "date --rfc-3339=seconds");
        file = popen (buffer, "r");
        if (!file)
        {
            iError ("Unable to get current date (no1)");
            return (0);
        }
        if (!fgets (nowDate, sizeof (nowDate), file))
        {
            iError ("Unable to get current date (no2)");
            return (0);
        }
        if (pclose (file) != 0)
        {
            iError ("Unable to get current date (no3)");
            return (0);
        }
        while (strlen (nowDate) &&
                (nowDate [strlen (nowDate)-1] == '\r' ||
                 nowDate [strlen (nowDate)-1] == '\n'))
            nowDate [strlen (nowDate)-1] = '\0';
        if (strcmp (nowDate, expirationDate) >= 0)
        {
            iInfo ("License has expired (%s)", expirationDate);
            unlink ("/var/cache/iplayer/foo");
            unlink ("/var/cache/iplayer/bar");
            unlink (config.path_certificate);
            unlink (config.path_license);
            memset (buffer, '\0', sizeof (buffer));
            snprintf (buffer, sizeof (buffer) - 1,
                      "echo \"License has expired.\" > \"%s\"",
                      REASON_FILE);
            system (buffer);
        }
        else
            iLog ("License expires on : %s", expirationDate);

        iDebug ("Trying to get a new version of the license...");
        // Try to download new license files
        requestUpdate ();
    }

    return (1);

}

int checkLicense ()
{
    FILE * file = NULL;
    char path [FILENAME_MAXLENGTH];
    char hash [HASH_MAXLENGTH+1];

    if (!getHash (&config, hash, sizeof (hash)))
    {
        iError ("Unable to get Hash of the Box.");
        return (0);
    }

    // Check if a license key exists
    memset (path, '\0', FILENAME_MAXLENGTH);
    snprintf (path, FILENAME_MAXLENGTH-1, "%s",
                config.path_certificate);
    if (!(file = fopen (path, "r")))
    {
        // File doesn't exists, Box does not have license file !
        iError ("Box does not have a license file.");
        return (0);
    }
    fclose (file);

    // Check CA and validity dates
    return (checkLicenseValidity (&config));
}

int requestLicense ()
{
    return (requestLicenseToNapafilia ());
/*
    if (strlen (params.composer_host))
        return (requestLicenseToComposer ());
    else
*/
    //    return (requestLicenseToIbidum ());
}

int requestLicenseToComposer ()
{
    iDebug ("Requesting license to %s", params.composer_host);
    iDebug ("************ TODO ************");
    iDebug ("iSynchro/iSynchro.c:requestLicenseToComposer()");
    iDebug ("*********** /TODO ************");
    return (0);
}

int requestLicenseToNapafilia ()
{
    char buffer [LINE_MAXLENGTH];
    char uri [HOST_MAXLENGTH];
    char mac [64];
    char hash [HASH_MAXLENGTH+1];
    char args [HTTPPOST_MAXLENGTH];

    if (!getMac (&config, mac, sizeof (mac)) ||
        !getHash (&config, hash, sizeof (hash)))
    {
        iError ("Cannot get information to request license.");
        return (0);
    }

    memset (buffer, '\0', sizeof (buffer));
    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://%s/%s/player.pem",
                                        LICENSE_SERVER, hash);

/*
    snprintf (args, sizeof (args) - 1,
            "net=`cat /sys/class/net/%s/address"            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "hd=`sudo /sbin/hdparm -i %s"                   \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "nvme=`sudo /usr/sbin/nvme id-ctrl %s"          \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "origin=`echo \"%s\""                           \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "reason=`echo \"%s\""                           \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
            config.license_net,
            config.license_hd,
            config.license_hd,
            "TODO",
            "TODO");
*/

    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " -T5"                      \
                " --secure-protocol=TLSv1"  \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document=%s.tmp" \
                " %s"                       \
                " && mv %s.tmp %s",
                PATH_WGET,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                args,
                config.path_certificate,
                uri,
                config.path_certificate, config.path_certificate);
    iDebug (buffer);
    system (buffer);

    return (1);
}

int requestLicenseToIbidum ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    char mac [64];
    char hash [HASH_MAXLENGTH+1];
    int status;
    FILE * fp;
    char * licensePos = NULL;
    int indexPos;
    char details [LINE_MAXLENGTH];
    char reason [LINE_MAXLENGTH];
    char origin [LINE_MAXLENGTH];
    char buffer [LINE_MAXLENGTH];
    int length;

    if (!getMac (&config, mac, sizeof (mac)) ||
        !getHash (&config, hash, sizeof (hash)))
    {
        iError ("Cannot get information to request license.");
        return (0);
    }

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://%s/box/%s/license.key",
                                        LICENSE_SERVER, hash);
                                        
    // Get Origin of the installation
    memset (origin, '\0', sizeof (origin));
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "if [ ! -f \"%s\" ]; then echo \"?/%s\" > %s ; " \
                "else grep \"%s\" \"%s\" 2>/dev/null 1>&2 ; " \
                "if [ \"$?\" != \"0\" ]; then " \
                "echo -n \"%s\" >> \"%s\" ; fi; fi; " \
                "cat \"%s\"",
              ORIGIN_FILE,
              hash,
              ORIGIN_FILE,
              hash,
              ORIGIN_FILE,
              hash,
              ORIGIN_FILE,
              ORIGIN_FILE);
    //iDebug ("%s", buffer);
    fp = popen (buffer, "r");
    if (fp)
    {
        length = fread (origin, sizeof (char), sizeof (origin), fp);
        pclose (fp);

        while (length && (origin [length-1] == '\r' ||
                          origin [length-1] == '\n'))
        {
            origin [length-1] = '\0';
            length--;
        }
    }

    // Get Reason
    memset (reason, '\0', sizeof (reason));
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "cat \"%s\"",
              REASON_FILE);
    fp = popen (buffer, "r");
    if (fp)
    {
        length = fread (reason, sizeof (char), sizeof (reason), fp);
        pclose (fp);

        while (length && (reason [length-1] == '\r' ||
                          reason [length-1] == '\n'))
        {
            reason [length-1] = '\0';
            length--;
        }
    }

    snprintf (args, sizeof (args) - 1,
            "net=`cat /sys/class/net/%s/address"            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "hd=`sudo /sbin/hdparm -i %s"                   \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "nvme=`sudo /usr/sbin/nvme id-ctrl %s"          \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "origin=`echo \"%s\""                           \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "reason=`echo \"%s\""                           \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
            config.license_net,
            config.license_hd,
            config.license_hd,
            origin,
            reason);

    status = HTTPRequestNoCert (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_PENDING:
            iDebug ("License request sent to %s", LICENSE_SERVER);
            break;
        case STATUS_OK:
            // Write the license on hard disk
            iDebug ("Downloading license...");
            fp = fopen (config.path_license, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save license file '%s'", config.path_license);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with license format.");
                return (0);
            }
            else
            {
                // Get length to write (binary data can have '\0' bytes,
                // so strlen is useless)
                indexPos = sizeof (output) - 1;
                while (indexPos > 0 && output [indexPos] == '\0')
                    indexPos--;
                indexPos -= 4;  // HTTP added characters ?!
                licensePos = output + strlen ("S:OK\n");
                fwrite (licensePos, indexPos, 1, fp);
                iLog ("License downloaded (%d bytes).", indexPos);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    // Getting certificate...
    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://%s/box/%s/player.cert",
                                        LICENSE_SERVER, hash);
                                        

    memset (args, '\0', sizeof (args));
    status = HTTPRequestNoCert (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_ERROR:
            iError ("Cannot get iBox certificate.");
            if (getReplyDetails (output, details, sizeof (details)))
                iError ("Details are: %s", details);
            return (0);
        case STATUS_OK:
            // Write the license on hard disk
            iDebug ("Downloading certificate...");
            fp = fopen (config.path_certificate, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save certificate file '%s'", 
                        config.path_certificate);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with certificate format.");
                return (0);
            }
            else
            {
                // Get length to write (binary data can have '\0' bytes,
                // so strlen is useless)
                indexPos = sizeof (output) - 1;
                while (indexPos > 0 && output [indexPos] == '\0')
                    indexPos--;
                indexPos -= 4;  // HTTP added characters ?!
                licensePos = output + strlen ("S:OK\n");
                fwrite (licensePos, indexPos, 1, fp);
                iLog ("Certificate downloaded (%d bytes).", indexPos);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    return (1);
}

int requestUpdate ()
{
    /*
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;
    FILE * fp;
    char * licensePos = NULL;
    int indexPos;
    char oldMD5 [MD5_MAXLENGTH];
    char details [LINE_MAXLENGTH];
    char tmpPath [FILENAME_MAXLENGTH];
    */
    char mac [64];
    char hash [HASH_MAXLENGTH+1];
    char buffer [CMDLINE_MAXLENGTH];

    if (!getMac (&config, mac, sizeof (mac)) ||
        !getHash (&config, hash, sizeof (hash)))
    {
        iError ("Cannot get information to request license.");
        return (0);
    }

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to request the license.");
        return (0);
    }

    // Update composer's hostname
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
              "%s -c %s --hostname",
                config.path_isynchro,
                config.filename);
    system (buffer);

    // Getting certificate...
    if (!requestLicenseToNapafilia ())
    {
        iError ("Cannot get last License...");
        return (0);
    }

    return (1);

    // Comparison between current and downloaded certificate
/*
    memset (oldMD5, '\0', sizeof (oldMD5));
    memset (newMD5, '\0', sizeof (newMD5));
    if (!common_getFileMD5 (config.path_certificate, oldMD5, sizeof (oldMD5)))
    {
        iError ("Unable to get MD5 of %s", tmpPath);
        return (0);
    }
    if (!common_getFileMD5 (tmpPath, newMD5, sizeof (newMD5)))
    {
        iError ("Unable to get MD5 of %s", tmpPath);
        return (0);
    }

    iDebug ("Old Certificate: %s", oldMD5);
    iDebug ("New Certificate: %s", newMD5);

    if (strcmp (oldMD5, newMD5) == 0)
    {
        iLog ("No new certificate...");
        return (0);
    }
    else
    {
        // Remove old files
        unlink ("/var/cache/iplayer/foo");
        unlink ("/var/cache/iplayer/bar");
        unlink (config.path_certificate);
        unlink (config.path_license);
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer) - 1,
                  "echo \"License renewing...\" > \"%s\"",
                  REASON_FILE);
        system (buffer);

        // Download license files
        if (!requestLicense ())
        {
            fprintf (stdout, "Error, exiting...\n");
            iError ("No valid license available at this time. Exiting.");
            return (0);
        }

        // Ask the composer to update our license in DB
        sendUpdLicense ();
    }

    return (1);
*/
}

int sendPluginMsg (char * msg)
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    char * index;
    char plugin [PLUGIN_MAXLENGTH];
    int status;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to send plugin message.");
        return (0);
    }

    // Check msg is in format '<plugin>:<msg>'
    index = strchr (msg, '/');
    if (!index)
    {
        iError ("Message '%s' is not in the form 'plugin/message'", msg);
        return (0);
    }
    memset (plugin, '\0', sizeof (plugin));
    strncpy (plugin, msg, index - msg);
    if (strlen (plugin) < 1 || strlen (index) < 2)
    {
        iError ("Message '%s' has an empty field 'plugin/message'", msg);
        return (0);
    }

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/pluginmsg",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "msg=`echo -n '"                                \
                    "%s"                                        \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                msg);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Sent '%s' to plugin '%s'", index+1, plugin);
    return (1);
}

int sendAlive ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    char hash [HASH_MAXLENGTH+1];
    char wp [HASH_MAXLENGTH+1];
    char schedule [MD5_MAXLENGTH+1];
    char playerStatus [LINE_MAXLENGTH];
    int status;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to send alive message.");
        return (0);
    }

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/alive",
                                        params.composer_host);

    // Get Hash of the box
    if (!getHash (&config, hash, sizeof (hash)))
    {
        iError ("Cannot get information to send Alive message.");
        return (0);
    }

    // Get Wallpaper hash
    if (!getLogoHash (wp, sizeof (wp)))
    {
        iDebug ("No WallPaper found.");
    }

    // Get Schedule MD5
    if (!getScheduleMD5 (schedule, sizeof (schedule)))
        memset (schedule, '\0', sizeof (schedule));

    // Get global status of the Player
    if (!getPlayerStatus (playerStatus, sizeof (playerStatus)))
    {
        memset (playerStatus, '\0', sizeof (playerStatus));
        snprintf (playerStatus, sizeof (playerStatus),
                    "ERROR - Cannot get global Player status");
    }

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "box=`echo -n '"                                \
                    "{\"hash\":\"%s\","                         \
                    "\"status\":\"%s\","                        \
                    "\"wp\":\"%s\","                            \
                    "\"program\":\"%s\","                       \
                    "\"manifest\":\"%s\","                      \
                    "\"layout\":\"%s\","                        \
                    "\"schedule\":\"%s\","                      \
                    "\"playlist\":\"%s\"}"                      \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                hash, 
                playerStatus,
                wp,
                config.currentProgram.name,
                config.currentProgram.manifest,
                config.currentProgram.layout,
                schedule,
                config.currentProgram.playlist);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Sent ALIVE to %s, program '%s'", 
                params.composer_host,
                config.currentProgram.name);
    return (1);
}

int getLogoHash (char * buffer, int size)
{
    char cmd [CMDLINE_MAXLENGTH];
    char md5 [HASH_MAXLENGTH+1];
    FILE * fp;

    memset (buffer, '\0', size);
    memset (cmd, '\0', sizeof (cmd));
    memset (md5, '\0', sizeof (md5));

    snprintf (cmd, sizeof (cmd) - 1, "/usr/bin/md5sum `"        \
                "if [ -e \"%s/%s\" ]; then "                    \
                    "echo \"%s/%s\" ; "                         \
                "else "                                         \
                    "echo \"%s/%s\" ; "                         \
                "fi` | cut -f1 -d' '",
                config.path_library, CUSTOM_BASE,
                config.path_library, CUSTOM_BASE,
                config.path_library, WALLPAPER_BASE);

    fp = popen (cmd, "r");
    if (!fp)
    {
        iError ("Unable to check MD5 sum for Wallpaper (no1)");
        return (0);
    }
    if (!fgets (md5, sizeof (md5), fp))
    {
        iError ("Unable to check MD5 sum for Wallpaper (no2)");
        pclose (fp);
        return (0);
    }
    if (pclose (fp) != 0)
    {
        iError ("Unable to check MD5 sum for Wallpaper (no3)");
        return (0);
    }
 
    strncpy (buffer, md5, size);
    return (1);
}

int updateWallpaper ()
{
    char buffer [FILENAME_MAXLENGTH];
    char cmd    [CMDLINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    memset (cmd, '\0', sizeof (cmd));
    snprintf (buffer, sizeof (buffer)-1, "%s/%s",
                config.path_library, CUSTOM_BASE);

    if (HTTPDownloadWP (buffer))
    {
        iInfo ("[%d] New Wallpaper downloaded.", getpid ());
        // Convert custom background logo to the correct size
        snprintf (cmd, sizeof (cmd) - 1,
                    "if [ -e \"%s/%s\" ]; then "                    \
                        "/usr/bin/convert %s/%s -resize `%s -r`! %s/`%s -r`_%s ; "
                    "fi",
                    config.path_library, CUSTOM_BASE,
                    config.path_library, CUSTOM_BASE,
                    config.path_iwm, config.path_library,
                    config.path_iwm, CUSTOM_BASE);
        system (cmd);
        return (1);
    }
    else
    {
        iError ("[%d] Failed to download new wallpaper.", getpid ());
        return (0);
    }
}

int sendSubscribe ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    char mac [64];
    char hash [HASH_MAXLENGTH+1];
    char wp [HASH_MAXLENGTH+1];
    int status;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    memset (wp, '\0', sizeof (wp));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/subscribe",
                                        params.composer_host);

    if (!getMac (&config, mac, sizeof (mac)) ||
        !getHash (&config, hash, sizeof (hash)))
    {
        iError ("Cannot get information to subscribe.");
        return (0);
    }

    if (!getLogoHash (wp, sizeof (wp)))
    {
        iDebug ("No WallPaper found.");
    }

    snprintf (args, sizeof (args) - 1,
            "box=`echo -n '"                            \
                "{\"hash\":\"%s\","                     \
                "\"mac\":\"%s\","                       \
                "\"ip\":\"%s\","                        \
                "\"gw\":\"%s\","                        \
                "\"dns\":\"%s\","                       \
                "\"wp\":\"%s\","                        \
                "\"pkgsrc\":\"%s\","                    \
                "\"pkgver\":\"%s\","                    \
                "\"label\":\"%s\","                     \
                "\"mode\":\"\","                        \
                "\"composer\":\"%s\","                  \
                "\"composer_ip\":\"%s\","               \
                "\"coordinates\":\"%s\"}"               \
            "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                hash, mac, 
                params.box_ip, params.box_gw, params.box_dns,
                wp,
                params.pkg_src, ISYNCHRO_VERSION, params.box_name, 
                params.composer_host, params.composer_ip,
                params.box_coordinates);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iInfo ("Subscription sent to '%s' (%s)", params.composer_host,
                                            params.composer_ip);
    return (1);
}

int sendUnsubscribe (char * composerIp, char * composerName)
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://%s/players/unsubscribe",
                                        composerName);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    // License is now invalid
    unlink ("/var/cache/iplayer/foo");
    unlink ("/var/cache/iplayer/bar");
    unlink (config.path_certificate);
    unlink (config.path_license);
    memset (output, '\0', sizeof (output));
    snprintf (output, sizeof (output) - 1,
              "echo \"Composer migration.\" > \"%s\"",
              REASON_FILE);
    system (output);

    iInfo ("Unsubscription sent to %s (%s)", composerName, composerIp);
    return (1);
}

int sendUpdate ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    char oldComposerIp      [IP_MAXLENGTH];
    char oldComposerName    [HOST_MAXLENGTH];

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/update",
                                        params.composer_host);

    snprintf (args, sizeof (args) - 1,
            "box=`echo -n '{"                           \
                "\"composer\":\"%s\","                  \
                "\"composer_ip\":\"%s\","               \
                "\"ip\":\"%s\","                        \
                "\"gw\":\"%s\","                        \
                "\"dns\":\"%s\","                       \
                "\"pkgsrc\":\"%s\","                    \
                "\"label\":\"%s\","                     \
                "\"coordinates\":\"%s\"}"               \
            "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                params.composer_host, params.composer_ip,
                params.box_ip, params.box_gw, params.box_dns,
                params.pkg_src, params.box_name, 
                params.box_coordinates);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Update received.");

    memset (oldComposerIp, '\0', sizeof (oldComposerIp));
    strncpy (oldComposerIp, params.composer_ip, sizeof (oldComposerIp)-1);
    memset (oldComposerName, '\0', sizeof (oldComposerName));
    strncpy (oldComposerName, params.composer_host, sizeof (oldComposerName)-1);

    if (parseNewParameters (output, &params) > 0 &&
        writeParametersFile (&config, &params) >= 0)
    {
        if (strncmp (oldComposerIp, params.composer_ip,
                     strlen (oldComposerIp)) != 0 ||
            strncmp (oldComposerName, params.composer_host,
                     strlen (oldComposerName)) != 0)
        {
            iLog ("Composer has changed ! '%s' ('%s') -> '%s' ('%s')",
                        oldComposerName, oldComposerIp,
                        params.composer_host, params.composer_ip);

            sendUnsubscribe (oldComposerIp, oldComposerName);
        }
        return (applyParameters (&config, &params));
    }
    else
        return (0);
}

int sendConfirm (char * action)
{
    if (strcmp (action, "UPDATE") == 0)
        sendConfirmUpdate ();
    else
        iDebug ("TODO: Confirm action '%s'", action);

    return (1);
}

// Update the current emission, because meta-files changed
int updateProgram (char * program)
{
    char sysCmd [CMDLINE_MAXLENGTH];

    // If download is in progress, we're ignoring this command...
    if (strlen (config.dwProgram.name))
    {
        if (strcmp (config.dwProgram.name, program) != 0)
        {
            iError ("Cannot update emission to '%s'", program);
            iError ("Already downloading emission '%s'", config.dwProgram.name);
        }
        return (0);
    }

    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                "%s --update \"%s\" 2>/dev/null 1>&2 &",
                ISYNCHRO_APPNAME,
                program);
    system (sysCmd);
    return (1);
}

int changeCalendar ()
{
    char sysCmd [CMDLINE_MAXLENGTH];

    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                "%s --calendar 2>/dev/null 1>&2 &",
                ISYNCHRO_APPNAME);
    system (sysCmd);
    return (1);
}

int sendEndDownload (char * program, char * msg)
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/enddownload",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "box=`echo -n '"                                \
                    "{\"status\":\"%s\","                       \
                    "\"program\":\"%s\"}"                       \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                msg,
                program);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iDebug ("Sent EndDownload to %s", params.composer_host);
    return (1);
}

int sendUpdLicense ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to ask Composer to update license.");
        return (0);
    }

    iDebug ("Asking to the composer %s to update my license...", 
            params.composer_host);
    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/updlicense",
                                        params.composer_host);
    memset (args, '\0', sizeof (args));

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
    {
        iError ("Composer cannot get an up-to-date license.");
        return (0);
    }

    return (1);
}


int sendConfirmUpdate ()
{
    char uri [HOST_MAXLENGTH];
    char output [HTTPREPLY_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/confirmupdate",
                                        params.composer_host);

    snprintf (args, sizeof (args) - 1,
            "box=`echo -n '{"                           \
                "\"composer\":\"%s\","                  \
                "\"composer_ip\":\"%s\","               \
                "\"ip\":\"%s\","                        \
                "\"gw\":\"%s\","                        \
                "\"dns\":\"%s\","                       \
                "\"pkgsrc\":\"%s\","                    \
                "\"label\":\"%s\","                     \
                "\"coordinates\":\"%s\"}"               \
            "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                params.composer_host, params.composer_ip,
                params.box_ip, params.box_gw, params.box_dns,
                params.pkg_src, params.box_name, 
                params.box_coordinates);

    status = HTTPRequest (uri, args, output, sizeof (output));
    if (status != STATUS_OK)
        return (0);

    iLog ("Box information updated successfully");
    return (1);
}

int getReplyDetails (char * reply, char * details, int detailsSize)
{
    int index = 0;
    char * ptr;
    int j;

    memset (details, '\0', detailsSize);
    while (index < strlen (reply) && strlen (details) < 1)
    {
        // First characters of the line are "D:" ?
        if (*(reply+index) == 'D' &&
            *(reply+index+1) == ':')
        {
            // Getting Details !
            index += 2;
            for (j = 0 ; j < detailsSize - 1 &&
                         index < strlen (reply) &&
                         reply [index] != ':' &&
                         reply [index] != '\n' ; j++, index++)
                details [j] = reply [index];
        }
        else
        {
            // Following line
            ptr = strchr (reply+index, '\n');
            if (ptr == NULL)
                index = strlen (reply);
            else
                index = ptr - reply + 1;
        }
    }
    return (strlen (details));
}

int getReplyTodo (char * reply)
{
    int index = 0;
    char * ptr;
    int j;

    while (index < strlen (reply))
    {
        // First characters of the line are "C:" ?
        if (*(reply+index) == 'C' &&
            *(reply+index+1) == ':')
        {
            memset (&(todoList[lastTodo]), '\0', sizeof (cmdToDo));
            // Adding the command to the list
            index += 2;
            for (j = 0 ; j < HTTPACTION_MAXLENGTH - 1 &&
                         index < strlen (reply) &&
                         reply [index] != ':' &&
                         reply [index] != '\n' ; j++, index++)
                todoList [lastTodo].cmd [j] = reply [index];
            if (index < strlen (reply) && reply [index] == ':')
            {
                // Adding the post parameters
                index++;
                for (j = 0 ; j < HTTPPOST_MAXLENGTH - 1 &&
                             index < strlen (reply) &&
                             reply [index] != '\n' ; j++, index++)
                    todoList [lastTodo].post [j] = reply [index];
            }
            lastTodo = (lastTodo + 1) % MAX_COMMANDS_TODO;
        }
        else
        {
            // Following line
            ptr = strchr (reply+index, '\n');
            if (ptr == NULL)
                index = strlen (reply);
            else
                index = ptr - reply + 1;
        }
    }
    return (1);
}

int processTodoList ()
{
    while (currentTodo != lastTodo)
    {
        processCurrentCmd ();
        currentTodo = (currentTodo + 1) % MAX_COMMANDS_TODO;
    }

    return (1);
}

int processCurrentCmd ()
{
    if (strlen (todoList[currentTodo].cmd))
    {
        if (strcmp (todoList [currentTodo].cmd, "ALIVE") == 0)
            sendAlive ();
        else if (strcmp (todoList [currentTodo].cmd, "CALENDAR") == 0)
            changeCalendar ();
        else if (strcmp (todoList [currentTodo].cmd, "SUBSCRIBE") == 0)
        {
            if (sendSubscribe ())
            {
                sendGetPlugins ();
                sendGetCommands ();
            }
        }
        else if (strcmp (todoList [currentTodo].cmd, "UPDATE") == 0)
            sendUpdate ();
        else if (strcmp (todoList [currentTodo].cmd, "CONFIRM") == 0)
            sendConfirm (todoList [currentTodo].post);
        else if (strcmp (todoList [currentTodo].cmd, "CHGPROGRAM") == 0)
            updateProgram (todoList [currentTodo].post);
        else if (strcmp (todoList [currentTodo].cmd, "PLUGIN") == 0)
            executePlugin (todoList [currentTodo].post);
        else if (strcmp (todoList [currentTodo].cmd, "LICENSE") == 0)
            iDebug ("TODO: COMMAND LICENSE...");
        else if (strcmp (todoList [currentTodo].cmd, "UNSUBSCRIBE") == 0)
            sendUnsubscribe (params.composer_ip, params.composer_host);
        else if (strcmp (todoList [currentTodo].cmd, "WALLPAPER") == 0)
            updateWallpaper ();
        else
            iError ("Cannot POST unknown command : '%s'",
                    todoList [currentTodo].cmd);
    }

    // Cleaning up
    memset (&(todoList[currentTodo]), '\0', sizeof (cmdToDo));

    return (1);
}

int getReplyStatus (char * reply)
{
    int index = 0;
    char * ptr;
    char status [STATUS_MAXLENGTH];
    int j;

    memset (status, '\0', sizeof (status));
    while (index < strlen (reply) && strlen (status) < 1)
    {
        // First characters of the line are "S:" ?
        if (*(reply+index) == 'S' &&
            *(reply+index+1) == ':')
        {
            // Getting Status !
            index += 2;
            for (j = 0 ; j < sizeof (status)-1 &&
                         index < strlen (reply) &&
                         reply [index] != ':' &&
                         reply [index] != '\n' ; j++, index++)
                status [j] = reply [index];
        }
        else
        {
            // Following line
            ptr = strchr (reply+index, '\n');
            if (ptr == NULL)
                index = strlen (reply);
            else
                index = (ptr - reply > index) ? ptr-reply : index+1;
        }
    }

    if (strcmp (status, "OK") == 0)
        return (STATUS_OK);
    else if (strcmp (status, "FAILED") == 0)
        return (STATUS_FAILED);
    else if (strcmp (status, "ERROR") == 0)
        return (STATUS_ERROR);
    else if (strcmp (status, "PENDING") == 0)
        return (STATUS_PENDING);
    else
        return (STATUS_UNKNOWN);
}

int HTTPPostFile (char * url, char * postfile, char * reply, int replySize)
{
    char buffer [LINE_MAXLENGTH];
    FILE * file;
    int status;
    char details [LINE_MAXLENGTH];
    int length;

    memset (buffer, '\0', sizeof (buffer));
    memset (reply, '\0', replySize);

    // Create the command line
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " -T5"                      \
                " --secure-protocol=TLSv1"  \
                " --certificate=%s"         \
                " --private-key=%s/%s"      \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-file=\"%s\""       \
                " --output-document=-"      \
                " %s",
                PATH_WGET,
                config.path_certificate,
                config.path_tmp, CHECKED_LICENSE,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                postfile,
                url);

    file = popen (buffer, "r");
    if (!file)
    {
        iError ("Unable to get '%s' (no1)", url);
        //iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }

    length = fread (reply, sizeof (char), replySize, file);

    if (pclose (file) != 0)
    {
        iError ("Unable to get '%s' (no3)", url);
        iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }
    while (length && (reply [length-1] == '\r' ||
                              reply [length-1] == '\n'))
    {
        reply [length-1] = '\0';
        length--;
    }

    // Parse reply
    status = getReplyStatus (reply);
    if (status != STATUS_OK && status != STATUS_PENDING)
    {
        iError ("%s returned '%s'", url, statusStrings [status]);
        if (status == STATUS_UNKNOWN)
            iError ("Reply is: %s", reply);
        else if (getReplyDetails (reply, details, sizeof (details)))
            iError ("Details are: %s", details);
    }

    // Other command to send ?
    getReplyTodo (reply);

    return (status);
}

int HTTPRequest (char * url, char * post, char * reply, int replySize)
{
    return (HTTPRequestNoCheck (url, post, reply, replySize, 0));
}

int HTTPRequestNoCheck (char * url, char * post, char * reply, int replySize, int nocheck)
{
    char buffer [LINE_MAXLENGTH];
    FILE * file;
    int status;
    char details [LINE_MAXLENGTH];
    int length;

    memset (buffer, '\0', sizeof (buffer));
    memset (reply, '\0', replySize);

    // Create the command line
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " -T5"                      \
                "%s"                        \
                " --secure-protocol=TLSv1"  \
                " --certificate=%s"         \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document=-"      \
                " %s",
                PATH_WGET,
                (nocheck == 0) ? "" : " --no-check-certificate",
                config.path_certificate,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                post,
                url);

/*
iDebug ("HTTP Request is :");
iDebug ("%s", buffer);
*/

    file = popen (buffer, "r");
    if (!file)
    {
        iError ("Unable to get '%s' (no1)", url);
        //iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }

    length = fread (reply, sizeof (char), replySize, file);

    if (pclose (file) != 0)
    {
        iError ("Unable to get '%s' (no3)", url);
        iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }
    while (length && (reply [length-1] == '\r' ||
                              reply [length-1] == '\n'))
    {
        reply [length-1] = '\0';
        length--;
    }

    // Parse reply
    status = getReplyStatus (reply);
    if (status != STATUS_OK && status != STATUS_PENDING)
    {
        iError ("%s returned '%s'", url, statusStrings [status]);
        if (status == STATUS_UNKNOWN)
            iError ("Reply is: %s", reply);
        else if (getReplyDetails (reply, details, sizeof (details)))
            iError ("Details are: %s", details);
    }

    // Other command to send ?
    getReplyTodo (reply);

    return (status);
}

int HTTPRequestNoCert (char * url, char * post, char * reply, int replySize)
{
    char buffer [LINE_MAXLENGTH];
    FILE * file;
    int status;
    char details [LINE_MAXLENGTH];
    int length;

    memset (buffer, '\0', sizeof (buffer));
    memset (reply, '\0', replySize);

    // Create the command line
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " -T5"                      \
                " --secure-protocol=TLSv1"  \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document=-"      \
                " %s",
                PATH_WGET,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                post,
                url);

/*
iDebug ("HTTP Request is :");
iDebug ("%s", buffer);
*/

    file = popen (buffer, "r");
    if (!file)
    {
        iError ("Unable to get '%s' (no1)", url);
        //iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }

    length = fread (reply, sizeof (char), replySize, file);

    if (pclose (file) != 0)
    {
        iError ("Unable to get '%s' (no3)", url);
        iDebug ("Command is '%s'", buffer);
        return (STATUS_ERROR);
    }
    while (length && (reply [length-1] == '\r' ||
                              reply [length-1] == '\n'))
    {
        reply [length-1] = '\0';
        length--;
    }

    // Parse reply
    status = getReplyStatus (reply);
    if (status != STATUS_OK && status != STATUS_PENDING)
    {
        iError ("%s returned '%s'", url, statusStrings [status]);
        if (status == STATUS_UNKNOWN)
            iError ("Reply is: %s", reply);
        else if (getReplyDetails (reply, details, sizeof (details)))
            iError ("Details are: %s", details);
    }

    // Other command to send ?
    getReplyTodo (reply);

    return (status);
}

int synchroCalendar ()
{
    char sysCmd [CMDLINE_MAXLENGTH];
    char details [LINE_MAXLENGTH];
    char schedule [MD5_MAXLENGTH+1];

    iLog ("Downloading calendar...");
    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to download calendar.");
        return (0);
    }


    memset (details, '\0', sizeof (details));
    memset (sysCmd, '\0', sizeof (sysCmd));
    memset (schedule, '\0', sizeof (schedule));

    // Flush the old calendar
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1, "%s -c %s --flush '%s/%s'",
                    config.path_ischedule,
                    config.filename,
                    config.path_playlists, config.playlist_schedule);
    system (sysCmd);
    
    // Download the schedule file
    if (!getScheduleFile ())
    {
        strncpy (details, "Error while getting schedule file.", 
                    sizeof (details));
        return (0);
    }

    // Import the new schedule file
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1, "%s -c %s --import '%s/%s'",
                config.path_ischedule,
                config.filename,
                config.path_playlists, config.playlist_schedule);
    system (sysCmd);
    
    return (1);
}

int downloadProgram (char * program)
{
    char details [LINE_MAXLENGTH];
    char buffer [LINE_MAXLENGTH];

    iLog ("Downloading program '%s'", program);

    if (checkIP (&config) == 0)
    {
        // No IP on the Box, cannot go further
        iError ("Player does not have connectivity to download program.");
        return (0);
    }

    memset (details, '\0', sizeof (details));

    // Create filetree
    if (!createFiletree (program))
        return (0);

    // Getting manifest, layout and playlist
    if (!getManifestFile (program))
    {
        strncpy (details, "Error while getting manifest file.", 
                    sizeof (details));
        sendEndDownload (program, details);
        return (0);
    }
    if (!getLayoutFile (program))
    {
        strncpy (details, "Error while getting layout file.", 
                    sizeof (details));
        sendEndDownload (program, details);
        return (0);
    }
    if (!getPlaylistFile (program))
    {
        strncpy (details, "Error while getting playlist file.", 
                    sizeof (details));
        sendEndDownload (program, details);
        return (0);
    }

    // Downloading missing files
    if (!createDirectory (config.path_library))
    {
        iError ("Unable to create directory '%s'", config.path_library);
        strncpy (details, "Error while creating local filetree.",
                    sizeof (details));
        sendEndDownload (program, details);
        return (0);
    }
    if (!downloadFiles (program, buffer, sizeof (buffer)))
    {
        strncpy (details, buffer, sizeof (details));
        sendEndDownload (program, details);
        return (0);
    }

    if (strlen (buffer))
        strncpy (details, buffer, sizeof (details));
    else
        strncpy (details, "OK", sizeof (details));
    sendEndDownload (program, details);

    return (1);
}

int updateEmission (char * program)
{
    char sysCmd [CMDLINE_MAXLENGTH];

    if (downloadProgram (program))
    {
        // If program is EXACTLY the same, don't do anything...
        if (strncmp (program, config.currentProgram.name, strlen (config.currentProgram.name)) == 0)
        {
            if (compareMD5MetaFiles (program))
            {
                // Same name, same layout, same playlist, same manifest file... Nothing to do !
                iDebug ("[%d] Do not launch program '%s' because already playing (identic).",
                        getpid (), program);
                return (1);
            }
        }

        // If iZoneMgr is running as emission pool launcher, kill the process
        iLog ("[%d] Updating emission '%s'", getpid (), program);
        memset (sysCmd, '\0', sizeof (sysCmd));
        snprintf (sysCmd, sizeof (sysCmd) - 1,
                    "if [ -f \"%s/iZoneMgr.pid\" ]; then "                  \
                        "kill -9 `cat %s/iZoneMgr.pid`; "                   \
                        "rm %s/iZoneMgr.pid ; "                             \
                        "%s -c %s --stop ; "                                \
                        "%s -c %s --programs \"`cat %s/iZoneMgr.pool`\" ; " \
                    "else "                                                 \
                        "%s -c %s --program '%s' ; "                        \
                    "fi",
                    config.path_tmp, 
                    config.path_tmp, 
                    config.path_tmp,
                    config.path_izonemgr, config.filename,
                    config.path_izonemgr, config.filename, config.path_tmp,
                    config.path_izonemgr, config.filename, program);
        system (sysCmd);
        return (1);
    }
    else
        return (0);
}

int synchroProgram (char * program)
{
    char sysCmd [CMDLINE_MAXLENGTH];

    if (downloadProgram (program))
    {
        // If program is EXACTLY the same, don't do anything...
        if (strncmp (program, config.currentProgram.name, strlen (config.currentProgram.name)) == 0)
        {
            if (compareMD5MetaFiles (program))
            {
                // Same name, same layout, same playlist, same manifest file... Nothing to do !
                return (1);
            }
        }

        // If iZoneMgr is running as emission pool launcher, kill the process
        memset (sysCmd, '\0', sizeof (sysCmd));
        snprintf (sysCmd, sizeof (sysCmd) - 1,
                    "if [ -f \"%s/iZoneMgr.pid\" ]; then \
                        kill -9 `cat %s/iZoneMgr.pid`; \
                        rm %s/iZoneMgr.pid ; \
                        %s -c %s --stop ; \
                    fi",
                    config.path_tmp, 
                    config.path_tmp, 
                    config.path_tmp,
                    config.path_izonemgr, config.filename);
        iDebug ("[%d] Killing existing iZoneMgr loop.", getpid ());
        system (sysCmd);

        memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
        snprintf (sysCmd, CMDLINE_MAXLENGTH - 1, "%s -c %s --program '%s'",
                    config.path_izonemgr,
                    config.filename,
                    program);
        system (sysCmd);
        return (1);
    }
    else
    {
        // Maybe we can't download the program, but we still try to launch it
        memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
        snprintf (sysCmd, CMDLINE_MAXLENGTH - 1, "%s -c %s --program '%s'",
                    config.path_izonemgr,
                    config.filename,
                    program);
        system (sysCmd);
        return (0);
    }
}

int synchroPrograms (char * programs)
{
    char program [32*NAME_MAXLENGTH];
    char sysCmd [CMDLINE_MAXLENGTH];
    char * startIndex;
    char * stopIndex;
    int i, j;

    // If iZoneMgr is running as emission pool launcher, kill the process
    memset (sysCmd, '\0', sizeof (sysCmd));
    snprintf (sysCmd, sizeof (sysCmd) - 1,
                "if [ -f \"%s/iZoneMgr.pid\" ]; then \
                    kill -9 `cat %s/iZoneMgr.pid`; \
                    rm %s/iZoneMgr.pid ; \
                    %s -c %s --stop ; \
                fi",
                config.path_tmp, 
                config.path_tmp, 
                config.path_tmp,
                config.path_izonemgr, config.filename);
    iDebug ("[%d] Killing existing iZoneMgr loop.", getpid ());
    system (sysCmd);

    memset (program, '\0', sizeof (program));
    // Parse 'programs' parameter: split the string with ';' delimiter
    // Each part is a different emission's name
    i = 0;
    startIndex  = programs;
    stopIndex   = strchr (startIndex, ';');
    while (i < 32 && stopIndex)
    {
        strncpy (program + i*NAME_MAXLENGTH, startIndex, stopIndex-startIndex);
        i++;
        startIndex = stopIndex + 1;
        stopIndex = strchr (startIndex, ';');
    }
    if (startIndex - programs < strlen (programs))
    {
        // Copy the last part, not ended with the ';'
        strncpy (program + i*NAME_MAXLENGTH, startIndex, NAME_MAXLENGTH);
        i++;
    }

    // Download first emission
    memset (&dwProg, '\0', sizeof (sProgram));
    strncpy (dwProg.name, program, NAME_MAXLENGTH-1);
    common_setDWProgram (&config, &dwProg);
    downloadProgram (program);

    // Create command line to launch emissions
    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1, 
                "%s -c %s --programs '%s' &",
                config.path_izonemgr,
                config.filename,
                programs);
    system (sysCmd);

    // Download emissions following the first
    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    strcpy (sysCmd, "{ ");
    for (j = 1 ; j < i ; j++)
    {
        snprintf (sysCmd + strlen (sysCmd), CMDLINE_MAXLENGTH - 1, 
                    "%s -c %s --download '%s' ; ",
                    config.path_isynchro,
                    config.filename,
                    program + j*NAME_MAXLENGTH);
    }
    strcat (sysCmd, " } &");
    system (sysCmd);

    common_flushDWProgram (&config);
    return (1);
}

int createFiletree (char * program)
{
    char fullPath [FILENAME_MAXLENGTH];

    // Create top directory
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath) - 1, "%s/%s", 
              config.path_playlists, program);
    if (!createDirectory (fullPath))
    {
        iError ("Unable to create directory '%s'", fullPath);
        return (0);
    }

    // Create meta directory
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath) - 1, "%s/%s/%s", 
              config.path_playlists, program, config.playlist_meta);
    if (!createDirectory (fullPath))
    {
        iError ("Unable to create directory '%s'", fullPath);
        return (0);
    }

    // Create media directory
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath) - 1, "%s/%s/%s", 
              config.path_playlists, program, config.playlist_media);
    if (!createDirectory (fullPath))
    {
        iError ("Unable to create directory '%s'", fullPath);
        return (0);
    }

    return (1);
}

int createDirectory (char * path)
{
    struct stat buffer;
    if (stat (path, &buffer) != 0)
    {
        // Directory doesn't exists, trying to create it !
        if (mkdir (path, 0755) != 0)
            return (0);
    }
    return (1);
}

int getManifestFile (char * program)
{
    char uri [HOST_MAXLENGTH];
    char output [FILE_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;
    FILE * fp;
    char path [FILENAME_MAXLENGTH];
    int contentPos;

    memset (path, '\0', sizeof (path));
    snprintf (path, sizeof (path) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_manifest);

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/getfile",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "request=`echo -n '"                            \
                    "{\"program\":\"%s\","                      \
                    "\"file\":\"%s\"}"                          \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                program,
                "manifest");

    status = HTTPRequest (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_OK:
            // Write the manifest on the Hard disk
            fp = fopen (path, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save manifest file '%s'", path);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with Manifest format.");
                return (0);
            }
            else
            {
                contentPos = strlen ("S:OK\n");
                fwrite (output + contentPos, 1, strlen (output), fp);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    return (1);
}

int getScheduleFile ()
{
    char uri [HOST_MAXLENGTH];
    char output [FILE_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;
    FILE * fp;
    char path [FILENAME_MAXLENGTH];
    int contentPos;

    memset (path, '\0', sizeof (path));
    snprintf (path, sizeof (path) - 1, "%s/%s",
                config.path_playlists,
                config.playlist_schedule);

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/getfile",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "request=`echo -n '"                            \
                    "{\"file\":\"%s\"}"                         \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                "schedule");

    status = HTTPRequest (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_OK:
            // Write the schedule on the Hard disk
            fp = fopen (path, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save schedule file '%s'", path);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with Schedule format.");
                return (0);
            }
            else
            {
                contentPos = strlen ("S:OK\n");
                fwrite (output + contentPos, 1, strlen (output), fp);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    return (1);
}

int getLayoutFile (char * program)
{
    char uri [HOST_MAXLENGTH];
    char output [FILE_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;
    FILE * fp;
    char path [FILENAME_MAXLENGTH];
    int contentPos;

    memset (path, '\0', sizeof (path));
    snprintf (path, sizeof (path) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_layout);

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/getfile",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "request=`echo -n '"                            \
                    "{\"program\":\"%s\","                      \
                    "\"file\":\"%s\"}"                          \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                program,
                "layout");

    status = HTTPRequest (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_OK:
            // Write the layout on the Hard disk
            fp = fopen (path, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save layout file '%s'", path);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with Layout format.");
                return (0);
            }
            else
            {
                contentPos = strlen ("S:OK\n");
                fwrite (output + contentPos, 1, strlen (output), fp);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    return (1);
}

int getPlaylistFile (char * program)
{
    char uri [HOST_MAXLENGTH];
    char output [FILE_MAXLENGTH];
    char args [HTTPPOST_MAXLENGTH];
    int status;
    FILE * fp;
    char path [FILENAME_MAXLENGTH];
    int contentPos;

    memset (path, '\0', sizeof (path));
    snprintf (path, sizeof (path) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_playlist);

    memset (uri, '\0', sizeof (uri));
    snprintf (uri, sizeof (uri) - 1, "https://players.%s/players/getfile",
                                        params.composer_host);

    memset (args, '\0', sizeof (args));
    snprintf (args, sizeof (args) - 1,
                "request=`echo -n '"                            \
                    "{\"program\":\"%s\","                      \
                    "\"file\":\"%s\"}"                          \
                "' | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
                program,
                "playlist");

    status = HTTPRequest (uri, args, output, sizeof (output));
    switch (status)
    {
        case STATUS_OK:
            // Write the playlist on the Hard disk
            fp = fopen (path, "wb");
            if (!fp)
            {
                // File is not writable
                iError ("Can't save playlist file '%s'", path);
                return (0);
            }
            if (strncmp (output, "S:OK\n", strlen ("S:OK\n")) != 0 ||
                strlen (output) <= strlen ("S:OK\n"))
            {
                fclose (fp);
                iError ("Error with Playlist format.");
                return (0);
            }
            else
            {
                contentPos = strlen ("S:OK\n");
                fwrite (output + contentPos, 1, strlen (output), fp);
            }
            fclose (fp);
            break;
        default:
            return (0);
    }

    return (1);
}

int executePlugin (char * command)
{
    char buffer [LINE_MAXLENGTH];

    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer)-1, "%s/%s", config.path_actions, command);
    iDebug ("Executing: '%s'", buffer);
    system (buffer);
    return (1);
}

int downloadPackages (char * packages)
{
    // Format of 'packages' string:
    // I: xxx=1.2 www yyy=2.3 zzz=3.4
    // U: aaa bbb
    //
    // Lines starting with 'I: ' means install these packages (with optional versions)
    // Lines starting with 'U: ' means 'Uninstall and purge those packages'

    char * lineSeparator = "\r\n";
    char * contextLine;
    char * line;
    char cmd [CMDLINE_MAXLENGTH];
    FILE * fp;
    char reply [LINE_MAXLENGTH];

    memset (cmd, '\0', sizeof (cmd));

    // For each non-commented line of the output
    for (line = strtok_r (packages, lineSeparator, &contextLine) ;
         line ;
         line = strtok_r (NULL, lineSeparator, &contextLine))
    {
        if (strlen (line) < 1 || line [0] == '#')
            continue;

        if (strlen (line) < 2 || line [1] != ':')
        {
            iError ("Wrong format for packages to Install/Uninstall: '%s'", line);
            return (0);
        }

        // Parse first character to determine actions to do
        switch (line [0])
        {
            case 'I':
                // Install packages
                strncpy (cmd, "sudo apt-get update && sudo apt-get install -y ", sizeof (cmd) - 1);
                strncat (cmd, line + 2, sizeof (cmd) - (1+strlen (cmd)));
                break;
            case 'U':
                // Uninstall packages
                strncpy (cmd, "sudo apt-get purge -y ", sizeof (cmd) - 1);
                strncat (cmd, line + 2, sizeof (cmd) - (1+strlen (cmd)));
                break;
            default:
                iError ("Wrong format for packages to Install/Uninstall: '%s'", line);
                return (0);
        }

        // Remove end-of-line if exists
        while (strlen (cmd) && \
                (cmd [strlen (cmd)-1] == '\r' || \
                 cmd [strlen (cmd)-1] == '\n' || \
                 cmd [strlen (cmd)-1] == ' '))
            cmd [strlen (cmd)-1] = '\0';

        iDebug ("Command: '%s'", cmd);
        fp = popen (cmd, "r");
        if (!fp)
        {
            iError ("Unable to execute: '%s'", cmd);
            return (0);
        }
        memset (reply, '\0', sizeof (reply));
        while (fgets (reply, sizeof (reply), fp))
        {
            iDebug ("%s", reply);
            memset (reply, '\0', sizeof (reply));
        }
        if (pclose (fp) != 0)
        {
            iError ("Error while executing: '%s'", cmd);
            if (line [0] == 'I')
            {
                // ERROR status is blocking function only during installation
                // operation. If we don't purge packages, it is not blocking.
                return (0);
            }
        }
    }
    
    return (1);
}

int downloadFiles (char * program, char * message, int size)
{
    char manifest [FILE_MAXLENGTH];
    char path [FILENAME_MAXLENGTH];
    char link [FILENAME_MAXLENGTH];
    char target [FILENAME_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];
    char * lineSeparator = "\r\n";
    char * splitSeparator = ":";
    char * line, * hash, * filename;
    char * contextLine;
    char * contextSplit;
    FILE * fp;

    if (message && size)
        memset (message, '\0', size);

    // Get manifest file
    memset (path, '\0', sizeof (path));
    snprintf (path, sizeof (path)-1,
                "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_manifest);
    if (!getFileContent (&config, path, manifest, sizeof (manifest)))
    {
        // Ask for update the license, in case the problem is coming from
        // failed uncrypt process...
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer)-1,
                            "%s --force-license &",
                            config.path_isynchro);
        system (buffer);

        if (message && size)
            strncpy (message, "Error while downloading Media files - cannot get Manifest", size-1);
        return (0);
    }

    // For each non-commented line of the file
    for (line = strtok_r (manifest, lineSeparator, &contextLine) ;
         line ;
         line = strtok_r (NULL, lineSeparator, &contextLine))
    {
        if (strlen (line) < 1 || line [0] == '#')
            continue;
        if (line [0] == ':')
        {
            iError ("Wrong format for manifest, line is '%s'", line);
            if (message && size)
                snprintf (message, size, 
                          "Error while downloading Media files - incorrect Manifest");
            return (0);
        }

        iDebug ("Checking media %s", line);

// TODO
// Check MD5Sum if file exists before download
//  and
// Check MD5Sum if file exsits after download

        hash        = strtok_r (line, splitSeparator, &contextSplit);
        filename    = strtok_r (NULL, splitSeparator, &contextSplit);
        while (filename [strlen (filename) - 1] == '\r' ||
               filename [strlen (filename) - 1] == '\n')
            filename [strlen (filename) - 1] = '\0';

        // Is the file already downloaded ?
        memset (path, '\0', sizeof (path));
        snprintf (path, sizeof (path) - 1,
                    "%s/%s",
                        config.path_library,
                        hash);
        fp = fopen64 (path, "r");
        if (!fp)
        {
            iDebug ("File does not exist: %s", path);
            // File doesn't exist in the library, download it !
            if (!HTTPDownload (filename, hash, path))
            {
                iError ("File '%s' cannot be downloaded", filename);
                if (message && size)
                {
                    if (strlen (message))
                        snprintf (message + strlen (message), 
                                  size - strlen (message),
                                  " - %s", filename);
                    else
                        snprintf (message + strlen (message), 
                                  size - strlen (message),
                                  "Error while downloading Media file %s", filename);
                }

                // Error while downloading file is not a blocking error,
                // we continue to download other files, even if the emission will not be complete
                //return (0);
            }
        }
        else
        {
            iDebug ("File exists: %s", path);
            fclose (fp);
        }

        // If link doesn't exist in the media directory, link it
        memset (link, '\0', sizeof (link));
        snprintf (link, sizeof (link) - 1,
                    "%s/%s/%s/%s",
                        config.path_playlists,
                        program,
                        config.playlist_media,
                        filename);
        fp = fopen64 (link, "r");
        if (!fp)
        {
            iDebug ("Link does not exist: %s", link);
            // Delete symlink in case it redirects on deleted file
            deleteSymlink (link);

            // Create symlink
            if (createSymlink (path, link) != 0 &&
                    (message && size))
            {
                snprintf (message + strlen (message), 
                          size - strlen (message),
                          "Error while creating symlink to Media file %s",
                          filename);
            }
        }
        else
        {
            iDebug ("Link exists: %s", link);
            fclose (fp);

            // Check the symlink is on the correct file !
            memset (target, '\0', sizeof (target));
            memset (buffer, '\0', sizeof (buffer));
            snprintf (buffer, sizeof (buffer)-1,
                            "basename `readlink \"%s\"`", link);
            fp = popen (buffer, "r");
            if (fp)
            {
                if (!fgets (target, sizeof (target), fp))
                {
                    iError ("Unable to check the integrity of %s", link);
                }
                else
                {
                    while (strlen (target) &&
                            (target [strlen (target)-1] == '\r' ||
                             target [strlen (target)-1] == '\n'))
                        target [strlen (target)-1] = '\0';

                    iDebug ("Checking link '%s' -> '%s'", link, path);
                    // Compare the current and expected filename
                    if (strncmp (target, hash, strlen (hash)) != 0)
                    {
                        iError ("%s: Not linked to %s but %s", 
                                filename, hash, target);
                        iDebug ("Deleting %s", link);
                        deleteSymlink (link);
                        // Create symlink
                        iDebug ("Creating link %s to %s", link, path);
                        if (createSymlink (path, link) != 0 &&
                                (message && size))
                        {
                            snprintf (message + strlen (message), 
                                      size - strlen (message),
                                      "Error while creating symlink to Media file %s",
                                      filename);
                        }
                    }

                    if (!checkMd5Sum (path, hash))
                    {
                        // If md5 check fails, filename is automatically removed
                        // We still have to delete symlink
                        deleteSymlink (link);
                    }
                }
            }
            else
            {
                iError ("Cannot get real file pointed by %s", filename);
            }
            pclose (fp);
        }
    }

    return (1);
}

int checkMd5Sum (char * path, char * expected)
{
    char buffer [CMDLINE_MAXLENGTH];
    char md5 [HASH_MAXLENGTH+1];
    FILE * fp = NULL;

    // Check its MD5 sum
    memset (buffer, '\0', sizeof (buffer));
    memset (md5, '\0', sizeof (md5));
    snprintf (buffer, sizeof (buffer) - 1, "/usr/bin/md5sum '%s'", path);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("Unable to check MD5 sum for '%s' (no1)", path);
        unlink (path);
        return (0);
    }
    if (!fgets (md5, sizeof (md5), fp))
    {
        iError ("Unable to check MD5 sum for '%s' (no2)", path);
        pclose (fp);
        unlink (path);
        return (0);
    }
    if (pclose (fp) != 0)
    {
        iError ("Unable to check MD5 sum for '%s' (no3)", path);
        unlink (path);
        return (0);
    }

    if (strncmp (md5, expected, strlen (expected)) != 0)
    {
        iError ("Integrity check fails for file '%s'", path);
        iDebug ("Expected: %s", expected);
        iDebug ("Real MD5: %s", md5);
        unlink (path);
        return (0);
    }
    return (1);
}

int createSymlink (char * target, char * link)
{
    if (symlink (target, link) != 0)
    {
        iError ("Unable to create link to %s", target);
        return (1);
    }
    return (0);
}

int deleteSymlink (char * path)
{
    return (unlink (path));
}

int HTTPDownloadWP (char * outputFile)
{
    char buffer [LINE_MAXLENGTH];
    char uri    [HOST_MAXLENGTH];
    char args   [HTTPPOST_MAXLENGTH];
    FILE * fp;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));

    // Compose URL
    snprintf (uri, sizeof (uri) - 1, 
                "https://players.%s/players/downloadwp", params.composer_host);

    // Compose command-line
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " --dns-timeout=3"          \
                " --connect-timeout=15"     \
                " --read-timeout=15"        \
                " --secure-protocol=TLSv1"  \
                " --certificate=%s"         \
                " --private-key=%s/%s"      \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document='%s'"   \
                " %s",
                PATH_WGET,
                config.path_certificate,
                config.path_tmp, CHECKED_LICENSE,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                args,
                outputFile,
                uri);
    // Launch command-line
    system (buffer);

    // Check if the file is downloaded
    fp = fopen64 (outputFile, "r");
    if (!fp)
        return (0);

    fclose (fp);
    return (1);
}

int HTTPDownload (char * file, char * hash, char * outputFile)
{
    char buffer [LINE_MAXLENGTH];
    char uri    [HOST_MAXLENGTH];
    char args   [HTTPPOST_MAXLENGTH];
    char md5    [MD5_MAXLENGTH + 1];
    FILE * fp;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));

    // Compose URL
    snprintf (uri, sizeof (uri) - 1, 
                "https://players.%s/players/downloadmedia", params.composer_host);

    // Compose POST details
    snprintf (args, sizeof (args) - 1,
            "file=`echo -n '%s'"                            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "hash=`echo -n '%s'"                            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
            file, hash);

    // Compose command-line
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " --dns-timeout=3"          \
                " --connect-timeout=15"     \
                " --read-timeout=15"        \
                " --secure-protocol=TLSv1"  \
                " --certificate=%s"         \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document='%s'"   \
                " %s",
                PATH_WGET,
                config.path_certificate,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                args,
                outputFile,
                uri);

    // Launch command-line
    system (buffer);

/*
iDebug ("Command is :");
iDebug ("%s", buffer);
*/

    // Check if the file is downloaded
    fp = fopen64 (outputFile, "r");
    if (!fp)
        return (0);
    fclose (fp);

    // Check its MD5 sum
    memset (buffer, '\0', sizeof (buffer));
    memset (md5, '\0', sizeof (md5));
    snprintf (buffer, sizeof (buffer) - 1, "/usr/bin/md5sum '%s'", outputFile);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("Unable to check MD5 sum for '%s' (no1)", file);
        unlink (outputFile);
        return (0);
    }
    if (!fgets (md5, sizeof (md5), fp))
    {
        iError ("Unable to check MD5 sum for '%s' (no2)", file);
        pclose (fp);
        unlink (outputFile);
        return (0);
    }
    if (pclose (fp) != 0)
    {
        iError ("Unable to check MD5 sum for '%s' (no3)", file);
        unlink (outputFile);
        return (0);
    }

    if (strncmp (md5, hash, strlen (hash)) != 0)
    {
        iError ("Integrity check fails for file '%s'", file);
        iDebug ("Expected: %s", hash);
        iDebug ("Real MD5: %s", md5);
        unlink (outputFile);
        return (0);
    }

    return (1);
}

int HTTPDownloadPkg (char * file, char * hash, char * outputFile)
{
    char buffer [LINE_MAXLENGTH];
    char uri    [HOST_MAXLENGTH];
    char args   [HTTPPOST_MAXLENGTH];
    char md5    [MD5_MAXLENGTH + 1];
    FILE * fp;

    memset (uri, '\0', sizeof (uri));
    memset (args, '\0', sizeof (args));

    // Compose URL
    snprintf (uri, sizeof (uri) - 1, 
                "https://players.%s/players/downloadpkg", params.composer_host);

    // Compose POST details
    snprintf (args, sizeof (args) - 1,
            "file=`echo -n '%s'"                            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`&"   \
            "hash=`echo -n '%s'"                            \
            " | od -t x1 -w2048 -A n | tr \\\" \\\" %%`",
            file, hash);

    // Compose command-line
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                "%s"                        \
                " -q"                       \
                " -t1"                      \
                " --dns-timeout=3"          \
                " --connect-timeout=15"     \
                " --read-timeout=15"        \
                " --secure-protocol=TLSv1"  \
                " --certificate=%s"         \
                " --private-key=%s/%s"      \
                " --ca-directory=%s"        \
                " --user-agent=%s/%s"       \
                " --post-data=\"%s\""       \
                " --output-document='%s'"   \
                " %s",
                PATH_WGET,
                config.path_certificate,
                config.path_tmp, CHECKED_LICENSE,
                config.path_ca,
                ISYNCHRO_APPNAME, ISYNCHRO_VERSION,
                args,
                outputFile,
                uri);

    // Launch command-line
    system (buffer);

/*
iDebug ("Command is :");
iDebug ("%s", buffer);
*/

    // Check if the file is downloaded
    fp = fopen64 (outputFile, "r");
    if (!fp)
        return (0);
    fclose (fp);

    // Check its MD5 sum
    memset (buffer, '\0', sizeof (buffer));
    memset (md5, '\0', sizeof (md5));
    snprintf (buffer, sizeof (buffer) - 1, "/usr/bin/md5sum '%s'", outputFile);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("Unable to check MD5 sum for '%s' (no1)", file);
        unlink (outputFile);
        return (0);
    }
    if (!fgets (md5, sizeof (md5), fp))
    {
        iError ("Unable to check MD5 sum for '%s' (no2)", file);
        pclose (fp);
        unlink (outputFile);
        return (0);
    }
    if (pclose (fp) != 0)
    {
        iError ("Unable to check MD5 sum for '%s' (no3)", file);
        unlink (outputFile);
        return (0);
    }

    if (strncmp (md5, hash, strlen (hash)) != 0)
    {
        iError ("Integrity check fails for file '%s'", file);
        unlink (outputFile);
        return (0);
    }

    return (1);
}

char * getScheduleMD5 (char * buffer, int size)
{
    char fileContent [FILE_MAXLENGTH];                                                                 
    char filename [FILENAME_MAXLENGTH];
    char md5 [MD5_MAXLENGTH+1];

    memset (md5, '\0', sizeof (md5));
    memset (filename, '\0', sizeof (filename));

    snprintf (filename, sizeof (filename)-1, "%s/%s", config.path_playlists, config.playlist_schedule);
    if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))                 
    {   
        if (!common_getStringMD5 (fileContent, strlen (fileContent), md5, sizeof (md5)))       
        {
            iError ("Cannot get MD5 of file %s", filename);
            return (NULL);                                                                        
        }
    }
    else
    {
        iError ("Cannot get content of file '%s'", filename);
        return (NULL);
    }

    memset (buffer, '\0', size);
    strncpy (buffer, md5, size-1);
    return (buffer);
}

int compareMD5MetaFiles (char * program)
{
    char fileContent [FILE_MAXLENGTH];                                                                 
    char filename [FILENAME_MAXLENGTH];
    char md5 [MD5_MAXLENGTH+1];

    // Manifest
    memset (md5, '\0', sizeof (md5));
    memset (filename, '\0', sizeof (filename));
    snprintf (filename, sizeof (filename) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_manifest);
    if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))                 
    {   
        if (!common_getStringMD5 (fileContent, strlen (fileContent), md5, sizeof (md5)))       
        {
            iError ("Cannot get MD5 of file %s", filename);
            return (0);
        }
    }
    else
    {
        iError ("Cannot get content of file '%s'", filename);
        return (0);
    }
    if (strncmp (md5, config.currentProgram.manifest, MD5_MAXLENGTH) != 0)
        return (0);

    // Playlist
    memset (md5, '\0', sizeof (md5));
    memset (filename, '\0', sizeof (filename));
    snprintf (filename, sizeof (filename) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_playlist);
    if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))                 
    {   
        if (!common_getStringMD5 (fileContent, strlen (fileContent), md5, sizeof (md5)))       
        {
            iError ("Cannot get MD5 of file %s", filename);
            return (0);
        }
    }
    else
    {
        iError ("Cannot get content of file '%s'", filename);
        return (0);
    }
    if (strncmp (md5, config.currentProgram.playlist, MD5_MAXLENGTH) != 0)
        return (0);

    // Layout
    memset (md5, '\0', sizeof (md5));
    memset (filename, '\0', sizeof (filename));
    snprintf (filename, sizeof (filename) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_layout);
    if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))                 
    {   
        if (!common_getStringMD5 (fileContent, strlen (fileContent), md5, sizeof (md5)))       
        {
            iError ("Cannot get MD5 of file %s", filename);
            return (0);
        }
    }
    else
    {
        iError ("Cannot get content of file '%s'", filename);
        return (0);
    }
    if (strncmp (md5, config.currentProgram.layout, MD5_MAXLENGTH) != 0)
        return (0);

    // Playlist, Layout and Manifest files of the program given in parameter are the one currently
    // played.
    return (1);
}

int getPlayerStatus (char * output, int size)
{
    char file [FILE_MAXLENGTH];
    char layout [FILENAME_MAXLENGTH];
    int nbExpectedZones;
    int nbRealZones;

    memset (output, '\0', size);
    memset (layout, '\0', sizeof (layout));

    // Get the Layout file
    iDebug ("Getting zones from the Layout file...");
    snprintf (layout, sizeof (layout) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                config.currentProgram.name,
                config.playlist_meta,
                config.playlist_layout);
    if (!getFileContent (&config, layout, file, FILE_MAXLENGTH))
    {
        snprintf (output, size, "ERROR - Cannot get Layout file for %s", 
                                config.currentProgram.name);
        iError ("Can't get Layout file");
        return (1);
    }
    // Get the number of expected zones
    nbExpectedZones = getNbZonesFromLayout (file);

    // Get the real number of playing zones in memory
    nbRealZones = getNbZonesFromMemory ();
    
    if (nbRealZones != nbExpectedZones)
    {
        snprintf (output, size,
                    "ERROR - Player is not correctly playing the emission");
        iError ("Player status is ERROR (%d/%d)", nbRealZones, nbExpectedZones);
        return (1);
    }

    // Everything is fine
    snprintf (output, size, "OK");
    return (1);
}

int getNbZonesFromLayout (char * layout)
{
    int nbZones = 0;
    char * lineSeparator = "\r\n";
    char * contextLine;
    char * line;

    for (line = strtok_r (layout, lineSeparator, &contextLine) ;
            line ;
            line = strtok_r (NULL, lineSeparator, &contextLine))
    {
        if (strlen (line) < 1 || line [0] == '#')
            continue;

        nbZones++;
    }

    return (nbZones);
}

int getNbZonesFromMemory ()
{
    int nbZones = 0;

    int shmid;
    int shmsize = SHM_ZONESSIZE;
    sZones * shm;
    int i;

    // Open the Shared memory
    if ((shmid = shmget (MEMKEY_ZONES, shmsize, 0666)) < 0)
    {
        iError ("Cannot get shared memory for Zones");
        return (-1);
    }

    if ((char *)(shm = (sZones *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError ("Cannot get shared memory for Zones");
        return (-1);
    }

    // Show all zone characteristics
    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strlen ((*shm)[i].name))
        {
            switch ((*shm)[i].status)
            {
                case STATUS_OK:
                    nbZones++;
                    break;
                case STATUS_NOK:
                    iError ("[%s] Status ERROR", (*shm)[i].name);
                    break;
                case STATUS_UNKNOWN:
                default:
                    iError ("[%s] Status UNKNOWN", (*shm)[i].name);
            }
        }
    }

    // Close the Shared memory
    shmdt (shm);
    return (nbZones);
}
