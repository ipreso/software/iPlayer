// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iLaunch/iLaunch.h
 * Description: Launch the iPlayer software
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#ifndef VERSION
#define ILAUNCH_VERSION     "Version Unknown"
#else
#define ILAUNCH_VERSION     VERSION
#endif

#define ILAUNCH_APPNAME     "iLaunch (iPlayer Launcher)"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

#define DISPLAY_NAME        ":0.0"
