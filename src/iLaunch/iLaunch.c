// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iLaunch/iLaunch.c
 * Description: Launch the iPlayer software
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.22: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <X11/Xlib.h>

#include "iLaunch.h"
#include "iCommon.h"
#include "log.h"
#include "license.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iLaunch [options]\n"                                                   \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    -c, --conf '<file>'      Set the configuration file of the iPlayer\n" \
"\n"                                                                    \

// Wait 5s maximum for the PID file to be created by iSchedule
#define LAUNCH_SCHEDULE_TIMEOUT 5

#define MAX_PROPERTY_VALUE_LEN 4096

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"conf",        required_argument,  0, 'c'},
    {0, 0, 0, 0}
};

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char params [128];
    char command [CMDLINE_MAXLENGTH];
    sConfig config;
    sParams sparams;
    int wait;
    Display * disp;

    memset (params, '\0', sizeof (params));
    memset (command, '\0', sizeof (command));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "hvc:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", ILAUNCH_APPNAME, ILAUNCH_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (params, optarg, sizeof (params)-1);
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (params) <= 0)
    {
        // Default configuration file
        strncpy (params, DEFAULT_CONFIGURATIONFILE, sizeof (params)-1);
    }

    // Load the configuration file (override if already loaded)
    if (common_loadConfig (params) < 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        return (EXIT_FAILURE);
    }
    else
    {
        common_getConfig (params, &config);
    }

    // Prepare background logo
    snprintf (command, sizeof (command) - 1,
                 "if [ ! -e \"%s/%s\" ]; then " \
                     "if [ -e \"%s\" ]; then "  \
                        "mkdir -p '%s' ; "      \
                        "cp '%s' '%s/%s'; "     \
                     "fi; "                     \
                 "fi",
                config.path_library, WALLPAPER_BASE,
                LOGO_FILE,
                config.path_library,
                LOGO_FILE,
                config.path_library, WALLPAPER_BASE);
    iDebug ("=> %s", command);
    system (command);

    // Wait for an available X server
    wait = 0;
    while (wait < 10 && !(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        iInfo ("Waiting to have a running X server...");
        wait++;
        sleep (1);
    }
    if (wait >= 10)
    {
        iError ("X server not available after %d seconds. Exiting...", wait);
        fprintf (stdout, "Error: X not available, exiting...\n");
        return (EXIT_FAILURE);
    }
    // Close it !
    if (disp)
        XCloseDisplay (disp);

    // Convert background logo to the correct size
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "/usr/bin/convert %s/%s -resize `%s -r`! %s/`%s -r`_%s",
                config.path_library, WALLPAPER_BASE,
                config.path_iwm, config.path_library,
                config.path_iwm, WALLPAPER_BASE);
    system (command);

    // Convert custom background logo to the correct size
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "if [ -e \"%s/%s\" ]; then "                    \
                    "/usr/bin/convert %s/%s -resize `%s -r`! %s/`%s -r`_%s ; "
                "fi",
                config.path_library, CUSTOM_BASE,
                config.path_library, CUSTOM_BASE,
                config.path_iwm, config.path_library,
                config.path_iwm, CUSTOM_BASE);
    system (command);

    // Set background logo as wallpaper
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
                "if [ -e \"%s/`%s -r`_%s\" ]; then "                        \
                    "/usr/bin/feh --bg-center %s/`%s -r`_%s 2>/dev/null; "  \
                "else "                                                     \
                    "/usr/bin/feh --bg-center %s/`%s -r`_%s 2>/dev/null ; " \
                "fi",
                config.path_library, config.path_iwm, CUSTOM_BASE,
                config.path_library, config.path_iwm, CUSTOM_BASE,
                config.path_library, config.path_iwm, WALLPAPER_BASE);

    if (common_loadParams (config.path_params, &config) < 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        return (EXIT_FAILURE);
    }
    else
    {
        common_getParams (config.path_params, &config, &sparams);
    }

    // Apply parameters to the system
    if (!applyParameters (&config, &sparams))
    {
        iCrit ("Can't apply parameters to the system.");
        return (EXIT_FAILURE);
    }

    // Close all existing windows
    snprintf (command, sizeof (command) - 1,
                 "%s -z '%s' -a", 
                 config.path_iwm,
                 config.filename);
    system (command);

    // Remove old semaphores
    system ("rm -f /dev/shm/sem.iplayer.*");

    // Go to the correct desktop
    snprintf (command, sizeof (command) - 1,
                 "%s -z '%s' -d%d", 
                 config.path_iwm,
                 config.filename,
                 config.desktop_blank);
    system (command);

    // Download last version of license
    snprintf (command, sizeof (command) - 1,
                 "%s -c '%s' --license", 
                 config.path_isynchro,
                 config.filename);
    system (command);

    // Save it into Shared Memory
    if (common_saveConfig (params, &config) <= 0)
    {
        iCrit ("Can't save configuration file '%s'", params);
        return (EXIT_FAILURE);
    }

    // Update composer's hostname
    memset (command, '\0', sizeof (command));
    snprintf (command, sizeof (command) - 1,
              "%s -c %s --hostname",
                config.path_isynchro,
                config.filename);
    system (command);

    // Launch the Scheduler as a daemon
    snprintf (command, sizeof (command) - 1,
                "/usr/bin/setsid %s -c '%s' --start >/dev/null 2>&1 < /dev/null &",
                config.path_ischedule,
                config.filename);
    iDebug ("=> '%s'", command);
    system (command);

    // Wait for the PID to be there
    wait = 0;
    while (access (IPLAYER_PID_FILE, F_OK) != 0 && wait < LAUNCH_SCHEDULE_TIMEOUT)
    {
        iDebug ("%s does not exist yet. Waiting 1s...", IPLAYER_PID_FILE);

        wait++;
        sleep (1);
    }
    if (access (IPLAYER_PID_FILE, F_OK) != 0)
    {
        iCrit ("iPlayer launch timeout: %s does not exist after %s seconds.");
        return (1);
    }

    iInfo ("iPlayer started.");
    return (0);
}

