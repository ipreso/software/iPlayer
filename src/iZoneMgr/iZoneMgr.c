// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iZoneMgr/iZoneMgr.c
 * Description: Manage layout and zones
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Updated layout parsing
 *  - 2009.10.06: Added program memorization
 *  - 2009.01.28: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>

#include "iCommon.h"
#include "license.h"
#include "log.h"
#include "SHM.h"
#include "iZoneMgr.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iZoneMgr [options]\n"                                                  \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -p, --program '<name>'   Set the program to be played\n"           \
"    -q, --programs '<name1>[,name2,...]' Set programs to be played\n"  \
"    -s, --stop               Stop all zones that are currently loaded\n" \
"    -v, --version            Show the version of this tool\n"          \
"    -c, --conf '<file>'      Set the configuration file of the iPlayer\n" \
"    -b, --block              Don't daemonize the process\n"            \
"    -o, --once               Play at least once all zones and stops\n" \
"    -l, --list               List zones in memory\n"                   \
"\n"                                                                    \

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"program",     required_argument,  0, 'p'},
    {"programs",    required_argument,  0, 'q'},
    {"stop",        no_argument,        0, 's'},
    {"version",     no_argument,        0, 'v'},
    {"conf",        required_argument,  0, 'c'},
    {"list",        no_argument,        0, 'l'},
    {"block",       no_argument,        0, 'b'},
    {"once",        no_argument,        0, 'o'},
    {0, 0, 0, 0}
};

sConfig     config;
sParams     params;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char paramsOpt [128];
    char program [128];
    char action = '\0';
    int block = 0;
    int once = 0;
    int ended = 0;

/*
int i;
char p [CMDLINE_MAXLENGTH];
memset (p, '\0', CMDLINE_MAXLENGTH);
snprintf (p, CMDLINE_MAXLENGTH-1, "[%d] ", getpid ());
for (i = 0 ; i < argc ; i++)
{
    snprintf (p + strlen (p), CMDLINE_MAXLENGTH-(1+strlen (p)), "%s ", argv[i]);
}
iDebug ("%s", p);
*/

    memset (paramsOpt, '\0', sizeof (paramsOpt));
    memset (program, '\0', sizeof (program));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "hvc:p:q:lsbo",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", IZONEMGR_APPNAME, IZONEMGR_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (paramsOpt, optarg, sizeof (paramsOpt)-1);
                break;
            case 'p':
                strncpy (program, optarg, sizeof (program)-1);
                action = 'p';
                break;
            case 'q':
                strncpy (program, optarg, sizeof (program)-1);
                action = 'q';
                break;
            case 'l':
                action = 'l';
                break;
            case 's':
                action = 's';
                break;
            case 'b':
                block = 1;
                break;
            case 'o':
                once = 1;
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (paramsOpt) <= 0)
    {
        // Default configuration file
        strncpy (paramsOpt, DEFAULT_CONFIGURATIONFILE, sizeof (paramsOpt)-1);
    }

    //iDebug ("Loading configuration file '%s'", paramsOpt);
    if (common_getConfig (paramsOpt, &config) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }

    if (common_getParams (config.path_params, &config, &params) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load parameters. Exiting.");
        return (EXIT_FAILURE);
    }

    switch (action)
    {
        case 'p':
            stopQueues ();
            launchPlaylist (program, once);
            if (block && once)
            {
                // Inform shared memory to not loop !
                shm_doNotLoopZones ();

                while (block && once && !ended)
                {
                    // Loop while at least one zone is in its first loop
                    usleep (1000*500);                      // 500ms
                    ended = shm_getLoopStatus ();
                }
            }
            break;
        case 'q':
            // Infinite loop to launch all emissions given
            launchLoop (program);
            break;
        case 'l':
            fprintf (stdout, "Program: '%s'\n", config.currentProgram.name);
            shm_listZones ();
            break;
        case 's':
            stopQueues ();
            break;
        default:
            fprintf (stderr, "Nothing to do. Exiting...");
    }

    return (EXIT_SUCCESS);
}

void launchLoop (char * programs)
{
    int i, j, k, l;
    char zones [ZONE_NBMAX][ZONE_MAXLENGTH];
    char sysCmd [CMDLINE_MAXLENGTH];
    char program [32*NAME_MAXLENGTH];
    char sequence [32*NAME_MAXLENGTH];
    char * startIndex;
    char * stopIndex;

    // If another loop exists, kill iZoneMrg+iZone processes
    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                "%s -c %s --stop",
                config.path_izonemgr,
                config.filename);
    system (sysCmd);

    if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
    {
        iError ("[%d] Can't get the zones to supervise them", getpid ());
        memset (zones, '\0', ZONE_NBMAX*ZONE_MAXLENGTH);
    }

    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strlen (zones [i]))
        {
            iDebug ("Zone %s is still present.", zones [i]);
            sleep (1);
            if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
            {
                iError ("[%d] Can't get the zones to supervise them", getpid ());
                memset (zones, '\0', ZONE_NBMAX*ZONE_MAXLENGTH);
            }
            else
            {
                i = -1;
            }
        }
    }

    // Save PID file
    memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
    snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                "echo \"%d\" > %s/iZoneMgr.pid",
                getpid (), config.path_tmp);
    system (sysCmd);
    iDebug ("Saving PID %d as looping process...", getpid ());

    // Loop
    memset (program, '\0', sizeof (program));
    // Parse 'programs' parameter: split the string with ';' delimiter
    // Each part is a different emission's name
    i = 0;
    startIndex  = programs;
    stopIndex   = strchr (startIndex, ';');
    while (i < 32 && stopIndex)
    {
        strncpy (program + i*NAME_MAXLENGTH, startIndex, stopIndex-startIndex);
        i++;
        startIndex = stopIndex + 1;
        stopIndex = strchr (startIndex, ';');
    }
    if (startIndex - programs < strlen (programs))
    {
        // Copy the last part, not ended with the ';'
        strncpy (program + i*NAME_MAXLENGTH, startIndex, NAME_MAXLENGTH);
        i++;
    }

    // Infinite loop to launch emissions
    while (1)
    {
        for (j = 0 ; j < i ; j++)
        {
            memset (sequence, '\0', sizeof (sequence));
            for (k = 0, l = j ; k < i ; k++)
            {
                if (strlen (sequence) > 0 &&
                    strlen (sequence) < sizeof (sequence) - 1)
                    sequence [strlen (sequence)] = ';';

                strncat (sequence, program + l*NAME_MAXLENGTH, NAME_MAXLENGTH);

                l++;
                if (l >= i)
                    l = 0;
            }

            memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
            snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                        "echo \"%s\" > %s/iZoneMgr.pool",
                        sequence, config.path_tmp);
            system (sysCmd);
            iDebug ("[%d] Loop sequence is '%s'", getpid (), sequence);

            memset (sysCmd, '\0', CMDLINE_MAXLENGTH);
            snprintf (sysCmd, CMDLINE_MAXLENGTH - 1,
                        "%s -c %s --program '%s' --once --block",
                        config.path_izonemgr,
                        config.filename,
                        program + j*NAME_MAXLENGTH);
            iLog ("[%d] Emission pool: processing program '%s'", 
                    getpid (), program + j*NAME_MAXLENGTH);
            system (sysCmd);
        }
    }
}

void launchPlaylist (char * program, int once)
{
    char file [FILE_MAXLENGTH];
    char layout [FILENAME_MAXLENGTH];
    char playlist [FILENAME_MAXLENGTH];
    char manifest [FILENAME_MAXLENGTH];
    char md5sum [MD5_MAXLENGTH+1];
    sProgram newProgram;

    memset (layout, '\0', sizeof (layout));
    memset (playlist, '\0', sizeof (playlist));
    memset (manifest, '\0', sizeof (manifest));

    // Compose paths
    snprintf (layout, sizeof (layout) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_layout);
    snprintf (playlist, sizeof (playlist) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_playlist);
    snprintf (manifest, sizeof (manifest) - 1, "%s/%s/%s/%s",
                config.path_playlists,
                program,
                config.playlist_meta,
                config.playlist_manifest);

    // Get new program informations
    strncpy (newProgram.name, program, NAME_MAXLENGTH-1);

    iLog ("Playing emission '%s'", program);

    // Should inform the name of the program before other actions
    // Some actions (like determining path of the items) are requiring
    // the name of the program...
    common_setProgram (&config, &newProgram);

    // Get the layout file
    if (!getFileContent (&config, layout, file, FILE_MAXLENGTH))
        return;
    if (!common_getStringMD5 (file, strlen (file), md5sum, sizeof (md5sum)))
    {
        iError ("Can't get Layout file MD5.");
        return;
    }
    else
    {
        strncpy (newProgram.layout, md5sum, MD5_MAXLENGTH + 1);
    }
    
    if (!initLayout (file))
    {
        iError ("Can't initialize the layout for program '%s'", program);
        return;
    }

    // Get the playlist file
    if (!getFileContent (&config, playlist, file, FILE_MAXLENGTH))
        return;
    if (!initQueues (file))
    {
        iError ("Cant initialize the queues for playlist '%s'");
        return;
    }
    if (!common_getStringMD5 (file, strlen (file), md5sum, sizeof (md5sum)))
    {
        iError ("Can't get Playlist file MD5.");
        return;
    }
    else
    {
        strncpy (newProgram.playlist, md5sum, MD5_MAXLENGTH + 1);
    }

    // Get the manifest file
    if (!getFileContent (&config, manifest, file, FILE_MAXLENGTH))
        return;
    if (!common_getStringMD5 (file, strlen (file), md5sum, sizeof (md5sum)))
    {
        iError ("Can't get Manifest file MD5.");
        return;
    }
    else
    {
        strncpy (newProgram.manifest, md5sum, MD5_MAXLENGTH + 1);
    }

    // Override the program to inform MD5 too (synchronization up-to-date)
    common_setProgram (&config, &newProgram);
    iDebug ("- Layout   : '%s'", newProgram.layout);
    iDebug ("- Playlist : '%s'", newProgram.playlist);
    iDebug ("- Manifest : '%s'", newProgram.manifest);

    // Save the new playlist into the parameter file
    memset (params.box_program, '\0', NAME_MAXLENGTH);
    strncpy (params.box_program, config.currentProgram.name, NAME_MAXLENGTH);
    writeParametersFile (&config, &params);
    
    // Launch the queues
    if (!playQueues (once))
    {
        iError ("Can't start the queues.");
        return;
    }
}

int initLayout (char * layout)
{
    char buffer [256];
    char * line;
    char * lineSeparator = "\r\n";
    char * splitSeparator = ":";
    char * contextLine;
    char * contextSplit;
    int    realLayers [ZONE_NBMAX];
    int    realLayer;
    sZones zones;
    sZone  zone;
    int    i;

    if (!shm_createZonesSM ())
        return (0);
    if (!shm_createAppsSM ())
        return (0);

    memset (zones, '\0', sizeof (zones));

    memset (realLayers, '\0', sizeof (int) * ZONE_NBMAX);
    i = 0;

    for (line = strtok_r (layout, lineSeparator, &contextLine) ;
            line ;
            line = strtok_r (NULL, lineSeparator, &contextLine))
    {
        if (strlen (line) < 1 || line [0] == '#')
            continue;

        // Parse the line
        memset (zone.name, '\0', ZONE_MAXLENGTH);
        strncpy (zone.name, 
                    strtok_r (line, splitSeparator, &contextSplit), 
                    ZONE_MAXLENGTH);

        zone.hidden = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.layer  = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.x      = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.y      = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.width  = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.height = atoi (strtok_r (NULL, splitSeparator, &contextSplit));
        zone.format = atoi (strtok_r (NULL, splitSeparator, &contextSplit));

        // Get valid layer for Xorg (1..LAYER_MAX),
        // according to the Composer's layer (1..ZONE_NBMAX)
        // [cf. http://tickets.ipreso.com/view.php?id=335]
        realLayer = getTheLowestLayer (&zones, realLayers, &zone);

        // Launch the Zone process
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer)-1,
                    "%s -c %s -z '%s' -i%d -l%d -o %dx%d -g %dx%d -f%d -x",
                    config.path_izone,
                    config.filename,
                    zone.name,
                    zone.hidden,
                    realLayer,
                    zone.x, zone.y,
                    zone.width, zone.height,
                    zone.format);
        //iDebug ("Executing : '%s'", buffer);
        system (buffer);

        if (i >= 0 && i < ZONE_NBMAX)
        {
            realLayers [i] = realLayer;
            memcpy (&(zones [i]), &zone, sizeof (zone));
            i++;
        }
    }

    return (1);
}

int getTheLowestLayer (sZones * zonesList, int * arrayLayers, sZone * zone)
{
    // Default layer is the current zone's layer
    int layer       = 1;
    //int lowest      = 1;
    sZone * tmpZone = NULL;
    int i;

    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        tmpZone = &((*zonesList)[i]);

        // Ignore not initiliazed zones
        if (tmpZone->layer <= 0)
            continue;

        // Ignore zones marked as Above our target
        if (tmpZone->layer >= zone->layer)
            continue;

        // Increment layer if zones are intersecting
        // (below or above)
        if (isCrossing (zone, tmpZone))
        {
            if (*(arrayLayers + i) >= layer)
            {
                layer = *(arrayLayers + i) + 1;
                //iDebug ("Zone %s (%d) is above %s (%d, in real %d). Increasing layer to %d",
                //        zone->name, zone->layer,
                //        tmpZone->name, tmpZone->layer,
                //        *(arrayLayers + i),
                //        layer);
            }
        }
    }

    // Never returns a layer that can not be managed by the WM
    if (layer > LAYER_MAX)
        layer = LAYER_MAX;
    if (layer < 1)
        layer = 1;

    iDebug ("Zone '%s' (configured layer %d) stand on Xorg layer #%d", 
            zone->name, zone->layer, layer);
    return (layer);
}

int isCrossing (sZone * z1, sZone * z2)
{
    // X corner of z1 belongs to z2
    if (z1->x >= z2->x && z1->x <= (z2->x + z2->width) &&
        z1->y >= z2->y && z1->y <= (z2->y + z2->height))
        return (1);

    // X+Width corner of z1 belongs to z2
    if ((z1->x + z1->width) >= z2->x && (z1->x + z1->width) <= (z2->x + z2->width) &&
        z1->y >= z2->y && z1->y <= (z2->y + z2->height))
        return (1);

    // X corner of z2 belongs to z1
    if (z2->x >= z1->x && z2->x <= (z1->x + z1->width) &&
        z2->y >= z1->y && z2->y <= (z1->y + z1->height))
        return (1);

    // X+Width corner of z2 belongs to z1
    if ((z2->x + z2->width) >= z1->x && (z2->x + z2->width) <= (z1->x + z1->width) &&
        z2->y >= z1->y && z2->y <= (z1->y + z1->height))
        return (1);
    
    // Zones are not intersecting
    return (0);
}

int initQueues (char * playlist)
{
    char buffer [CMDLINE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    char * currentline;
    char * newline;
    char zone [ZONE_MAXLENGTH];

    memset (zone, '\0', ZONE_MAXLENGTH);

    currentline = playlist;
    newline = strchr (currentline, '\n');
    while (currentline && strlen (currentline) && newline)
    {
        memset (line, '\0', LINE_MAXLENGTH);
        strncpy (line, currentline, newline - currentline);
        // Remove \n and \r
        while (strlen (line) >= 1 && (line [strlen (line) - 1] == '\n' ||
                                    line [strlen (line) - 1] == '\r'))
            line[strlen (line) - 1] = '\0';

        // Blank line ?
        if (strlen (line) <= 0 || line[0] == '#')
        {
            // Next line
            currentline = newline+1;
            newline = strchr (currentline, '\n');
            continue;
        }

        // Section line ?
        if (line[0] == '[' && line[strlen(line)-1] == ']')
        {
            // Launch the last zone playlist !
            if (strlen (zone))
            {
                memset (buffer, '\0', sizeof (buffer));
                snprintf (buffer, sizeof (buffer)-1,
                          "%s -c %s -z '%s' -p",
                            config.path_izone,
                            config.filename,
                            zone);
                //iDebug ("Executing : '%s'", buffer);
                system (buffer);
            }

            // Get this new playlist name
            memset (zone, '\0', ZONE_MAXLENGTH);
            strncpy (zone, line+1, 
                     (strlen(line) < ZONE_MAXLENGTH-1) ? 
                        strlen(line)-2 : ZONE_MAXLENGTH-1);
            // Next line
            currentline = newline+1;
            newline = strchr (currentline, '\n');
            continue;
        }
        
        // Enqueue the media
        if (strlen (zone))
        {
            memset (buffer, '\0', sizeof (buffer));
            snprintf (buffer, sizeof (buffer)-1,
                      "%s -c %s -z '%s' -q '%s'",
                        config.path_izone,
                        config.filename,
                        zone,
                        line);
            //iDebug ("Executing : '%s'", buffer);
            system (buffer);
        }

        // Next line
        currentline = newline+1;
        newline = strchr (currentline, '\n');
    }

    return (1);
}

int playQueues (int once)
{
    int i;
    char zones [ZONE_NBMAX][ZONE_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];

    if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
    {
        iError ("Can't get the zones to start them");
        return (0);
    }

    for (i = 0 ; i < ZONE_NBMAX && strlen (zones [i]) ; i++)
    {
        // Start the playlist of the zone
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        if (once)
        {
            snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -p --once",
                        config.path_izone,
                        zones[i]);
        }
        else
        {
            snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -p",
                        config.path_izone,
                        zones[i]);
        }
        system (buffer);
    }

    // Send ALIVE message
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, "%s -c %s 2>/dev/null 1>&2", 
                    config.path_isynchro,
                    config.filename);
    system (buffer);

    // Go to the main desktop after 1 second
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                 "sleep 1 ; /usr/bin/xsetroot -solid black ; %s -z '%s' -d%d",
                 config.path_iwm,
                 config.filename,
                 config.desktop_main);
    system (buffer);

    return (1);
}

int stopQueues ()
{
    int i;
    char zones [ZONE_NBMAX][ZONE_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];
    int width, height;

    iDebug ("[%d] Stopping queues...", getpid ());

    if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
    {
        iError ("Can't get the zones to stop them");
        return (0);
    }

    getResolution (&config, &width, &height);

    // Go to a blank desktop
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                 "%s -z '%s' -d%d",
                 config.path_iwm,
                 config.filename,
                 config.desktop_blank);
    system (buffer);

    // Logo background
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1,
                "if [ -e \"%s/%dx%d_%s\" ]; then "              \
                    "/usr/bin/feh --bg-center %s/%dx%d_%s ; "   \
                "else "                                         \
                    "/usr/bin/feh --bg-center %s/%dx%d_%s ; "   \
                "fi",
                config.path_library, width, height, CUSTOM_BASE,
                config.path_library, width, height, CUSTOM_BASE,
                config.path_library, width, height, WALLPAPER_BASE);
    system (buffer);

    for (i = 0 ; i < ZONE_NBMAX && strlen (zones [i]) ; i++)
    {
        // Kill the zone
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -k",
                    config.path_izone,
                    zones[i]);
        system (buffer);
    }

    // Close all windows

    // Wait all zones to be removed from memory
    if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
    {
        iError ("Can't get the zones to stop them");
        return (0);
    }

    for (i = 0 ; i < ZONE_NBMAX ; i++)
    {
        if (strlen (zones [i]))
        {
            sleep (1);
            if (!shm_getZones (zones[0], ZONE_NBMAX, ZONE_MAXLENGTH))
            {
                iError ("Can't get the zones to stop them");
                return (0);
            }
            i = -1;
        }
    }
    common_flushProgram (&config);

    // Free plugins' allocations
    freePlugins ();

    return (1);
}

int freePlugins ()
{
    char buffer [FILENAME_MAXLENGTH];
    int count;
    struct direct ** files;
    void * handle;
    int (*pClean) ();

    // Load all shared library from the plugin directory
    if ((count = scandir (config.path_plugins,
                          &files, pluginSelect, alphasort)) == -1)
    {
        iError ("Can't get plugin from '%s'", config.path_plugins);
        return (0);
    }

    while (count--)
    {
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer) - 1, "%s/%s",
                    config.path_plugins, files[count]->d_name);

        // Load of the library
        handle = dlopen (buffer, RTLD_LAZY);
        if (handle)
        {
            pClean = dlsym (handle, "clean");
            if (pClean && !pClean ())
            {
                iError ("[%s] Cleaning failed.",
                            files[count]->d_name);
            }
        }
    }
    return (1);
}
