// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iZoneMgr/iZoneMgr.h
 * Description: Manage layout and zones
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.28: Original revision
 *****************************************************************************/

#ifndef VERSION
#define IZONEMGR_VERSION     "Version Unknown"
#else
#define IZONEMGR_VERSION     VERSION
#endif

#define IZONEMGR_APPNAME            "iZoneMgr (iPlayer Zone Manager)"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

void launchPlaylist (char * filename, int once);
void launchLoop (char * programs);
int initLayout (char * layout);
int initQueues (char * playlist);
int playQueues (int once);
int stopQueues ();
int freePlugins ();
int allZonesHaveLooped ();
int getTheLowestLayer (sZones * pZones, int * realLayers, sZone * zone);
int isCrossing (sZone * z1, sZone * z2);
