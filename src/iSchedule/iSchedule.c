// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iSchedule/iSchedule.c
 * Description: Manage action to do according to schedule
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.27: Original revision
 *  - 2010.04.12: Added iCal support
 *  - 2011.06.08: Added thread-safe write access to shared memory
 *  - 2012.11.11: Added "one-instance" check for the main iSchedule process
 *****************************************************************************/

#define _GNU_SOURCE
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "iSchedule.h"
#include "iCommon.h"
#include "log.h"
#include "license.h"
#include "Sema.h"

#define HELP ""                                                             \
"Usage:\n"                                                                  \
"iSchedule [options] <command>\n"                                           \
"  <options>:\n"                                                            \
"    -h, --help             Show this helpful message\n"                    \
"    -v, --version          Show the version of this tool\n"                \
"    -c, --conf '<file>'    Set the configuration file of the iPlayer\n"    \
"\n"                                                                        \
"  <command>:\n"                                                            \
"    --start                Start the scheduler daemon\n"                   \
"    --import '<file>'      Import iCal file\n"                             \
"    --flush '<file>'       Flush events associated to iCal file\n"         \
"    --show                 Display current scheduler\n"                    \
"\n" 

#define MAX_PROPERTY_VALUE_LEN 4096

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"conf",        required_argument,  0, 'c'},
    {"import",      required_argument,  0, 'i'},
    {"flush",       required_argument,  0, 'f'},
    {"start",       no_argument,        0, 's'},
    {"show",        no_argument,        0, 'a'},
    {0, 0, 0, 0}
};

sConfig     config;
sParams     params;
//sEvent *    calendar    = NULL;
sCalendar * calendar = NULL;
int         gStop;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char fileConfig [FILENAME_MAXLENGTH];
    char fileCal    [FILENAME_MAXLENGTH];
    char action;
    int returnValue, tries;

    memset (fileConfig, '\0', sizeof (fileConfig));
    memset (fileCal, '\0', sizeof (fileCal));
    gStop = 0;
    action = '\0';

    // Parse parameters
    while ((c = getopt_long (argc, argv, "hvc:i:s",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", ISCHEDULE_APPNAME, ISCHEDULE_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (fileConfig, optarg, sizeof (fileConfig)-1);
                break;
            case 'i':
                strncpy (fileCal, optarg,sizeof (fileCal) - 1);
                action = 'i';
                break;
            case 'f':
                strncpy (fileCal, optarg,sizeof (fileCal) - 1);
                action = 'f';
                break;
            case 's':
                action = 's';
                break;
            case 'a':
                action = 'a';
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (fileConfig) <= 0)
    {
        // Default configuration file
        strncpy (fileConfig, DEFAULT_CONFIGURATIONFILE, sizeof (fileConfig)-1);
    }

    // Load the configuration file (override if already loaded)
    if (common_getConfig (fileConfig, &config) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }

    if (common_getParams (config.path_params, &config, &params) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load parameters. Exiting.");
        return (EXIT_FAILURE);
    }

    switch (action)
    {
        case 's':
            // Check there is no other instance of "iSchedule --start"
            tries = 0;
            do
            {
                returnValue = getRunningStatus ();
                tries++;
            }
            while (returnValue == SCHEDULE_ERROR_RUNNING &&
                   tries <= SCHEDULE_LOCK_TRIES);

            if (returnValue != SCHEDULE_NOT_RUNNING)
            {
                iLog ("Scheduler is ALREADY running.");
                return (EXIT_FAILURE);
            }

            // Initialize then start daemon
            iLog ("Scheduler is starting...");
            // Hook signals
            iScheduleSignalsHandler ();

            // Initialize calendar in shared memory
            if (!initSHM ())
                return (EXIT_FAILURE);

            // Create the trivial calendar
            createBaseCalendar ();

            // Import the default calendar
            snprintf (fileCal, sizeof (fileCal), "%s/%s",
                                                    config.path_playlists,
                                                    config.playlist_schedule);
            importiCal (fileCal);

            //debugCalendar ();
            
            // Launch the infinite loop
            doSchedule ();

            // Remove PID file
            iLog ("Removing the PID file...");
            if (unlink (IPLAYER_PID_FILE) != 0)
                iError ("Cannot remove %s file.", IPLAYER_PID_FILE);

            // Unlock instance semaphore
            if (!sema_unlock (SCHEDULE_INST_SEMAPHORE))
                iError ("Cannot unlock our iSchedule instance's semaphore\n");
            break;
        case 'i':
            if (!getSHM ())
                return (EXIT_FAILURE);
            iLog ("Importing file '%s'", fileCal);
            importiCal (fileCal);
            break;
        case 'f':
            if (!getSHM ())
                return (EXIT_FAILURE);
            iLog ("Flushing events '%s'", fileCal);
            flushiCal (fileCal);
            break;
        case 'a':
            if (!getSHM ())
                return (EXIT_FAILURE);
            debugCalendar ();
            break;
        default:
            fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
            return EXIT_FAILURE;
    }
    detachCalendar ();

    return (0);
}

sCalendar * initSHM ()
{
    int shmid;
    int shmsize = SHM_SCHEDULERSIZE;
 
    // Create the shared memory
    if ((shmid = shmget (MEMKEY_SCHEDULER, shmsize, IPC_CREAT | 0666)) < 0)
    {
        fprintf (stderr, "Cannot create Shared memory.\n");
        return (NULL);
    }

    if ((char *)(calendar = (sCalendar *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError ("Can't get the Scheduler memory. SHM Error.");
        return (NULL);
    }
    memset (calendar, '\0', SHM_SCHEDULERSIZE);
    calendar->entry = -1;

    return (calendar);
}

sCalendar * getSHM ()
{
    int shmid;
    int shmsize = SHM_SCHEDULERSIZE;
 
    // Create the shared memory
    if ((shmid = shmget (MEMKEY_SCHEDULER, shmsize, 0666)) < 0)
    {
        fprintf (stderr, "Cannot get Shared memory.\n");
        return (NULL);
    }

    if ((char *)(calendar = (sCalendar *)shmat (shmid, NULL, 0)) == (char *)-1)
    {
        iError("Can't get the Scheduler memory. SHM Error.");
        return (NULL);
    }

    return (calendar);
}

void createBaseCalendar ()
{
    char buffer [EVENT_MAXLENGTH];
    char hash [HASH_MAXLENGTH+1];
    sEvent event;

    memset (&event, '\0', sizeof (sEvent));

    // Get the Hash key
    memset (hash, '\0', sizeof (hash));
    if (!getHash (&config, hash, sizeof (hash)))
        return;

    // Event to launch periodic checks of license
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, "%s -c %s --license",
                config.path_isynchro, config.filename);
    event.date          = time (NULL) + 720;
    event.periodicity   = 7200;
    strncpy (event.zone, SCHEDULE_GLOBAL, ZONE_MAXLENGTH);     // No zone concerned
    strncpy (event.event, buffer, EVENT_MAXLENGTH);
    strncpy (event.ical, "base", MD5_MAXLENGTH);
    addEvent (&event);

    // Event to launch the iSynchro ALIVE
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, "%s -c %s", config.path_isynchro,
                                                       config.filename);
    event.date          = time (NULL) + 10;
    event.periodicity   = config.time_synchro;
    strncpy (event.zone, SCHEDULE_GLOBAL, ZONE_MAXLENGTH);     // No zone concerned
    strncpy (event.event, buffer, EVENT_MAXLENGTH);
    strncpy (event.ical, "base", MD5_MAXLENGTH);
    addEvent (&event);

    // Event to launch the iZoneMgr, with last recorded programmation
    /*
    memset (buffer, '\0', sizeof (buffer));
    snprintf (buffer, sizeof (buffer) - 1, "PROG=`iZoneMgr -l | grep \"Program: \" | cut -d\"'\" -f2` ; if [ -z \"$PROG\" ]; then %s -c %s -p '%s' ; else echo \"\"; fi",
                config.path_izonemgr,
                config.filename,
                params.box_program);
    event.date          = time (NULL) + 15;
    event.periodicity   = 0;
    strncpy (event.zone, SCHEDULE_GLOBAL, ZONE_MAXLENGTH);     // No zone concerned
    strncpy (event.event, buffer, EVENT_MAXLENGTH);
    strncpy (event.ical, "base", MD5_MAXLENGTH);
    addEvent (&event);
    */

    iDebug ("Basic Calendar created.");
}

void doSchedule ()
{
    sEvent * event  = NULL;
    time_t now, nextTime;
    double diff;
    int event_isdst, nextTime_isdst; // flag for distinguish between a winter date and a summer date
                                     // isdst means "IS Daylight Saving Time"
    struct tm* time_tmp = NULL;

    iDebug ("Scheduler started.");

    while (!gStop && calendar && calendar->entry >= 0)
    {
        now = time (NULL);
        while (!gStop && calendar->entry >= 0 && calendar->events [calendar->entry].date <= now)
        {
            event = &calendar->events [calendar->entry];
            
            // Executing the event only if its not finished and it is not a
            // time=0 (as soon as possible) event
            if (event->date == 0 || event->end >= now)
            {
                if (event->date == 0 || event->date != event->exdate)
                    doEvent (event);
                else
                {
                    iLog ("Planned exception on the following execution:");
                    iLog ("%s", event->event);
                }
            }

            if (event->periodicity > 0)
            {
                // Move the event to the next time "after" now, according
                // to its periodicity
                if (event->date == 0)
                    nextTime = now + event->periodicity;
                else
                    nextTime = event->date + event->periodicity;

                while (nextTime < now &&
                        nextTime + (event->end - event->date) < now)
                    nextTime += event->periodicity;

                // verify if event date needs to be modify for winter/summer time change
                time_tmp = localtime(&nextTime);
                nextTime_isdst = time_tmp->tm_isdst;
                time_tmp = localtime(&(event->date));
                event_isdst = time_tmp->tm_isdst;

                // from winter to summer
                if (nextTime_isdst == 1 && event_isdst == 0)
                {
                    iLog ("Event '%s' is now planned with Daylight Saving Time", event->event);
                    nextTime -=3600;
                }
                // from summer to winter
                if (nextTime_isdst == 0 && event_isdst == 1)
                {
                    iLog ("Event '%s' is now planned without Daylight Saving Time", event->event);
                    nextTime +=3600;
                }

                moveEvent (event, nextTime);
            }
            else
            {
                delEvent (event);
            }
        }

        now = time (NULL);
        if (!gStop && calendar->entry >= 0 &&
             calendar->events [calendar->entry].date > now)
        {
            diff = difftime (calendar->events [calendar->entry].date, now);
            if (diff > 5)
                diff = 5;
            sleep (diff);
        }
    }    
    if (gStop)
        iDebug ("Requested Scheduler to stop");
    if (!calendar)
        iDebug ("No Calendar in memory");
    if (calendar->entry)
        iDebug ("Calendar is empty");

    iLog ("Scheduler stopped.");
}

int doEvent (sEvent * event)
{
    char cmd [EVENT_MAXLENGTH];
    memset (cmd, '\0', sizeof (cmd));

    if (!event)
        return (-1);

    if (strncmp (event->zone, SCHEDULE_GLOBAL, EVENT_MAXLENGTH) == 0)
    {
        // Global command
        // - Execute command in background
        // - Redirect stderr and stdout in syslog
        snprintf (cmd, sizeof (cmd) - 1, "/bin/bash -c '%s' 2>&1 | logger -p local4.info -t iSchedule &", event->event);
        system (cmd);
    }
    else
    {
        // Command for the iZoneManager
        snprintf (cmd, sizeof (cmd) - 1, 
                    "%s -c %s -z %s -e %s 2>&1 &",
                    config.path_izonemgr,
                    config.filename,
                    event->zone,
                    event->event);
        system (cmd);
    }
    iDebug ("Scheduled: %s", cmd);

    return (1);
}

int getFreePosInMem ()
{
    int i;

    if (!calendar)
        return (-1);

    for (i = 0 ; i < MAX_SCHEDULEREVENTS ; i++)
    {
        if (calendar->events [i].zone [0] == '\0')
            return (i);
    }
    return (-1);
}

int addEvent (sEvent * event)
{
    int prev = -1;
    int next = -1;

    if (!event)
    {
        return (-1);
    }

    if (!sema_createLock (SCHEDULE_MEM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to have excluse access to iSchedule SHM\n");
        return (-3);
    }

    // Allocate memory
    int pos = getFreePosInMem ();
    if (pos < 0)
    {
        if (!sema_unlock (SCHEDULE_MEM_SEMAPHORE))
            iError ("Cannot unlock our iSchedule instance's semaphore\n");

        iError ("Can't allocate memory for a new event.");
        return (-2);
    }

    sEvent * newEvent = &calendar->events [pos];

    // Copy the event
    memset (newEvent, '\0', sizeof (sEvent));
    newEvent->date          = event->date;
    newEvent->end           = event->end;
    newEvent->exdate        = event->exdate;
    newEvent->periodicity   = event->periodicity;
    newEvent->pos           = pos;
    strncpy (newEvent->zone,    event->zone,    ZONE_MAXLENGTH-1);
    strncpy (newEvent->event,   event->event,   EVENT_MAXLENGTH-1);
    strncpy (newEvent->ical,    event->ical,    MD5_MAXLENGTH);

    if (newEvent->end <= 0)
        newEvent->end = newEvent->date;

    // Find the correct place to add it
    if (calendar->entry < 0)
    {
        // First element of the list
        newEvent->next  = -1;
        newEvent->prev  = -1;
        calendar->entry  = newEvent->pos;
    }
    else
    {
        next = calendar->entry;
        prev = -1;

        while (next >= 0 && calendar->events [next].date < newEvent->date)
        {
            // Go to the next event
            prev = next;
            next = calendar->events [next].next;
        }

        if (prev >= 0)
            calendar->events [prev].next = newEvent->pos;
        else
            calendar->entry = newEvent->pos;

        if (next >= 0)
            calendar->events [next].prev = newEvent->pos;

        newEvent->next = next;
        newEvent->prev = prev;
    }

    if (!sema_unlock (SCHEDULE_MEM_SEMAPHORE))
        iError ("Cannot unlock our iSchedule instance's semaphore\n");

    return (1);
}

int delEvent (sEvent * event)
{
    int prev;
    int next;

    if (!event)
        return (-1);

    if (!sema_createLock (SCHEDULE_MEM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to have excluse access to iSchedule SHM\n");
        return (-2);
    }

    // Get the prev and next event
    prev = event->prev;
    next = event->next;

    // Link them together
    if (prev >= 0)
        calendar->events [prev].next = next;
    if (next >= 0)
        calendar->events [next].prev = prev;

    // Free memory
    memset (event, '\0', sizeof (sEvent));

    if (prev < 0)
    {
        // The deleted event was the first, beginning of the list is now
        // the next event
        calendar->entry = next;
    }

    if (!sema_unlock (SCHEDULE_MEM_SEMAPHORE))
        iError ("Cannot unlock our iSchedule instance's semaphore\n");

    return (1);
}

void clearCalendar ()
{
    iDebug ("Clearing Calendar.");

    if (!sema_createLock (SCHEDULE_MEM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to have excluse access to iSchedule SHM\n");
        return;
    }

    // Close the Shared memory                                                                         
    if (calendar)
    {
        memset (calendar, '\0', SHM_SCHEDULERSIZE);
        calendar->entry = -1;
        shmdt (calendar);
    }

    if (!sema_unlock (SCHEDULE_MEM_SEMAPHORE))
        iError ("Cannot unlock our iSchedule instance's semaphore\n");
}

void detachCalendar ()
{
    // Close the Shared memory                                                                         
    if (calendar)
        shmdt (calendar);
}

int moveEvent (sEvent * event, time_t newDate)
{
    int prev;
    int next;

    if (!event)
        return (-1);

    if (!sema_createLock (SCHEDULE_MEM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to have excluse access to iSchedule SHM\n");
        return (-2);
    }

    event->end = newDate + (event->end - event->date);
    event->date = newDate;

    // Get the prev and next event
    prev = event->prev;
    next = event->next;

    // Link them together
    if (prev >= 0)
        calendar->events [prev].next = next;
    else
        calendar->entry = next;

    if (next >= 0)
        calendar->events [next].prev = prev;

    // Find the correct place
    next = calendar->entry;
    prev = -1;

    while (next == event->pos || (next >= 0 && calendar->events [next].date < event->date))
    {
        // Go to the next event
        prev = next;
        next = calendar->events [next].next;
    }

    if (prev >= 0)
        calendar->events [prev].next = event->pos;
    if (next >= 0)
        calendar->events [next].prev = event->pos;

    event->next = next;
    event->prev = prev;

    if (calendar->entry < 0 || calendar->entry == event->next)
        calendar->entry = event->pos;

    if (!sema_unlock (SCHEDULE_MEM_SEMAPHORE))
        iError ("Cannot unlock our iSchedule instance's semaphore\n");

    return (1);
}

void debugCalendar ()
{
    sEvent * event;
    int pos;
    char buffer1 [32];
    char buffer2 [32];
    char buffer3 [32];

    iDebug ("Showing the calendar:");

    if (!calendar)
    {
        iDebug ("No calendar in memory.");
        fprintf (stdout, "No calendar in memory.\n");
        return;
    }
    if (calendar->entry < 0)
    {
        iDebug ("Empty calendar.");
        fprintf (stdout, "Empty calendar.\n");
        return;
    }

    pos = calendar->entry;
    while (pos >= 0)
    {
        event = &calendar->events [pos];
        iDebug ("%s / %s (^%s) (%.4lus) [%s][%s] '%s'",
                                       dateToChar (&event->date, 
                                                   buffer1,
                                                   sizeof (buffer1)),
                                       (event->date == event->end) ? "!" :
                                           dateToChar (&event->end, 
                                                       buffer2,
                                                       sizeof (buffer2)),
                                       (event->exdate == 0) ? "!" :
                                           dateToChar (&event->exdate, 
                                                       buffer3,
                                                       sizeof (buffer3)),
                                       event->periodicity, 
                                       event->ical,
                                       event->zone, 
                                       event->event);
        fprintf (stdout, "%s [%s][%s] '%s'\n",
                                       dateToChar (&event->date, 
                                                   buffer1,
                                                   sizeof (buffer1)),
                                       event->ical,
                                       event->zone,
                                       event->event);
        pos = calendar->events [pos].next;
    }    
}

char * dateToChar (time_t * date, char * buffer, int size)
{
    struct tm * ltime;

    if (!buffer)
        return (NULL);

    ltime = localtime (date);
    snprintf (buffer, size, "%.4d.%.2d.%.2d %.2d:%.2d:%.2d",
                ltime->tm_year+1900,
                ltime->tm_mon+1,
                ltime->tm_mday,
                ltime->tm_hour,
                ltime->tm_min,
                ltime->tm_sec);

    return (buffer);
}

void iScheduleSignalsHandler ()
{
    signal (SIGTERM, signalHandler);
    signal (SIGINT, signalHandler);
    signal (SIGHUP, signalHandler);
}

void signalHandler (int sig)                                                  
{   
    char cmd [128];
    memset (cmd, '\0', sizeof (cmd));

    switch (sig)                                                              
    {   
        case SIGTERM:                                                         
        case SIGINT:                                                         
            // Killing ZoneManager
            iDebug ("Killing iZoneMgr...", sig);
            snprintf (cmd, sizeof (cmd) - 1, 
                        "%s -c %s -s", config.path_izonemgr, config.filename);
            system (cmd);
            
            // Stop the scheduler loop
            gStop = 1;
            break;

        case SIGHUP:
            // Someone send us this signal because something changed          
            // (play status, dimensions, ...)
            break;
        default:
            iError ("Unknown signal '%d'", sig);
    }
}

char * getStripLineFromFile (char * line, int size, FILE * fd)
{
    memset (line, '\0', size);
    if (fgets (line, size, fd))
    {
        // Remove the EndOfLine characters
        while (line [strlen (line) - 1] == '\r' ||
               line [strlen (line) - 1] == '\n')
            line [strlen (line) - 1] = '\0';

        return (line);
    }
    return (NULL);
}

char * getStripLineFromString (char * line, int size, char * buffer)
{
    int i = 0;

    if (strlen (buffer) < 1)
        return (NULL);
    
    // Copy the string
    memset (line, '\0', size);
    while (i < strlen (buffer) && i < size
            && buffer [i] != '\r'
            && buffer [i] != '\n')
    {
        line [i] = buffer [i];
        i++;
    }

    iDebug ("-> %s", line);

    if (i >= strlen (buffer))
        return (buffer + i);

    // Set the position after CRLF
    while (buffer [i] == '\r' ||
            buffer [i] == '\n')
        i++;

    return (buffer + i);
}

int importiCalString (char * buffer)
{
    char line [LINE_MAXLENGTH];
    char md5 [MD5_MAXLENGTH+1];
    int lineId;
    int iCalFormat = 0;
    int nbEvents = 0, addedEvents = 0;
    sEvent newEvent;
    char * currentBufferPosition;
    int summary;

    memset (md5, '\0', sizeof (md5));
    if (!common_getStringMD5 (buffer, strlen (buffer), md5, sizeof (md5)))
    {
        iError ("Unable to get MD5 of file's content.");
        return (0);
    }

    lineId = 0;
    summary = 0;
    currentBufferPosition = buffer;
    while ((currentBufferPosition = getStripLineFromString (line, sizeof (line), currentBufferPosition)))
    {
        lineId++;

        if (strlen (line) <= 1)
            continue;

        // Manage iCal encapsulation
        if (strncmp (line, ICAL_VCALENDAR_START, strlen (ICAL_VCALENDAR_START)) == 0)
        {
            iCalFormat = 1;
            continue;
        }

        if (strncmp (line, ICAL_VCALENDAR_END, strlen (ICAL_VCALENDAR_END)) == 0)
        {
            iCalFormat = 0;
            continue;
        }

        if (!iCalFormat)
        {
            iError ("File is not in iCal format. (Error line %d)", lineId);
            break;
        }

        if (line [0] == ' ' || line [0] == '\t')
        {
            // Multi-line !
            if (summary)
            {
                // Last line was summary
                strncat (newEvent.event, line+1, EVENT_MAXLENGTH);
            }
            continue;
        }

        // Process body lines
        //   + Command
        if (strncmp (line, ICAL_SUMMARY, strlen (ICAL_SUMMARY)) == 0)
        {
            summary = 1;    // If multiline, command is "summary"
            strncpy (newEvent.event, line+strlen (ICAL_SUMMARY), EVENT_MAXLENGTH);
            continue;
        }
        else
            summary = 0;

        // - Events headers
        if (strncmp (line, ICAL_VEVENT_START, strlen (ICAL_VEVENT_START)) == 0)
        {
            memset (&newEvent, '\0', sizeof (sEvent));
            strncpy (newEvent.ical, md5, MD5_MAXLENGTH);
            nbEvents++;
            continue;
        }
        if (strncmp (line, ICAL_VEVENT_END, strlen (ICAL_VEVENT_END)) == 0)
        {
            if (strlen (newEvent.event) <= 0 ||
                newEvent.date == -1 ||
                strlen (newEvent.ical) <= 0)
            {
                iError ("Event defined line %d has no start or command associated.", lineId);
                continue;
            }
            if (strlen (newEvent.zone) <= 0)
                strncpy (newEvent.zone, SCHEDULE_GLOBAL, strlen (SCHEDULE_GLOBAL));
            if (newEvent.end == -1)
                newEvent.end = newEvent.date;
            addEvent (&newEvent);
            addedEvents++;
            continue;
        }
        // - Events properties
        //   + Start date
        if (strncmp (line, ICAL_DTSTART, strlen (ICAL_DTSTART)) == 0)
        {
            newEvent.date = getSystemTime (line + strlen (ICAL_DTSTART));
            if (newEvent.date == -1)
            {
                iError ("Cannot understand date: '%s'", line + strlen (ICAL_DTSTART));
                newEvent.date = time (NULL);
                continue;                
            }
            continue;
        }
        else if (strncmp (line, ICAL_DTSTARTTZ, strlen (ICAL_DTSTARTTZ)) == 0)
        {
            newEvent.date = getTZSystemTime (line + strlen (ICAL_DTSTARTTZ));
            if (newEvent.date == -1)
            {
                iError ("Cannot understand TZ date: '%s'", line + strlen (ICAL_DTSTARTTZ));
                newEvent.date = time (NULL);
                continue;                
            }
            continue;
        }
        else if (strncmp (line, ICAL_DTSTARTDAY, strlen (ICAL_DTSTARTDAY)) == 0)
        {
            newEvent.date = getSystemTime (line + strlen (ICAL_DTSTARTDAY));
            if (newEvent.date == -1)
            {
                iError ("Cannot understand date: '%s'", line + strlen (ICAL_DTSTARTDAY));
                newEvent.date = time (NULL);
                continue;                
            }
            continue;
        }
        //   + End date
        if (strncmp (line, ICAL_DTEND, strlen (ICAL_DTEND)) == 0)
        {
            newEvent.end = getSystemTime (line + strlen (ICAL_DTEND));
            continue;
        }
        else if (strncmp (line, ICAL_DTENDTZ, strlen (ICAL_DTENDTZ)) == 0)
        {
            newEvent.end = getTZSystemTime (line + strlen (ICAL_DTENDTZ));
            continue;
        }
        else if (strncmp (line, ICAL_DTENDDAY, strlen (ICAL_DTENDDAY)) == 0)
        {
            newEvent.end = getSystemTime (line + strlen (ICAL_DTENDDAY));
            continue;
        }
        //   + Exception date
        if (strncmp (line, ICAL_EXDATE, strlen (ICAL_EXDATE)) == 0)
        {
            newEvent.exdate = getSystemTime (line + strlen (ICAL_EXDATE));
            continue;
        }
        else if (strncmp (line, ICAL_EXDATETZ, strlen (ICAL_EXDATETZ)) == 0)
        {
            newEvent.exdate = getTZSystemTime (line + strlen (ICAL_EXDATETZ));
            continue;
        }
        else if (strncmp (line, ICAL_EXDATEDAY, strlen (ICAL_EXDATEDAY)) == 0)
        {
            newEvent.exdate = getSystemTime (line + strlen (ICAL_EXDATEDAY));
            continue;
        }
        //   + Affected zone
        if (strncmp (line, ICAL_LOCATION, strlen (ICAL_LOCATION)) == 0)
        {
            strncpy (newEvent.zone, line+strlen (ICAL_LOCATION), ZONE_MAXLENGTH);
            continue;
        }
        //   + Periodicity
        if (strncmp (line, ICAL_RRULE, strlen (ICAL_RRULE)) == 0)
        {
            newEvent.periodicity = getPeriodicity (line + strlen (ICAL_RRULE));
            continue;
        }
        if (strncmp (line, ICAL_UID, strlen (ICAL_UID)) == 0)
        {
            // Unused on iPlayer (but on iComposer)
            continue;
        }
        iError ("Don't know what it is : %s", line);
    }

    iLog ("Imported: %d/%d event%s.", addedEvents, nbEvents, (nbEvents > 1) ? "s":"");
    return (1);
}

int importiCalFile (char * filename)
{
    FILE * fd = NULL;
    char line [LINE_MAXLENGTH];
    char md5 [MD5_MAXLENGTH+1];
    int lineId;
    int nbEvents = 0, addedEvents = 0;

    memset (md5, '\0', sizeof (md5));
    if (!common_getFileMD5 (filename, md5, sizeof (md5)))
    {
        iError ("Unable to get MD5 of %s", filename);
        return (0);
    }

    fd = fopen (filename, "r");
    if (!fd)
    {
        iError ("Cannot open file '%s'", filename);
        return (0);
    }

    lineId = 0;
    while (getStripLineFromFile (line, sizeof (line), fd))
    {
        lineId++;
        iError ("TODO: %s", line);
    }

    if (fd)
        fclose (fd);

    iLog ("Imported: %d/%d event%s.", addedEvents, nbEvents, (nbEvents > 1) ? "s":"");
    return (1);
}

int importiCal (char * filename)
{
    // Determine if filename is a clear or crypted file
    FILE * fd = NULL;
    char line [LINE_MAXLENGTH];
    char fileContent [FILE_MAXLENGTH];

    fd = fopen (filename, "r");
    if (!fd)
    {
        iError ("Cannot open file '%s'", filename);
        return (0);
    }

    if (!getStripLineFromFile (line, sizeof (line), fd))
    {
        iError ("Unable to get the first line of file '%s'", filename);
        return (0);
    }

    fclose (fd);

    if (strncmp (line, ICAL_VCALENDAR_START, strlen (ICAL_VCALENDAR_START)) == 0)
    {
        return (importiCalFile (filename));
    }
    else
    {
        if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))
            return (importiCalString (fileContent));
        else
            return (0);
    }
}

int flushiCal (char * filename)
{
    FILE * fd = NULL;
    char line [LINE_MAXLENGTH];
    char fileContent [FILE_MAXLENGTH];
    char md5 [MD5_MAXLENGTH+1];

    // MD5 is the ID for events to delete
    memset (md5, '\0', sizeof (md5));

    fd = fopen (filename, "r");
    if (!fd)
    {
        // 'filename' is the hash or another id refering to events !
        strncpy (md5, filename, MD5_MAXLENGTH);
    }
    else
    {
        // Is the file cyphered ?
        if (!getStripLineFromFile (line, sizeof (line), fd))
        {
            iError ("Unable to get the first line of file '%s'", filename);
            return (0);
        }
        fclose (fd);

        if (strncmp (line, ICAL_VCALENDAR_START, strlen (ICAL_VCALENDAR_START)) == 0)
        {
            // iCalendar file, not cyphered
            // MD5 is the hash of the file
            if (!common_getFileMD5 (filename, md5, sizeof (md5)))
                return (0);
        }
        else
        {
            // Cyphered file, so get MD5 of the decrypted content
            if (getFileContent (&config, filename, fileContent, sizeof (fileContent)))
            {
                if (!common_getStringMD5 (fileContent, strlen (fileContent), md5, sizeof (md5)))
                    return (0);
            }
            else
                return (0);
        }
    }
    
    return (flushiCalFromMD5 (md5));
}

int flushiCalFromMD5 (char * md5)
{
    sEvent * event;
    int pos;
    int removedEvents = 0;

    if (!calendar)
    {
        iDebug ("No calendar in memory.");
        return (0);
    }

    if (calendar->entry < 0)
    {
        iDebug ("Empty calendar.");
        return (0);
    }

    iDebug ("Flushing events associated to %s", md5);

    pos = calendar->entry;
    while (pos >= 0)
    {
        event = &calendar->events [pos];

        // Save next event
        pos = calendar->events [pos].next;

        // Remove from memory events having the same MD5 as source
        if (strncmp (event->ical, md5, MD5_MAXLENGTH) == 0)
        {
            delEvent (event);
            removedEvents++;
        }
    }    
    iLog ("Removed: %d event%s.", removedEvents, (removedEvents > 1) ? "s":"");
    return (1);
}

time_t getSystemTime (char * string)
{
    struct tm t = { 0 };

    if (strptime (string, "%Y%m%dT%H%M%S", &t))
    {
        // Time in the calendar is in UTC Format.
        // Convert into localtime by substracting the timezone
        return (mktime (&t) - timezone);
    }
    else if (strptime (string, "%Y%m%d", &t))
    {
        // Argument is only giving the day
        t.tm_isdst = -1;
        return (mktime (&t));
    }
    else
        return (-1);
}

time_t getTZSystemTime (char * string)
{
    struct tm t = { 0 };

    if (strncmp (string, "Europe/Paris:", strlen ("Europe/Paris:")) == 0)
    {
        if (strptime (string + strlen ("Europe/Paris:"), "%Y%m%dT%H%M%S", &t))
        {
            t.tm_isdst = daylight;
            return (mktime (&t));
        }
        else
        {
            iError ("Cannot convert '%s'", string + strlen ("Eruope/Paris:"));
            return (-1);
        }
    }
    else
    {
        iError ("Time with unknown timezone: %s", string);
        return (-1);
    }
}

int getPeriodicity (char * string)
{
    char intervalChars [16];
    int intervalInt;
    char * position;
    int periodicity = 0;
    // Support FREQ=SECONDLY
    //              MINUTELY
    //              HOURLY
    //              DAILY
    //              WEEKLY
    // All other recurrences are ignored.

    // Support INTERVAL=xx;
    position = string;
    while (position < string + strlen (string) &&
            strncmp (position, ICAL_FREQ_INTERVAL, strlen (ICAL_FREQ_INTERVAL)) != 0)
        position++;

    if (position < string + strlen (string))
    {
        position = position + strlen (ICAL_FREQ_INTERVAL);
        int i = 0;
        memset (intervalChars, '\0', sizeof (intervalChars));
        while (position < string + strlen (string) && *position != ';')
            intervalChars [i++] = *position++;
        intervalInt = atoi (intervalChars);
        if (!intervalInt)
            intervalInt = 1;
    }
    else
        intervalInt = 1;

    if (strncmp (string, ICAL_FREQ, strlen (ICAL_FREQ)) != 0)
        return (periodicity * intervalInt);

    periodicity = 1;    // 1 second
    position = string + strlen (ICAL_FREQ);
    if (strncmp (position, ICAL_FREQ_SECONDLY, strlen (ICAL_FREQ_SECONDLY)) == 0)
        return (periodicity * intervalInt);

    periodicity *= 60;  // 1 minute
    if (strncmp (position, ICAL_FREQ_MINUTELY, strlen (ICAL_FREQ_MINUTELY)) == 0)
        return (periodicity * intervalInt);

    periodicity *= 60;  // 1 hour
    if (strncmp (position, ICAL_FREQ_HOURLY, strlen (ICAL_FREQ_HOURLY)) == 0)
        return (periodicity * intervalInt);

    periodicity *= 24;  // 1 day
    if (strncmp (position, ICAL_FREQ_DAILY, strlen (ICAL_FREQ_DAILY)) == 0)
        return (periodicity * intervalInt);

    periodicity *= 7;  // 1 week
    if (strncmp (position, ICAL_FREQ_WEEKLY, strlen (ICAL_FREQ_WEEKLY)) == 0)
        return (periodicity * intervalInt);

    return (0);
}

int getRunningStatus ()
{
    FILE* pidFile = NULL;
    int pid = 0;

    // Creating Lock on the 'iSchedule --start' instance
    if (sema_createLock (SCHEDULE_INST_SEMAPHORE))
    {
        // Save our PID
        if ((pidFile = fopen (IPLAYER_PID_FILE, "w")) == NULL)
        {
            iCrit ("Cannot open iSchedule PID file for writing. Exiting...");
            if (!sema_unlock (SCHEDULE_INST_SEMAPHORE))
                iError ("Cannot unlock our iSchedule instance's semaphore");
            exit (EXIT_FAILURE);
        }
        if (fprintf (pidFile, "%d", getpid ()) <= 0)
        {
            fclose (pidFile);
            iCrit ("Cannot write into iSchedule PID file. Exiting...");
            if (!sema_unlock (SCHEDULE_INST_SEMAPHORE))
                iError ("Cannot unlock our iSchedule instance's semaphore");
            exit (EXIT_FAILURE);
        }
        fclose (pidFile);
        return (SCHEDULE_NOT_RUNNING);
    }

    iError ("Cannot lock semaphore to have exclusive iSchedule main instance");

    // Check the PID file exists
    pidFile = fopen (IPLAYER_PID_FILE, "r");
    if (!pidFile)
    {
        // If the file does not exist, unlock the semaphore
        iLog ("No PID file for the existing iSchedule Lock !");
        iLog ("Unlocking the semaphore...");

        if (!sema_unlock (SCHEDULE_INST_SEMAPHORE))
            iError ("Cannot unlock iSchedule instance's semaphore");
        return (SCHEDULE_ERROR_RUNNING);
    }

    // Get the running PID
    if (fscanf (pidFile, "%d", &pid) != 1 || pid <= 0)
    {
        fclose (pidFile);
        // No PID in file ? Same behavior than no PID file...
        iLog ("No data in PID file for the existing iSchedule Lock ! (%d)", pid);
        iLog ("Unlocking the semaphore...");

        if (!sema_unlock (SCHEDULE_INST_SEMAPHORE))
            iError ("Cannot unlock iSchedule instance's semaphore");
        return (SCHEDULE_ERROR_RUNNING);
    }
    fclose (pidFile);

    // If the file exists, check that the given process is running
    if (kill (pid, 0) != 0)
    {
        // If process does not exist, remove the PID file
        iLog ("%s does not have a running PID process (%d).",
                IPLAYER_PID_FILE, pid);
        iLog ("Removing the PID file...");

        if (unlink (IPLAYER_PID_FILE) != 0)
            iError ("Cannot remove %s file.", IPLAYER_PID_FILE);
        return (SCHEDULE_ERROR_RUNNING);
    }
    
    // PID file exists, and process is running. Everything is fine...
    return (SCHEDULE_ALREADY_RUNNING);
}
