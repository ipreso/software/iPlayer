// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iSchedule/iSchedule.h
 * Description: Manage action to do according to schedule
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.01.27: Original revision
 *  - 2010.04.12: Added iCal support
 *  - 2011.06.08: Added thread-safe write access to shared memory
 *  - 2012.11.11: Added "one-instance" check for the main iSchedule process
 *****************************************************************************/

#ifndef VERSION
#define ISCHEDULE_VERSION     "Version Unknown"
#else
#define ISCHEDULE_VERSION     VERSION
#endif

#define ISCHEDULE_APPNAME           "iSchedule (iPlayer Scheduler)"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"

#define MEMKEY_SCHEDULER        15
#define MAX_SCHEDULEREVENTS     1024


#include <stdio.h>
#include <time.h>
#include <sys/shm.h>
#include "config.h"

typedef struct event
{
    time_t          date;                   // Date/Time of the event
    time_t          end;                    // End of the event
    time_t          exdate;                 // Exception date/time
    long            periodicity;            // Periodicity (in s), 0 not
    char            zone [ZONE_MAXLENGTH];  // Zone of the event
    char            event [EVENT_MAXLENGTH];// Description of the event
    char            ical [MD5_MAXLENGTH+1]; // MD5 of the imported file

    int             next;
    int             prev;
    int             pos;

} sEvent;

typedef struct calendar
{
    int     entry;
    sEvent  events [MAX_SCHEDULEREVENTS];
} sCalendar;

#define SHM_SCHEDULERSIZE   (sizeof (sCalendar))

#define ICAL_VCALENDAR_START    "BEGIN:VCALENDAR"
#define ICAL_VCALENDAR_END      "END:VCALENDAR"
#define ICAL_VEVENT_START       "BEGIN:VEVENT"
#define ICAL_VEVENT_END         "END:VEVENT"
#define ICAL_UID                "UID:"
#define ICAL_DTSTART            "DTSTART:"
#define ICAL_DTSTARTTZ          "DTSTART;TZID="
#define ICAL_DTSTARTDAY         "DTSTART;VALUE=DATE:"
#define ICAL_DTEND              "DTEND:"
#define ICAL_DTENDTZ            "DTEND;TZID="
#define ICAL_DTENDDAY           "DTEND;VALUE=DATE:"
#define ICAL_SUMMARY            "SUMMARY:"
#define ICAL_LOCATION           "LOCATION:"
#define ICAL_EXDATE             "EXDATE:"
#define ICAL_EXDATETZ           "EXDATE;TZID="
#define ICAL_EXDATEDAY          "EXDATE;VALUE=DATE:"
#define ICAL_RRULE              "RRULE:"
#define ICAL_FREQ               "FREQ="
#define ICAL_FREQ_SECONDLY      "SECONDLY"
#define ICAL_FREQ_MINUTELY      "MINUTELY"
#define ICAL_FREQ_HOURLY        "HOURLY"
#define ICAL_FREQ_DAILY         "DAILY"
#define ICAL_FREQ_WEEKLY        "WEEKLY"
#define ICAL_FREQ_MONTHLY       "MONTHLY"
#define ICAL_FREQ_INTERVAL      "INTERVAL="

#define SCHEDULE_GLOBAL         "_GLOBAL_"

#define SCHEDULE_MEM_SEMAPHORE  "/iplayer.mem_ischedule"
#define SCHEDULE_INST_SEMAPHORE "/iplayer.inst_ischedule"

#define SCHEDULE_NOT_RUNNING        1
#define SCHEDULE_ALREADY_RUNNING    2
#define SCHEDULE_ERROR_RUNNING      3

#define SCHEDULE_LOCK_TRIES         5

sCalendar * initSHM     ();
sCalendar * getSHM      ();
int getFreePosInMem     ();
void createBaseCalendar ();
void doSchedule         ();
int doEvent             (sEvent * event);

void debugCalendar      ();
void clearCalendar      ();
void detachCalendar     ();

int addEvent (sEvent * event);
int delEvent (sEvent * event);
int moveEvent (sEvent * event, time_t newDate);
char * dateToChar (time_t * date, char * buffer, int size);

void iScheduleSignalsHandler ();
void signalHandler (int sig);

int importiCal (char * filename);
int importiCalFile (char * filename);
int importiCalString (char * buffer);
int flushiCal (char * filename);
int flushiCalFromMD5 (char * md5);
char * getStripLineFromFile (char * line, int size, FILE * fd);
char * getStripLineFromString (char * line, int size, char * buffer);
time_t getSystemTime (char * string);
time_t getTZSystemTime (char * string);
int getPeriodicity (char * string);

int getRunningStatus ();
