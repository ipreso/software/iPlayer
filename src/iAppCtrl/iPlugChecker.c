// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iAppCtrl/iPlugChecker.c
 * Description: Check plugin files functions.
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2010.01.06: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <dlfcn.h>
#include <stdint.h>

#include "iAppCtrl.h"

#undef IAPPCTRL_APPNAME
#define IAPPCTRL_APPNAME            "iPlugChecker (iPlayer Plugin Checker)"


#define HELP ""                                                         \
"Usage:\n"                                                              \
"iPlugChecker [options]\n"                                              \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"\n"                                                                    \
"    -p, --plugin '/path/to/library     Check plugin functions\n"       \
"\n" 

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},

    {"media",       required_argument,  0, 'm'},
    {"plugin",      required_argument,  0, 'p'},

    {0, 0, 0, 0}
};

sPlugin pluginStruct;

void freePlugin (char * binary)
{
    if (pluginStruct.handle && strlen (pluginStruct.plugin))
        dlclose (pluginStruct.handle);
}

int loadPlugin (char * binary)
{
    void * handle;
    int (*pInitPlugin) (sPlugin *, void *);

    // Load of the library
    handle = dlopen (binary, RTLD_LAZY);
    if (handle)
    {
        pInitPlugin = dlsym (handle, "initPlugin");
        if (pInitPlugin)
        {
            // Function exist in the file, loading this plugin
            if (!pInitPlugin (&pluginStruct, handle))
            {
                fprintf (stderr, "Cannot initialize the Plugin %s\n", binary);
                return (0);
            }
        }
        else
        {
            fprintf (stderr, "'initPlugin' function doesn't exist !\n");
            return (0);
        }
    }
    else
    {
        fprintf (stderr, "Cannot open '%s': %s\n", binary, dlerror ());
        return (0);
    }

    return (1);
}

int checkPluginFunctions (char * params)
{
    if (strlen (params) <= 0)
    {
        fprintf (stderr, "No plugin given in parameters\n");
        return (EXIT_FAILURE);
    }

    if (!loadPlugin (params))
        return (EXIT_FAILURE);

    // Check that plugin structure is correctly filled
    fprintf (stdout, "Plugin's information:\n");
    fprintf (stdout, "- name:    %s\n",     pluginStruct.plugin);
    fprintf (stdout, "- handle:  0x%lx\n",  (uintptr_t) pluginStruct.handle);
    fprintf (stdout, "- GetCmd:  0x%lx\n",  (uintptr_t) pluginStruct.pGetCmd);
    fprintf (stdout, "- Play:    0x%lx\n",  (uintptr_t) pluginStruct.pPlay);
    fprintf (stdout, "- Prepare: 0x%lx\n",  (uintptr_t) pluginStruct.pPrepare);
    fprintf (stdout, "- Stop:    0x%lx\n",  (uintptr_t) pluginStruct.pStop);
    fprintf (stdout, "- Clean:   0x%lx\n",  (uintptr_t) pluginStruct.pClean);

    if (strlen (pluginStruct.plugin) <= 1)
    {
        fprintf (stderr, "Incorrect plugin's name\n");
        return (EXIT_FAILURE);
    }

    if (!pluginStruct.handle)
    {
        fprintf (stderr, "Incorrect plugin's initialization (handle)\n");
        return (EXIT_FAILURE);
    }

    if (!pluginStruct.pGetCmd)
    {
        fprintf (stderr, "pGetCmd function is REQUIRED\n");
        return (EXIT_FAILURE);
    }

    if (!pluginStruct.pPlay)
    {
        fprintf (stderr, "pPlay function is REQUIRED\n");
        return (EXIT_FAILURE);
    }

    freePlugin (params);
    return (EXIT_SUCCESS);
}

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char params [512];
    char action = '\0';
    int result;

    memset (params, '\0', sizeof (params));
    memset (&pluginStruct, '\0', sizeof (pluginStruct));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "hvp:m:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", 
                         IAPPCTRL_APPNAME, IAPPCTRL_VERSION);
                return EXIT_SUCCESS;
            case 'p':
                strncpy (params, optarg, sizeof (params)-1);
                action = 'p';
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    switch (action)
    {
        case 'p':
            result = checkPluginFunctions (params);
            break;
        default:
            fprintf (stdout, "No action asked.\n");
            result = EXIT_FAILURE;
    }

    return (result);
}
