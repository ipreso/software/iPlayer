// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iAppCtrl/iAppCtrl.c
 * Description: Manage media by launching correct applications
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Changes:
 *  - 2009.10.08: Updated playlist parsing
 *  - 2009.02.01: Original revision
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <dlfcn.h>

#include "iAppCtrl.h"
#include "iCommon.h"
#include "log.h"
#include "SHM.h"
#include "Sema.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iAppCtrl [options]\n"                                                  \
"  <options>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -v, --version            Show the version of this tool\n"          \
"    -c, --conf '<file>'      Set the configuration file of the iPlayer\n"\
"\n"                                                                    \
"    -t, --type '<media>'     Output the type of the media\n"           \
"    -a, --all                Output the list of supported type of media\n"\
"    -d, --display '<media>'  Load a zone-independant media\n"          \
"\n"                                                                    \
"iAppCtrl <action> -z <zone>\n"                                         \
"    -z, --zone '<name>'      Set the zone targeted by the action\n"   \
"\n"                                                                    \
"  <action>:\n"                                                         \
"    -l, --load '<media>'     Load a media and return its Window ID\n"  \
"    -p, --play <WID>         Play the application with the Window ID\n"\
"    -s, --stop <WID>         Stop the application with the Window ID\n"\
"    -u, --unload <WID>       Unload the media with the Window ID\n"    \
"    -w, --wait <WID>         Wait the media with the Window ID has\n"  \
"                             finished\n"                               \
"\n"                                                                    \

static struct option long_options[] =
{
    {"help",        no_argument,        0, 'h'},
    {"version",     no_argument,        0, 'v'},
    {"conf",        required_argument,  0, 'c'},

    {"type",        required_argument,  0, 't'},
    {"display",     required_argument,  0, 'd'},
    {"all",         no_argument,        0, 'a'},

    {"zone",        required_argument,  0, 'z'},

    {"load",        required_argument,  0, 'l'},
    {"play",        required_argument,  0, 'p'},
    {"stop",        required_argument,  0, 's'},
    {"unload",      required_argument,  0, 'u'},
    {"wait",        required_argument,  0, 'w'},

    {0, 0, 0, 0}
};

sConfig     config;
int         gStop;
sPlugins    gPlugins;
char        gZoneName [ZONE_MAXLENGTH];

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    char params [128];
    char media [CMDLINE_MAXLENGTH+64];
    char buffer [CMDLINE_MAXLENGTH];
    char plugin [PLUGIN_MAXLENGTH];
    char action = '\0';
    int WID = 0;
    gStop = 0;

    memset (params, '\0', sizeof (params));
    memset (gZoneName, '\0', sizeof (gZoneName));
    memset (media, '\0', sizeof (media));
    memset (gPlugins, '\0', sizeof(gPlugins));
    memset (plugin, '\0', sizeof (plugin));
    memset (buffer, '\0', sizeof (buffer));

    iAppCtrlSignalsHandler ();

    // Parse parameters
    while ((c = getopt_long (argc, argv, "c:hvt:d:az:l:p:s:u:w:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", 
                         IAPPCTRL_APPNAME, IAPPCTRL_VERSION);
                return EXIT_SUCCESS;
                break;
            case 'c':
                strncpy (params, optarg, sizeof (params)-1);
                break;

            case 'z':
                strncpy (gZoneName, optarg, sizeof (gZoneName)-1);
                break;

            case 'l':
                strncpy (media, optarg, sizeof (media)-1);
                action = 'l';
                break;
            case 'p':
                WID = atoi (optarg);
                action = 'p';
                break;
            case 's':
                WID = atoi (optarg);
                action = 's';
                break;
            case 'u':
                WID = atoi (optarg);
                action = 'u';
                break;
            case 'w':
                WID = atoi (optarg);
                action = 'w';
                break;

            case 'a':
                action = 'a';
                break;
            case 't':
                strncpy (media, optarg, sizeof (media)-1);
                action = 't';
                break;
            case 'd':
                strncpy (media, optarg, sizeof (media)-1);
                action = 'd';
                break;

            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    if (strlen (params) <= 0)
    {
        // Default configuration file
        strncpy (params, DEFAULT_CONFIGURATIONFILE, sizeof (params)-1);
    }

    if (common_getConfig (params, &config) <= 0)
    {
        fprintf (stdout, "Error, exiting...\n");
        iCrit ("Can't load configuration. Exiting.");
        return (EXIT_FAILURE);
    }

    if (!loadPlugins ())
    {
        iCrit ("Cannot initialize Plugins.");
        return (EXIT_FAILURE);
    }
    switch (action)
    {
        case 'l':
            if (!strlen (gZoneName))
            {
                iCrit ("No Zone specified. Exiting...");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            if ((WID = loadItem (media)) <= 0)
            {
                fprintf (stdout, "0\n");
                iCrit ("[%s] Cannot load the Item.", gZoneName);
                freePlugins ();
                return (EXIT_FAILURE);
            }
            // Prepare to dimensions, layer, ...
            iDebug ("[%d] Window %d appeared. Preparation...", getpid(), WID);
            if (!prepareItem (WID))
            {
                fprintf (stdout, "0\n");
                iCrit ("[%s] Cannot prepare the Item with WID %d",
                        gZoneName, WID);
                unloadItem (WID);
                freePlugins ();
                return (EXIT_FAILURE);
            }
            fprintf (stdout, "%d\n", WID);
            break;
        case 'd':
            // Create a test zone
            strncpy (gZoneName, "__test__", sizeof (gZoneName)-1);
/*
            snprintf (buffer, sizeof (buffer)-1,
                      "%s -c %s -z '%s' -i0 -l10 -o 25x25 -g 50x50 -f0 -x",
                        config.path_izone,
                        config.filename,
                        gZoneName);
*/
            snprintf (buffer, sizeof (buffer)-1,
                      "%s -c %s -z '%s' -i0 -l10 -o 0x0 -g 100x100 -f0 -x",
                        config.path_izone,
                        config.filename,
                        gZoneName);
            system (buffer);

            // Configure a fake emission's name
            strncpy (config.currentProgram.name, gZoneName, NAME_MAXLENGTH);

            if ((WID = loadItem (media)) <= 0)
            {
                snprintf (buffer, sizeof (buffer)-1,
                          "%s -c %s -z '%s' -k",
                            config.path_izone,
                            config.filename,
                            gZoneName);
                system (buffer);
                fprintf (stdout, "0\n");
                iCrit ("Cannot load the Item.");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            fprintf (stdout, "%d\n", WID);
            // Prepare to dimensions, layer, ...
            if (!prepareItem (WID))
            {
                snprintf (buffer, sizeof (buffer)-1,
                          "%s -c %s -z '%s' -k",
                            config.path_izone,
                            config.filename,
                            gZoneName);
                system (buffer);
                iCrit ("Cannot prepare the Item with WID %d",
                        WID);
                unloadItem (WID);
                freePlugins ();
                return (EXIT_FAILURE);
            }
            break;
        case 'p':
            if (!strlen (gZoneName))
            {
                iCrit ("No Zone specified. Exiting...");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            // Show on the main desktop
            if (!playItem (WID))
            {
                iCrit ("Cannot play the Item.");
                unloadItem (WID);
                freePlugins ();
                return (EXIT_FAILURE);
            }
            break;
        case 'u':
        case 's':
            if (!strlen (gZoneName))
            {
                iCrit ("No Zone specified. Exiting...");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            // Stop and unload application
            if (!unloadItem (WID))
            {
                iCrit ("Cannot unload the Item.");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            // If zone is a test zone, remove it
            if (strcmp (gZoneName, "__test__") == 0)
            {
                snprintf (buffer, sizeof (buffer)-1,
                          "%s -c %s -z '%s' -k",
                            config.path_izone,
                            config.filename,
                            gZoneName);
                system (buffer);
            }
            break;
        case 'w':
            if (!strlen (gZoneName))
            {
                iCrit ("No Zone specified. Exiting...");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            // Wait the end of the application
            if (!waitItem (WID))
            {
                iCrit ("Cannot wait the Item.");
                unloadItem (WID);
                freePlugins ();
                return (EXIT_FAILURE);
            }
            break;

        case 't':
            // Get the Plugin for the media
            if (!getPlugin (media, plugin, sizeof (plugin)))
            {
                fprintf (stderr, "There is no plugin for this Media.\n");
                freePlugins ();
                return (EXIT_FAILURE);
            }
            fprintf (stdout, "%s\n", plugin);
            break;
        case 'a':
            // List MIME that we know how to manage
            listPlugins ();
            break;

        default:
            fprintf (stderr, "Nothing to do. Exiting...\n");
    }

    freePlugins ();
    return (EXIT_SUCCESS);
}

char * getPlugin (char * media, char * buffer, int size)
{
    int i;
    memset (buffer, '\0', size);

    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, media, 
                     strlen (gPlugins[i].plugin)) == 0)
        {
            // Founded !
            strncpy (buffer, gPlugins[i].plugin, size - 1);
            return (buffer);
        }
    }

    // Not Found !
    iError ("[%s] No plugin for media '%s'", gZoneName, media);
    return (NULL);
}

void listPlugins ()
{
    int i;

    fprintf (stdout, "List of available plugins:\n\n");
    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins [i].plugin) ; i++)
        fprintf (stdout, "- '%s'\n", gPlugins [i].plugin);

    if (i == 0)
        fprintf (stdout, "List is empty.\n");
    fprintf (stdout, "\n");
}

void freePlugins ()
{
    int i;
    void * lastHandle = NULL;

    for (i = 0 ; i < PLUGIN_MAXNUMBER && strlen (gPlugins[i].plugin) ; i++)
    {
        if (gPlugins[i].handle && gPlugins[i].handle != lastHandle)
        {
            dlclose (gPlugins[i].handle);
            lastHandle = gPlugins[i].handle;
        }
    }
}

int loadPlugins ()
{
    char buffer [FILENAME_MAXLENGTH];
    int count;
    struct direct ** files;
    void * handle;
    int (*pInitPlugin) (sPlugins *, void *);

    // Load all shared library from the plugin directory
    if ((count = scandir (config.path_plugins,
                          &files, pluginSelect, alphasort)) == -1)
    {
        iError ("Can't get plugin from '%s'", config.path_plugins);
        return (0);
    }

    while (count--)
    {
        memset (buffer, '\0', sizeof (buffer));
        snprintf (buffer, sizeof (buffer) - 1, "%s/%s",
                    config.path_plugins, files[count]->d_name);

        // Load of the library
        handle = dlopen (buffer, RTLD_LAZY);
        if (handle)
        {
            pInitPlugin = dlsym (handle, "initPlugin");
            if (pInitPlugin)
            {
                // Function exist in the file, loading this plugin
                if (!pInitPlugin (&gPlugins, handle))
                {
                    iError ("[%s] Init Plugin failed.", 
                            files[count]->d_name);
                }
            }
        }
        else
        {
            iError ("Can't open '%s': %s", files[count]->d_name, dlerror ());
        }
        free (files[count]);
    }
    free (files);

    return (1);
}

int loadItem (char * media)
{
    int i, status;
    char plugin [PLUGIN_MAXLENGTH];
    char cmd [CMDLINE_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];
    char WID [16];
    int screenWidth, screenHeight;
    FILE * fp;
    sApp app;
    memset (plugin, '\0', PLUGIN_MAXLENGTH);
    memset (cmd, '\0', CMDLINE_MAXLENGTH);
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (WID, '\0', sizeof (WID));
    sZone zone;

    if (!shm_getZone (gZoneName, &zone))
    {
        iError ("[%s] Cannot get zone's information", gZoneName);
        return (0);
    }
    if (!getResolution (&config, &screenWidth, &screenHeight))
    {
        iError ("[%s] Cannot get zone's dimensions", gZoneName);
        iError ("[%s] Maybe a not allowed access to Xorg (xhost).", gZoneName);
        return (0);
    }

    // Get the Plugin
    if (!getPlugin (media, plugin, PLUGIN_MAXLENGTH))
    {
        iError ("[%s] No plugin for '%s'", gZoneName, media);
        return (0);
    }

    // Get the command line to load the application
    for (i = 0 ; i < PLUGIN_MAXNUMBER && !strlen (cmd) ; i++)
    {
        if (strncmp (gPlugins[i].plugin, plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (!gPlugins[i].pGetCmd ||
                !gPlugins[i].pGetCmd (&config, 
                                         gZoneName, media,
                                         zone.width * screenWidth / 100,
                                         zone.height * screenHeight / 100, 
                                         cmd, CMDLINE_MAXLENGTH))
            {
                iError ("[%s] Unable to launch media '%s'", 
                        gZoneName, media);
                return (0);
            }
        }
    }
    if (!strlen (cmd))
    {
        iError ("[%s] Unable to find a plugin for '%s'", gZoneName, media);
        return (0);
    }

    // Launch the command line
    snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -x '%s'",
                config.path_iwm,
                config.filename,
                cmd);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[%s] Unable to launch '%s'", gZoneName, buffer);
        return (0);
    }
    if (!fgets (WID, sizeof (WID), fp))
    {
        status = pclose (fp);
        iError ("[%s] Cannot get results of '%s'. Status is %d",
                gZoneName, buffer, status);

        // Unlock the launch of application if not done properly
        if (status != 256)
            sema_unlock ("/iplayer.iwm");
        
        // Close all opened window on the load desktop
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -a%d",
                    config.path_iwm,
                    config.filename,
                    config.desktop_load);
        system (buffer);

        return (0);
    }
    status = pclose (fp);

    if (status != 0 || atoi (buffer) < 0)
    {
        iError ("[%s] Unable to get WID for '%s'. Status is '%d'", 
                gZoneName, media, status);

        // Unlock the launch of application if not done properly
        sema_unlock ("/iplayer.iwm");

        // Close all opened window on the load desktop
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -z '%s' -a%d",
                    config.path_iwm,
                    config.filename,
                    config.desktop_load);
        system (buffer);
        return (0);
    }
    while (strlen(WID) && (WID[strlen(WID)-1] == '\r'
                        || WID[strlen(WID)-1] == '\n'))
        WID[strlen(WID)-1] = '\0';

    app.wid = atoi (WID);
    if (app.wid <= 0)
    {
        iError ("[%s] WID invalid for '%s': %d", 
                gZoneName, media, app.wid);
        return (0);
    }

    // Save it into Shared memory
    strncpy (app.zone, gZoneName, ZONE_MAXLENGTH);
    strncpy (app.plugin, plugin, PLUGIN_MAXLENGTH);
    strncpy (app.item, media, LINE_MAXLENGTH);
    app.pid = 0;
    if (!shm_saveApp (&app))
    {
        iError ("Unable to save the App. of '%s' in Memory.", media);
        return (0);
    }

    return (app.wid);
}

int prepareItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sZone zone;
    sApp app;
    int i;

    if (!shm_getZone (gZoneName, &zone))
        return (0);

    // Move the window on the correct layer
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -l %d",
                config.path_iwm,
                config.filename,
                wid,
                zone.layer);
    system (buffer);

    // Call specific preparation
    if (!shm_getApp (wid, &app))
    {
        iError ("Can't access to application refereced by Window ID %d", wid);
        return (0);
    }
    for (i = 0 ; i < PLUGIN_MAXNUMBER ; i++)
    {
        if (strncmp (gPlugins[i].plugin, app.plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (gPlugins[i].pPrepare && 
                !gPlugins[i].pPrepare (&config, wid))
            {
                iError ("[%s] Unable to Prepare the Media '%s'", 
                        gZoneName, app.plugin);
                return (0);
            }
            return (1);
        }
    }

    return (0);
}

int playItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sZone zone;
    sApp app;
    int i;

    if (!shm_getZone (gZoneName, &zone))
        return (0);

    if (!shm_setAppPID (wid, getpid ()))
    {
        iError ("Unable to save the App. with PID in Memory.", getpid ());
        return (0);
    }

    if (!shm_getApp (wid, &app))
    {
        iError ("Can't access to application refereced by Window ID %d", wid);
        return (0);
    }

    // Move the window on the correct desktop
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -s %d,-1,-1,-1,-1",
                config.path_iwm,
                config.filename,
                wid,
                config.desktop_main);
    system (buffer);

    shm_setAppStatus (wid, STATUS_PLAY);

    // If duration = 0, end is not known, we consider it is an infinite media
    // For "only one loop per zone" behaviour, marking the zone as already
    // done. So:
    // - Get the duration
    // - Mark the zone as INFINITE if duration=0 or unknown
    if (!getParam (&config, app.item, "duration", buffer, sizeof (buffer))
            || atoi (buffer) == 0)
    {
        iLog ("Marking zone %s as INFINITE.", gZoneName);
        if (shm_infiniteZone (gZoneName) == 0)
        {
            iDebug ("Zone %s is taggued INFINITE.", gZoneName);
        }
        else
        {
            iError ("[%d] Zone %d NOT taggued as INFINITE !", getpid (), gZoneName);
            iError ("[%d] Exiting !", getpid ());
            shm_setAppStatus (wid, STATUS_STOP);
            return (0);
        }
    }

    // Call specific play
    for (i = 0 ; i < PLUGIN_MAXNUMBER ; i++)
    {
        if (strncmp (gPlugins[i].plugin, app.plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (!gPlugins[i].pPlay || !gPlugins[i].pPlay (&config, wid))
            {
                iError ("[%s] Media with WID %d is not ending normally. Stop asked ?", 
                        gZoneName, wid);
            }
            shm_setAppStatus (wid, STATUS_STOP);

            return (1);
        }
    }

    shm_setAppStatus (wid, STATUS_STOP);
    return (0);
}

int waitItem (int wid)
{
    int status;
    int exists;
    int i;
    char buffer [CMDLINE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    FILE * fp = NULL;

    i = 0;
    exists = 0;
    do
    {
        sleep (1);

        // Once every 10s, check the window exists
        i = (i + 1) % 10;
        if (!i)
        {
            memset (buffer, '\0', CMDLINE_MAXLENGTH);
            memset (line, '\0', LINE_MAXLENGTH);
            snprintf (buffer, CMDLINE_MAXLENGTH-1,
                        "%s -z '%s' -w %d -e",
                        config.path_iwm, config.filename, wid);
            fp = popen (buffer, "r");
            if (!fp)
            {
                iError ("[%s] Unable to launch '%s'", gZoneName, buffer);
            }
            else
            {
                fgets (line, sizeof (line), fp);
                exists = pclose (fp);
                if (exists != 0)
                {
                    iError ("[%d] [%s] Waiting for window %d but is already closed.",
                            getpid (), gZoneName, wid);

                    // Marking application as Stopped
                    shm_setAppStatus (wid, STATUS_STOP);
                }
            }
        }
    }
    while ((status = shm_getAppStatus (wid)) == STATUS_PLAY);

    return (status >= 0);
}

int unloadItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    FILE * fp = NULL;
    int status;

    // Stop the item
    if (!stopItem (wid))
    {
        iError ("[%s] Unable to stop properly WID %d", gZoneName, wid);
        return (0);
    }

    // Close the window 
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (line, '\0', LINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -c",
                config.path_iwm, config.filename, wid);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[%s] Unable to launch '%s'", gZoneName, buffer);
        return (0);
    }
    fgets (line, sizeof (line), fp);
    status = pclose (fp);
    if (status != 0)
    {
        iError ("[%d] Error while closing '%s' (%s). Status is %d",
                getpid (), buffer, gZoneName, status);
        //sema_unlock ("/iplayer.iwm");
    }

    // Kill all child process
    kill (wid, 15);
        
    // Remove it from the memory
    return (shm_delApp (wid));
}

int stopItem (int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    char line [LINE_MAXLENGTH];
    sApp app;
    int i;
    FILE * fp = NULL;
    int status;

    // Hide the window
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    memset (line, '\0', LINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -i 0",
                config.path_iwm,
                config.filename,
                wid);
    fp = popen (buffer, "r");
    if (!fp)
    {
        iError ("[%s] Unable to launch '%s'", gZoneName, buffer);
        return (0);
    }
    fgets (line, sizeof (line), fp);
    status = pclose (fp);
    if (status != 0)
    {
        iError ("[%d] Error while stopping '%s' (%s). Status is %d",
                getpid (), buffer, gZoneName, status);
        //sema_unlock ("/iplayer.iwm");
    }

    // Call specific stop
    if (!shm_getApp (wid, &app))
    {
        iError ("Can't access to application refereced by Window ID %d", wid);
        return (0);
    }
    for (i = 0 ; i < PLUGIN_MAXNUMBER ; i++)
    {
        if (strncmp (gPlugins[i].plugin, app.plugin, PLUGIN_MAXLENGTH) == 0)
        {
            if (gPlugins[i].pStop && gPlugins[i].pStop (&config, wid) != 0)
            {
                iError ("[%s] Unable to Stop the Media '%s'", 
                        gZoneName, app.plugin);
            }
            // Sending signal to iAppCtrl playing process
            kill (app.pid, SIGTERM);
            return (1);
        }
    }

    return (1);
}

int getZoneDimensions (char * name, int * width, int * height)
{
    sZone zone;

    if (!shm_getZone (name, &zone))
        return (0);

    (*width)    = zone.width;
    (*height)   = zone.height;

    return (1);
}

void iAppCtrlSignalsHandler ()
{
    signal (SIGTERM, signalHandler);
    signal (SIGINT, signalHandler);
    signal (SIGHUP, signalHandler);
}

void signalHandler (int sig)
{
    switch (sig)
    {
        case SIGTERM:
        case SIGINT:
            gStop = 1;
            break;
        case SIGHUP:
            // Someone send us this signal because something changed
            // (play status, dimensions, ...)
            break;
        default:
            iError ("[%s] Unknown signal '%d' in iAppCtrl", gZoneName, sig);
    }
}
