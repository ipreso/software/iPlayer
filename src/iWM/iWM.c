// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        src/iWM/iWM.c
 * Description: Provide Windows information and management
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com
 * Changes:
 *  - 2009.01.22: Original revision
 *  - 2013.01.13: Use shortkeys to change window's layer (Fluxbox > 1.1)
 *****************************************************************************/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <X11/Xatom.h>
#include <signal.h>
#include <errno.h>

#include "iWM.h"
#include "iCommon.h"
#include "log.h"
#include "Sema.h"

#define HELP ""                                                         \
"Usage:\n"                                                              \
"iWM <command>\n"                                                       \
"  <command>:\n"                                                        \
"    -h, --help               Show this helpful message\n"              \
"    -d[X], --desktop[=X]     Get [or move to] the current Desktop ID\n"\
"    -r, --resolution         Get the current Screen Resolution\n"      \
"    -v, --version            Show the version of this tool\n"          \
"    -x, --launch '<cmd>'     Command 'cmd' is executed and Window ID is\n" \
"                             returned.\n"                              \
"    -a[X], --all[=X]         Close all windows opened [on desktop X]\n"\
"    -z, --conf '<file>'      Set the configuration file\n"             \
"\n"                                                                    \
"iWM <command> [<option>] -w <window>\n"                                \
"  <command> [<option>]:\n"                                             \
"    -c, --close              Close a window\n"                         \
"    -f, --fullscreen         Set a window in fullscreen mode\n"        \
"    -i, --show <{0,1}>       Show or hide a window\n"                  \
"    -k, --key <KeyID>        Send Key press event to the window\n"     \
"    -l, --layer <ID>         Change the layer of a window\n"           \
"    -m, --movein <D,X,Y,W,H> Center a window into a defined zone\n"    \
"    -s, --set <D,X,Y,W,H>    Set desktop, coordonates and size of a\n" \
"                             window\n"                                 \
"    -e, --exists             Returns 0 if window exists, 1 otherwise\n"\
"\n"                                                                    \
"    -w, --window <window>    ID of the concerned Window \n"            \
"\n"

#define MAX_PROPERTY_VALUE_LEN  4096

#define IWM_SEMAPHORE           "/iplayer.iwm"
#define LOAD_SEMAPHORE           "/iplayer.load"

static struct option long_options[] =
{
    {"all",         optional_argument,  0, 'a'},
    {"close",       no_argument,        0, 'c'},
    {"desktop",     optional_argument,  0, 'd'},
    {"exists",      no_argument,        0, 'e'},
    {"fullscreen",  no_argument,        0, 'f'},
    {"help",        no_argument,        0, 'h'},
    {"show",        required_argument,  0, 'i'},
    {"key",         required_argument,  0, 'k'},
    {"layer",       required_argument,  0, 'l'},
    {"movein",      required_argument,  0, 'm'},
    {"resolution",  no_argument,        0, 'r'},
    {"set",         required_argument,  0, 's'},
    {"version",     no_argument,        0, 'v'},
    {"window",      required_argument,  0, 'w'},
    {"launch",      required_argument,  0, 'x'},
    {"conf",        required_argument,  0, 'z'},
    {0, 0, 0, 0}
};

sConfig conf;
int gStop;

int main (int argc, char ** argv)
{
    int c, option_index = 0;
    int desktop = -1;
    int result;
    char window [16];
    char action = '\0';
    char params [CMDLINE_MAXLENGTH];
    char conffile [FILENAME_MAXLENGTH];
    int loadLock = 0;
    gStop = 0;

/*
int i;
memset (params, '\0', CMDLINE_MAXLENGTH);
for (i = 0 ; i < argc ; i++)
{
    snprintf (params + strlen (params), CMDLINE_MAXLENGTH-(1+strlen (params)), "%s ", argv[i]);
}
iDebug ("[%d] %s", getpid (), params);
*/

    memset (window, '\0', sizeof (window));
    memset (params, '\0', sizeof (params));
    memset (conffile, '\0', sizeof (conffile));

    // Parse parameters
    while ((c = getopt_long (argc, argv, "a::hd::erx:s:m:ck:l:fi:w:vz:",
                             long_options,
                             &option_index)) != EOF)
    {
        switch (c)
        {
            case 'h':
                // Display help
                fprintf (stdout, "%s", HELP);
                return EXIT_SUCCESS;
            case 'd':
                // If no argument, get the current Desktop ID
                // Or move to the specified desktop
                if (optarg)
                    setCurrentDesktop (atoi (optarg));
                else
                    getCurrentDesktop();
                return EXIT_SUCCESS;
            case 'r':
                // Get the resolution of the Screen
                getScreenResolution ();
                return EXIT_SUCCESS;
                break;
            case 'x':
                // Launch an application and send the loaded window on
                // the Wait desktop
                action = 'x';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 's':
                // Manage desktop, offset and size of a window
                action = 's';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'm':
                // Center a window
                action = 'm';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'a':
                // Close all windows
                action = 'a';
                if (optarg)
                    desktop = atoi (optarg);
                break;
            case 'c':
                // Close a window
                action = 'c';
                break;
            case 'e':
                // Check if window exists or not
                action = 'e';
                break;
            case 'k':
                action = 'k';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'l':
                // Change the layer of a window
                action = 'l';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'f':
                // Set a window in fullscreen mode
                action = 'f';
                break;
            case 'i':
                // Show or hide a window
                action = 'i';
                strncpy (params, optarg, sizeof (params)-1);
                break;
            case 'v':
                // Display version
                fprintf (stdout, "%s %s\n", IWM_APPNAME, IWM_VERSION);
                return EXIT_SUCCESS;
            case 'w':
                strncpy (window, optarg, sizeof (window)-1);
                break;
            case 'z':
                strncpy (conffile, optarg, sizeof (conffile)-1);
                break;
            default:
                fprintf(stderr, "Please check '%s --help'.\n", argv[0]);
                return EXIT_FAILURE;
        }

        // Reinitialize the Option Index
        option_index = 0;
    }

    // Do we have a configuration file parameter ?
    if (strlen (conffile) <= 0)
        strncpy (conffile, DEFAULT_CONFIGURATIONFILE, sizeof (conffile) - 1);

/*
int i;
char params2 [CMDLINE_MAXLENGTH];
memset (params2, '\0', CMDLINE_MAXLENGTH);
snprintf (params2, CMDLINE_MAXLENGTH-1, "[%d] ", getpid ());
for (i = 0 ; i < argc ; i++)
{
    snprintf (params2 + strlen (params2), CMDLINE_MAXLENGTH-(1+strlen (params2)), "%s ", argv[i]);
}
iDebug ("[%d] %s", getpid (), params2);
*/

    if (action == 'x')
    {
        // Lock a semaphore to only launch one application at a time
        if (!sema_createLockMaxWait (LOAD_SEMAPHORE, 90))
        {
            iError ("Cannot lock semaphore to only launch one application");
            return (EXIT_FAILURE);
        }
        loadLock = 1;
    }

    // Only one instance of iWM is accepted !
    if (!sema_createLock (IWM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to be the only iWM instance");
        if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
        {
            fprintf (stderr, "Cannot unlock our Load semaphore\n");
            iError ("Cannot unlock our Load semaphore");
        }
        return (EXIT_FAILURE);
    }

    // Handle X Errors
    XSetErrorHandler (XLibErrorHandler);

    if (action == 'x')
    {
        // Disable SIGTERM and SIGINT, always output something !
        signal (SIGTERM, signalHandler);
        signal (SIGINT, signalHandler);

        // Get the Configuration
        if (common_getConfig (conffile, &conf) <= 0)
        {
            if (!sema_unlock (IWM_SEMAPHORE))
            {
                if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
                {
                    fprintf (stderr, "Cannot unlock our Load semaphore\n");
                    iError ("Cannot unlock our Load semaphore");
                }
                fprintf (stderr, "Cannot unlock our iWM semaphore\n");
                iError ("Cannot unlock our iWM semaphore");
            }
            return (EXIT_FAILURE);
        }

        launchApp (params);
        if (!sema_unlock (IWM_SEMAPHORE))
        {
            fprintf (stderr, "Cannot unlock our iWM semaphore\n");
            iError ("Cannot unlock our iWM semaphore");
        }
        if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
        {
            fprintf (stderr, "Cannot unlock our Load semaphore\n");
            iError ("Cannot unlock our Load semaphore");
        }
        return (EXIT_SUCCESS);
    }
    else if (action == 'a')
    {
        result = closeAllWindows (desktop);
    }
    else if (action != '\0')
    {
        // Get the Configuration
        if (common_getConfig (conffile, &conf) <= 0)
        {
            if (!sema_unlock (IWM_SEMAPHORE))
            {
                if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
                {
                    fprintf (stderr, "Cannot unlock our Load semaphore\n");
                    iError ("Cannot unlock our Load semaphore");
                }
                fprintf (stderr, "Cannot unlock our iWM semaphore\n");
                iError ("Cannot unlock our iWM semaphore");
            }
            return (EXIT_FAILURE);
        }

        if (strlen (window) <= 0)
        {
            fprintf (stderr, "No Window targeted by the action. Exiting...\n");
            iCrit ("No Window targeted by the action '%c'", action);
            if (!sema_unlock (IWM_SEMAPHORE))
            {
                if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
                {
                    fprintf (stderr, "Cannot unlock our Load semaphore\n");
                    iError ("Cannot unlock our Load semaphore");
                }
                fprintf (stderr, "Cannot unlock our iWM semaphore\n");
                iError ("Cannot unlock our iWM semaphore");
            }
            return (EXIT_FAILURE);
        }
        doWindowAction (window, action, params);
        result = 0;
    }
    else
        result = EXIT_FAILURE;

    if (!sema_unlock (IWM_SEMAPHORE))
    {
        if (loadLock && !sema_unlock (LOAD_SEMAPHORE))
        {
            fprintf (stderr, "Cannot unlock our Load semaphore\n");
            iError ("Cannot unlock our Load semaphore");
        }
        fprintf (stderr, "Cannot unlock our iWM semaphore\n");
        iError ("Cannot unlock our iWM semaphore");
        return (EXIT_FAILURE);
    }
    return (result);
}

void getCurrentDesktop ()
{
    Display * disp;
    unsigned long * curDesktop = NULL;

    // Open the Display
    if (!(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        fprintf (stderr, "Can't open Display '%s' !", DISPLAY_NAME);
        iError ("Can't open Display '%s' !", DISPLAY_NAME);
        return;
    }

    Window root = DefaultRootWindow (disp);

    if (! (curDesktop =
            (unsigned long *) getProperty (disp,
                                           root,
                                           XA_CARDINAL,
                                           "_NET_CURRENT_DESKTOP",
                                           NULL)))
    {
        if (! (curDesktop =
                (unsigned long *) getProperty (disp,
                                               root,
                                               XA_CARDINAL,
                                               "_WIN_WORKSPACE",
                                               NULL)))
        {
            XCloseDisplay (disp);
            fprintf (stderr, "Can't get the Current Desktop ID.");
            iError ("Can't get the Current Desktop ID.");
            return;
        }
    }
    if (curDesktop == NULL)
    {
        fprintf (stderr, "-1");
        XCloseDisplay (disp);
        return;
    }

    fprintf (stdout, "%lu\n", *curDesktop);
    free (curDesktop);

    // Close the Display
    XCloseDisplay (disp);
}

void setCurrentDesktop (int desktop)
{
    Display * disp;

    // Open the Display
    if (!(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        fprintf(stderr, "Can't open Display '%s' !", DISPLAY_NAME);
        iError ("Can't open Display '%s' !", DISPLAY_NAME);
        return;
    }

    //iDebug ("Switching to Desktop %d.", desktop);
    clientMsg (disp, DefaultRootWindow (disp), "_NET_CURRENT_DESKTOP",
               (unsigned long) desktop, 0, 0, 0, 0);

    // Close the Display
    XCloseDisplay (disp);
}

void getScreenResolution ()
{
    system ("xdpyinfo | grep dimensions | awk '{print $2}'");
}

char * getProperty (Display * disp, Window win, Atom xaPropType,
                    char * propName, unsigned long * size)
{
    Atom xaPropName;
    Atom xaRetType;
    int retFormat;
    unsigned long retNitems;
    unsigned long retBytesAfter;
    unsigned long tmpSize;
    unsigned char *retProp = NULL;
    char * ret = NULL;

    if (!propName)
    {
        iError ("propName is NULL !");
        return (NULL);
    }

    xaPropName = XInternAtom(disp, propName, False);
    if (xaPropName == None ||
        xaPropName == BadAlloc ||
        xaPropName == BadValue)
    {
        iError ("xaPropName is Invalid");
        return (NULL);
    }

    // Check if Window is okay and will not cause a segmentation fault
    XWindowAttributes xwa;
    Status res = XGetWindowAttributes (disp, win, &xwa);
    if (res == BadDrawable || res == BadWindow)
    {
        iDebug ("[%d] Cannot get %s property: %s.", getpid (),
                 propName, (res == BadWindow) ? "BadWindow" : "BadDrawable");
        fprintf (stderr, "Cannot get %s property.\n", propName);
        return NULL;
    }

    /* MAX_PROPERTY_VALUE_LEN / 4 explanation (XGetWindowProperty manpage):
     *
     * long_length = Specifies the length in 32-bit multiples of the
     *               data to be retrieved.
     */
    if (XGetWindowProperty (disp, win, xaPropName, 0,
                            MAX_PROPERTY_VALUE_LEN / 4, False,
                            xaPropType, &xaRetType, &retFormat,
                            &retNitems, &retBytesAfter, &retProp) != Success)
    {
        iDebug ("[%d] Cannot get %s property (XGetWindowProperty != Success).",
                getpid (), propName);
        fprintf (stderr, "Cannot get %s property.\n", propName);
        return NULL;
    }

//if (strcmp (propName, "_NET_WM_DESKTOP") == 0)
//    iDebug ("[%d] Checking returned type...", getpid());

    if (xaRetType != xaPropType)
    {
        iDebug ("[%d] Invalid type of %s property (%lu != %lu).", 
                getpid (), propName, xaRetType, xaPropType);
        //fprintf (stderr, "Invalid type of %s property.\n", propName);
        if (retProp)
        {
            XFree(retProp);
            retProp = NULL;
        }
        return NULL;
    }

    // Null terminate the result to make string handling easier
//if (strcmp (propName, "_NET_WM_DESKTOP") == 0)
//    iDebug ("Composing result...");

    tmpSize = (retFormat / 8) * retNitems;

    // Correct 64bits implementation of 32 bit data
    if (retFormat == 32)
        tmpSize *= sizeof (long) / 4;

    ret = malloc (tmpSize + 1);
    if (ret)
    {
        memcpy (ret, retProp, tmpSize);
        ret[tmpSize] = '\0';
    }
    else
    {
        iError ("Unable to allocate memory for a string of length %d", tmpSize+1);
    }

    if (size)
        *size = tmpSize;

    if (retProp)
        XFree(retProp);

//if (strcmp (propName, "_NET_WM_DESKTOP") == 0)
//    iDebug ("Returning result");

    return ret;
}

int clientMsg (Display * disp, Window win, char * message,
               unsigned long data0, unsigned long data1,
               unsigned long data2, unsigned long data3,
               unsigned long data4)
{
    XEvent event;
    long mask = SubstructureRedirectMask | SubstructureNotifyMask;

    event.xclient.type          = ClientMessage;
    event.xclient.serial        = 0;
    event.xclient.send_event    = True;
    event.xclient.message_type  = XInternAtom (disp, message, False);
    event.xclient.window        = win;
    event.xclient.format        = 32;
    event.xclient.data.l[0]     = data0;
    event.xclient.data.l[1]     = data1;
    event.xclient.data.l[2]     = data2;
    event.xclient.data.l[3]     = data3;
    event.xclient.data.l[4]     = data4;

    if (XSendEvent (disp, DefaultRootWindow (disp), False, mask, &event))
    {
        return 1;
    }
    else
    {
        fprintf (stderr, "Cannot send %s event.\n", message);
        iError ("Cannot send %s event.", message);
        return 0;
    }
}

void launchApp (char * cmdLine)
{
    Display * disp;
    int result;
    int toWait;
    int nbWindows;
    //struct timeval timestamp;
    char windowName [16];
    char params [32];
    char buffer [CMDLINE_MAXLENGTH];

    // Open the Display
    if (!(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        fprintf (stderr, "Can't open Display '%s' !", DISPLAY_NAME);
        iError ("Can't open Display '%s' !", DISPLAY_NAME);
        return;
    }

    // Check that no Window is opened on the 'load' desktop
    if ((nbWindows = getWindowsCountOnDesktop (disp, conf.desktop_load)) > 0)
    {
        iError ("Can't launch application when another one is starting too.");
        fprintf (stdout, "-1\n");
        XCloseDisplay (disp);
        return;
    }

    // Delocking iWM semaphore, in order to be locked by non-launching iWM commands
    // (because we do not release LOAD_SEMAPHORE)
    if (!sema_unlock (IWM_SEMAPHORE))
    {
        fprintf (stderr, "Cannot unlock our iWM semaphore\n");
        iError ("Cannot unlock our iWM semaphore during Application launch");
    }

    // Set our new Group PID
    setpgid (0, 0);

    // Execute the command
    //iDebug ("Executing: '%s'", cmdLine);
    if ((result = system (cmdLine)) != 0)
    {
        iError ("Command '%s' ending with code '%d'", cmdLine, result);
        fprintf (stdout, "-2\n");
        XCloseDisplay (disp);
        // Kill all potentiel child running process
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -15 -g %d", 
                    PKILL_PATH, getpgid (0));
        system (buffer);
        return;
    }
    //iDebug ("[%d] '%s'", getpid (), cmdLine);

    // Wait for a window
    toWait = 10*conf.timeout_windowload;

    nbWindows = 0;
    while (!gStop &&
           toWait > 0 && 
          (nbWindows = getWindowsCountOnDesktop (disp, conf.desktop_load)) == 0)
    {
        // Try every 1/10s
        usleep (1000*100);
        toWait--;
    }

    // No Window appears
    if (toWait <= 0 || nbWindows == 0)
    {
        iError ("Application didn't load window after %d seconds.",
                conf.timeout_windowload);
        iDebug ("Command in error is : '%s'", cmdLine);
        fprintf (stdout, "-3\n");
        XCloseDisplay (disp);
        // Kill all potentiel child running process
        memset (buffer, '\0', CMDLINE_MAXLENGTH);
        snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -15 -g %d", 
                    PKILL_PATH, getpgid (0));
        system (buffer);
        return;
    }

    // Locking back the iWM semaphore, to update window property
    // /!\ The "unlock" operation is done in the calling function
    if (!sema_createLock (IWM_SEMAPHORE))
    {
        iError ("Cannot lock semaphore to be the only iWM instance (%d)", __LINE__);
        fprintf (stdout, "-6\n");
        return;
    }
    iDebug ("[%d] IWM Semaphore locked back after window appears...", getpid ());

    // Set its name to something we can easily call : its new Group PID
    //gettimeofday (&timestamp, NULL);
    //snprintf (windowName, sizeof (windowName), "%d", (int)timestamp.tv_usec);
    snprintf (windowName, sizeof (windowName), "%d", getpgid (0));
    setWindowNameOnDesktop (disp, windowName, conf.desktop_load);

    // Create property for layer memorization (default is "NORMAL" layer = 8)
    if (setCardinalAtom (disp, windowName, IPRESO_LAYER_ATOM, 1) < 0)
    {
        iError ("[%d] Cannot set the IPRESO_LAYER property.", getpid ());

        // Closing all windows on this desktop
        iDebug ("[%d] Closing all windows on the desktop %d", getpid (), conf.desktop_load);
        closeAllWindows (conf.desktop_load);
        XCloseDisplay (disp);
        return;
    }

    // Move the window to the waiting desktop
    memset (params, '\0', sizeof (params));
    snprintf (params, sizeof (params)-1, "%d,-1,-1,-1,-1",
                                         conf.desktop_wait);
    moveWindow (disp, windowName, params);

    // Wait the window is really moved to the new window
    while (!gStop &&
           toWait > 0 && 
          (nbWindows = getWindowsCountOnDesktop (disp, conf.desktop_load)) > 0)
    {
        // Try every 1/10s
        usleep (1000*100);
        toWait--;
    }

    if (nbWindows > 0)
    {
        iError ("[%d] Window is on the load desktop after %ds...",
                getpid (), conf.timeout_windowload);
        // Closing all windows on this desktop
        closeAllWindows (conf.desktop_load);

        // Wait the window is removed from the load window
        toWait = conf.timeout_windowclose * 10;
        while (!gStop &&
               toWait > 0 && 
              (nbWindows = getWindowsCountOnDesktop (disp, conf.desktop_load)) > 0)
        {
            // Try every 1/10s
            usleep (1000*100);
            toWait--;
        }
        if (nbWindows > 0)
        {
            iError ("[%d] Cannot remove all windows from the load desktop\n",
                    getpid ());
            fprintf (stdout, "-4\n");
            XCloseDisplay (disp);
            return;
        }
        else
        {
            iDebug ("[%d] Windows removed from the load desktop\n",
                    getpid ());
            fprintf (stdout, "-5\n");
            XCloseDisplay (disp);
            return;
        }
    }

    // Show the window name
    fprintf (stdout, "%s\n", windowName);

    // Close the Display
    XCloseDisplay (disp);
}

int getWindowsCountOnDesktop (Display * disp, int desktop)
{
    Window * list;
    unsigned long size;
    unsigned long * pDesktopID;
    int i, total;

    // Get the list of Windows
    if ((list = getWindowsList (disp, &size)) == NULL)
    {
        iError ("Cannot count the number of window on desktop %d", desktop);
        return (-1);
    }

    // Count number of windows on the specified desktop
    total = 0;
    for (i = 0 ; i < size / sizeof (Window) ; i++)
    {
        if ((pDesktopID = (unsigned long*) getProperty (disp,
                                                        list [i],
                                                        XA_CARDINAL,
                                                        "_NET_WM_DESKTOP",
                                                        NULL)) == NULL)
        {
            pDesktopID = (unsigned long*) getProperty (disp,
                                                       list [i],
                                                       XA_CARDINAL,
                                                       "_WIN_WORKSPACE",
                                                       NULL);
        }
        if (pDesktopID == NULL)
        {
            iDebug ("[%d] Cannot get the desktop ID for window 0x%x (count). " \
                    "Ignoring...", getpid (), list[i]);
            continue;
        }

        if (*pDesktopID == desktop)
            total++;
    }
    return (total);
}

Window * getWindowsList (Display * disp, unsigned long * size)
{
    Window * list;

    if ((list = (Window*) getProperty (disp,
                                       DefaultRootWindow(disp),
                                       XA_WINDOW,
                                       "_NET_CLIENT_LIST",
                                       size)) == NULL)
    {
        if ((list = (Window*) getProperty (disp,
                                           DefaultRootWindow(disp),
                                           XA_WINDOW,
                                           "_WIN_CLIENT_LIST",
                                           size)) == NULL)
        {
            iError ("Can't get the Windows List.");
            fprintf (stderr, "Can't get the Windows List.");
            return (NULL);
        } 
    }

    return (list);
}

int setCardinalAtom (Display * disp, char * window, char * prop, unsigned long value)
{
    unsigned long localValue = value;
    unsigned long * returnedValue = NULL;

    Window win = getWindow (disp, window);
    if (win == BadWindow)
       return (-1);

    XChangeProperty (disp, win, XInternAtom (disp, prop, True),
                     XA_CARDINAL, 32, PropModeReplace,
                     (unsigned char*) &localValue, 1);

    returnedValue = (unsigned long *) getProperty (disp, win, XA_CARDINAL, prop, NULL);
    if (returnedValue && *returnedValue == localValue)
        return (*returnedValue);
    else
        return (-2);
}


int setWindowNameOnDesktop (Display * disp, char * name, int desktop)
{
    Window * list;
    unsigned long size;
    unsigned long * pDesktopID;
    int ok, i;

    ok = -1;
    
    if (!disp)
        return (-1);

    // Get the list of Windows
    if ((list = getWindowsList (disp, &size)) == NULL)
        return (-1);

    // Get the First Window on the specified desktop
    for (i = 0 ; ok != 1 && i < size / sizeof (Window) ; i++)
    {
        if ((pDesktopID = (unsigned long*) getProperty (disp,
                                                        list [i],
                                                        XA_CARDINAL,
                                                        "_NET_WM_DESKTOP",
                                                        NULL)) == NULL)
        {
            pDesktopID = (unsigned long*) getProperty (disp,
                                                       list [i],
                                                       XA_CARDINAL,
                                                       "_WIN_WORKSPACE",
                                                       NULL);
        }

        // We are on the correct Desktop ?
        if (pDesktopID && *pDesktopID == desktop)
        {
            char * wmName = NULL;
            while (!wmName || strcmp (wmName, name) != 0)
            {
                // Create the IPRESO_WID property
                iDebug ("Creating IPRESO_WID property for window '%s' (0x%x)", 
                        name, list [i]);
                XChangeProperty (disp, list [i],
                                 XInternAtom (disp, "IPRESO_WID", True),
                                 XA_STRING, 8, PropModeReplace,
                                 (unsigned char*) name, strlen (name));

                wmName = getProperty (disp, list [i], XA_STRING, "IPRESO_WID", NULL);
                iDebug ("[%d] getProperty returned %s", getpid (), wmName);
            }
            if (wmName)
            {
                free (wmName);
                wmName = NULL;
                ok = 1;
            }
            else
            {
                ok = -1;
            }
        }
    }

    return (ok);
}

void doWindowAction (char * window, char action, char * params)
{
    Display * disp;
    Window win;

    // Open the Display
    if (!(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        fprintf(stderr, "Can't open Display '%s' !", DISPLAY_NAME);
        iError ("Can't open Display '%s' !", DISPLAY_NAME);
        return;
    }

    switch (action)
    {
        case 'k':
            // Send key event to the Window
            sendEventToWindow (disp, window, params);
            break;
        case 's':
            // Move a window (desktop, x, y, width, height)
            moveWindow (disp, window, params);
            break;
        case 'm':
            // Center a window in a zone (desktop, x, y, width, height)
            centerWindow (disp, window, params);
            break;
        case 'c':
            // Close a window
            closeWindow (disp, window);
            break;
        case 'f':
            // Fullscreen mode
            fsWindow (disp, window);
            break;
        case 'i':
            // Show or hide a window
            showOrHideWindow (disp, window, params);
            break;
        case 'l':
            // Change the layer of a window
            changeLayer (disp, window, params);
            break;
        case 'e':
            win = getWindow (disp, window);
            if (win == BadWindow)
            {
                XCloseDisplay (disp);
                if (!sema_unlock (IWM_SEMAPHORE))
                {
                    fprintf (stderr, "Cannot unlock our iWM semaphore\n");
                    iError ("Cannot unlock our iWM semaphore");
                }
                exit (EXIT_FAILURE);
            }
            else
            {
                XCloseDisplay (disp);
                if (!sema_unlock (IWM_SEMAPHORE))
                {
                    fprintf (stderr, "Cannot unlock our iWM semaphore\n");
                    iError ("Cannot unlock our iWM semaphore");
                    exit (EXIT_FAILURE);
                }
                exit (EXIT_SUCCESS);
            }
            break;
        default:
            fprintf (stderr, "Unknown action '%c'\n", action);
    }

    XCloseDisplay (disp);
}

void closeWindow (Display * disp, char * window)
{
    Window * list;
    int found;
    int elapsed;
    int i;
    unsigned long size;
    char buffer [CMDLINE_MAXLENGTH];

    Window win = getWindow (disp, window);
    if (win == BadWindow)
    {
        return;
    }
    clientMsg (disp, win, "_NET_CLOSE_WINDOW", 0, 0, 0, 0, 0);

    // Wait to not have this window in the list before leaving
    // iWM would have a segmentation fault if the window
    // is in the list when it's launched, and closed when 
    // accessing it
    found = 1;
    elapsed = 0;
    while (elapsed < conf.timeout_windowclose * 10 && found)
    {
        if ((list = getWindowsList (disp, &size)) == NULL)
        {
            return;
        }

        found = 0;
        for (i = 0 ; !found && i < size / sizeof (Window) ; i++)
        {
            if (list [i] == win)
                found = 1;
        }
        if (found)
        {
            usleep (1000*100);
            elapsed++;
        }
    }
    if (found)
    {
        iError ("[%d] Window '%s' is still here after %ds.",
                 getpid (), window, conf.timeout_windowclose);
        if (atoi (window) > 1)
        {
            // Non-daemon process
            iDebug ("[%d] Killing processes with pgid %d",
                    getpid (), atoi (window));
            memset (buffer, '\0', CMDLINE_MAXLENGTH);
            snprintf (buffer, CMDLINE_MAXLENGTH-1, "%s -15 -g %d", 
                        PKILL_PATH, atoi (window));
            system (buffer);
        }
    }
}

void changeLayer (Display * disp, char * window, char * params)
{
    int layer = atoi (params);
    unsigned long fluxboxLayer;

    if (layer <= 0 || layer > LAYER_MAX)
    {
        iError ("Layer %d is invalid ([1-%d)", layer, LAYER_MAX);
        return;
    }

    fluxboxLayer = (LAYER_MAX + 1) - layer;

    Window win = getWindow (disp, window);
    if (win == BadWindow)
    {
        iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }
    iDebug ("Changing Window '%s' to layer %d", window, layer);
    clientMsg (disp, win, IPRESO_LAYER_ATOM, fluxboxLayer, 0, 0, 0, 0);
    setCardinalAtom (disp, window, IPRESO_LAYER_ATOM, fluxboxLayer);
}

void fsWindow (Display * disp, char * window)
{
    Window win = getWindow (disp, window);
    if (win == BadWindow)
    {
        iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }
    //iDebug ("Changing Window '%s' to Fullscreen mode", window);
    clientMsg (disp, win, "_NET_WM_STATE", 1, 
                XInternAtom(disp,"_NET_WM_STATE_FULLSCREEN", False), 0, 0, 0);
}

void showOrHideWindow (Display * disp, char * window, char * params)
{
    int show = atoi (params);
    if (show != 0 && show != 1)
    {
        iError ("Show parameter is '%d'. Possible values are 0 or 1.", show);
        return;
    }
    Window win = getWindow (disp, window);
    if (win == BadWindow)
    {
        if (show)
            iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }
    if (show)
    {
        //iDebug ("Showing window '%s'", window);
        clientMsg (disp, win, "_NET_WM_STATE", 0,
                   XInternAtom (disp, "_NET_WM_STATE_HIDDEN", False), 0, 0, 0);
    }
    else
    {
        clientMsg (disp, win, "_NET_WM_STATE", 1,
                   XInternAtom (disp, "_NET_WM_STATE_HIDDEN", False), 0, 0, 0);
    }
}

// Function to create a keyboard event
XKeyEvent createKeyEvent (Display * display, Window win,
                           Window winRoot, int press,
                           int keycode, int modifiers)
{
    XKeyEvent event;

    event.display       = display;
    event.window        = win;
    event.root          = winRoot;
    event.subwindow     = None;
    event.time          = CurrentTime;
    event.x             = 1;
    event.y             = 1;
    event.x_root        = 1;
    event.y_root        = 1;
    event.same_screen   = True;
    event.keycode       = XKeysymToKeycode (display, keycode);
    event.state         = modifiers;

    event.send_event    = False;
    event.serial        = None;

    if (press)
        event.type = KeyPress;
    else
        event.type = KeyRelease;

    return event;
}

void sendEventToWindow (Display * disp, char * window, char * params)
{
    int key1, key2, key3, key4;
    Window winRoot = XDefaultRootWindow (disp);
    XKeyEvent event;
    int modifiers;

    key1 = 0;
    key2 = 0;
    key3 = 0;
    key4 = 0;
    
    // Parse the parameters
    if (sscanf (params, "%x+%x+%x+%x", &key1, &key2, &key3, &key4) < 1)
    {
        iError ("Invalid keys to send to the Window");
        return;
    }
    
    Window win;
    win = getWindow (disp, window);
    if (win == BadWindow)
    {
        iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }

    Window focusedWindow;
    int reverttofocus;
    XGetInputFocus (disp, &focusedWindow, &reverttofocus);

    // Send Focus to the window
    XSetInputFocus (disp, win, RevertToNone, CurrentTime);

    modifiers = 0;
    if (key1)
    {
        iDebug ("%c%c%c%c - Sending KEYPRESS 0x%x",
                    (modifiers & ShiftMask) ? 'S' : '.', 
                    (modifiers & ControlMask) ? 'C' : '.',
                    (modifiers & Mod1Mask) ? 'A' : '.',
                    (modifiers & Mod4Mask) ? 'W' : '.',
                    key1);
        event = createKeyEvent (disp, win, winRoot, 1, key1, 0);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
        // File /usr/include/X11/keysymdef.h is describing modifiers
        // SHIFT keys
        if (key1 == XK_Shift_L || key1 == XK_Shift_R)
            modifiers |= ShiftMask;
        // CONTROL keys
        if (key1 == XK_Control_L || key1 == XK_Control_R)
            modifiers |= ControlMask;
        // ALT keys
        if (key1 == XK_Alt_L || key1 == XK_Alt_R)
            modifiers |= Mod1Mask;
        // WINDOWS keys
        if (key1 == XK_Meta_L || key1 == XK_Meta_R ||
            key1 == XK_Super_L || key1 == XK_Super_R)
            modifiers |= Mod4Mask;
    }
    if (key2)
    {
        iDebug ("%c%c%c%c - Sending KEYPRESS 0x%x",
                    (modifiers & ShiftMask) ? 'S' : '.', 
                    (modifiers & ControlMask) ? 'C' : '.',
                    (modifiers & Mod1Mask) ? 'A' : '.',
                    (modifiers & Mod4Mask) ? 'W' : '.',
                    key2);
        event = createKeyEvent (disp, win, winRoot, 1, key2, modifiers);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
        if (key2 == XK_Shift_L || key2 == XK_Shift_R)
            modifiers |= ShiftMask;
        if (key2 == XK_Control_L || key2 == XK_Control_R)
            modifiers |= ControlMask;
        if (key2 == XK_Alt_L || key2 == XK_Alt_R)
            modifiers |= Mod1Mask;
        if (key2 == XK_Meta_L || key2 == XK_Meta_R ||
            key2 == XK_Super_L || key2 == XK_Super_R)
            modifiers |= Mod4Mask;
    }
    if (key3)
    {
        iDebug ("%c%c%c%c - Sending KEYPRESS 0x%x",
                    (modifiers & ShiftMask) ? 'S' : '.', 
                    (modifiers & ControlMask) ? 'C' : '.',
                    (modifiers & Mod1Mask) ? 'A' : '.',
                    (modifiers & Mod4Mask) ? 'W' : '.',
                    key3);
        event = createKeyEvent (disp, win, winRoot, 1, key3, modifiers);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
        if (key3 == XK_Shift_L || key3 == XK_Shift_R)
            modifiers |= ShiftMask;
        if (key3 == XK_Control_L || key3 == XK_Control_R)
            modifiers |= ControlMask;
        if (key3 == XK_Alt_L || key3 == XK_Alt_R)
            modifiers |= Mod1Mask;
        if (key3 == XK_Meta_L || key3 == XK_Meta_R ||
            key3 == XK_Super_L || key3 == XK_Super_R)
            modifiers |= Mod4Mask;
    }
    if (key4)
    {
        iDebug ("%c%c%c%c - Sending KEYPRESS 0x%x",
                    (modifiers & ShiftMask) ? 'S' : '.', 
                    (modifiers & ControlMask) ? 'C' : '.',
                    (modifiers & Mod1Mask) ? 'A' : '.',
                    (modifiers & Mod4Mask) ? 'W' : '.',
                    key4);
        event = createKeyEvent (disp, win, winRoot, 1, key4, modifiers);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }

    // Release each keycode
    if (key4)
    {
        iDebug ("Releasing key 0x%x", key4);
        event = createKeyEvent (disp, win, winRoot, 0, key4, 0);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
    if (key3)
    {
        iDebug ("Releasing key 0x%x", key3);
        event = createKeyEvent (disp, win, winRoot, 0, key3, 0);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
    if (key2)
    {
        iDebug ("Releasing key 0x%x", key2);
        event = createKeyEvent (disp, win, winRoot, 0, key2, 0);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
    if (key1)
    {
        iDebug ("Releasing key 0x%x", key1);
        event = createKeyEvent (disp, win, winRoot, 0, key1, 0);
        XSendEvent (event.display, event.window, True, KeyPressMask, (XEvent *)&event);
    }
}

void moveWindow (Display * disp, char * window, char * params)
{
    signed long desktop, x, y, width, height;
    
    // Parse the parameters
    if (sscanf (params, "%ld,%ld,%ld,%ld,%ld", &desktop, &x, &y,
                                               &width, &height) != 5)
    {
        iError ("Invalid parameters to move the Window");
        return;
    }

    if (desktop != -1)
    {
        // Move the window to the specified desktop
        //iDebug ("Moving the Window '%s' to desktop %d",
        //            window, 
        //            (unsigned long)desktop);
        sendToDesktop (disp, window, desktop);
    }

    Window win;
    if ((width < 1 || height < 1) && (x >= 0 && y >= 0))
    {
        //iDebug ("Moving window '%s' to coordinates %ldx%ld", window, x, y);
        win = getWindow (disp, window);
        if (win == BadWindow)
        {
            iError ("[%d] %s doesn't exist.", getpid (), window);
            return;
        }
        XMoveWindow (disp, win, x, y);
    }
    else if ((x < 0 || y < 0) && (width >= 1 && height >= -1))
    {
        //iDebug ("Resizing window '%s' to %ldx%ld", window, width, height);
        win = getWindow (disp, window);
        if (win == BadWindow)
        {
            iError ("[%d] %s doesn't exist.", getpid (), window);
            return;
        }
        XResizeWindow (disp, win, width, height);
    }
    else if (x >= 0 && y >= 0 && width >= 1 && height >= 1)
    {
        //iDebug ("Moving/Resizing window '%s' to %ldx%ld:%ldx%ld", 
        //            window, 
        //            x, y, width, height);
        win = getWindow (disp, window);
        if (win == BadWindow)
        {
            iError ("[%d] %s doesn't exist.", getpid (), window);
            return;
        }
        XMoveResizeWindow (disp, win, x, y, width, height);
    }
}

void centerWindow (Display * disp, char * window, char * params)
{
    signed long zDesktop, zX, zY, zWidth, zHeight;  // Zone characteristics
    unsigned int width, height;                     // Window characteristics
    int x, y;

    signed long newX, newY;
    
    // Parse the parameters
    if (sscanf (params, "%ld,%ld,%ld,%ld,%ld", &zDesktop, &zX, &zY,
                                               &zWidth, &zHeight) != 5)
    {
        iError ("Invalid parameters to center the Window");
        return;
    }

    if (zX < 0 || zY < 0 || zWidth < 1 || zHeight < 1)
    {
        iError ("Invalid zone dimensions.");
        return;
    }

    if (zDesktop != -1)
    {
        // Move the window to the specified desktop
        iDebug ("Moving the Window '%s' to desktop %d",
                    window, 
                    (unsigned long)zDesktop);
        sendToDesktop (disp, window, zDesktop);
    }

    Window win, junk;
    int junkx, junky;
    unsigned int borderw, depth;
    win = getWindow (disp, window);
    if (win == BadWindow)
    {
        iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }

    // Get current dimensions of the Window
    XGetGeometry (disp, win, &junk, &junkx, &junky,
                  &width, &height, &borderw, &depth);
    XTranslateCoordinates (disp, win, junk, junkx, junky, &x, &y, &junk);

    if (width > zWidth)
    {
        // Window width is greater than zone Width
        newX = zX;
    }
    else
    {
        newX = zX + (zWidth - width) / 2;
    }
    if (height > zHeight)
    {
        // Window is greater than zone
        newY = zY;
    }
    else
    {
        newY = zY + (zHeight - height) / 2;
    }

    //iDebug ("Centering window '%s' to coordinates %ldx%ld", window, newX, newY);
    XMoveWindow (disp, win, newX, newY);
}

void sendToDesktop (Display * disp, char * window, int desktop)
{
    //iDebug ("Sending Window '%s' to desktop '%u'", window, desktop);
    Window win = getWindow (disp, window);
    if (win == BadWindow)
    {
        iError ("[%d] %s doesn't exist.", getpid (), window);
        return;
    }
    clientMsg (disp, win, "_NET_WM_DESKTOP", (unsigned long) desktop,
               0, 0, 0, 0);
}

Window getWindow (Display * disp, char * name)
{
    Window * list;
    unsigned long size;
    int i;
    char * wmName = NULL;

    // Get the list of Windows
    if ((list = getWindowsList (disp, &size)) == NULL)
        return (BadWindow);

    for (i = 0 ; i < size / sizeof (Window) ; i++)
    {
        wmName = getProperty (disp, list [i], XA_STRING, "IPRESO_WID", NULL);
        if (wmName == NULL)
        {
            continue;
        }

        if (strcmp (wmName, name) == 0)
        {
            free (wmName);
            return (list[i]);
        }

        if (wmName)
            free (wmName);
    }
    return (BadWindow);
}

int closeAllWindows (int desktop)
{
    Display * disp;
    Window * list;
    unsigned long size;
    unsigned long * pDesktopID;
    int i;

    if (desktop == -1)
        iDebug ("Closing all existing windows...");
    else
        iDebug ("Closing all existing windows on desktop #%d", desktop);

    // Open the Display
    if (!(disp = XOpenDisplay (DISPLAY_NAME)))
    {
        fprintf(stderr, "Can't open Display '%s' !", DISPLAY_NAME);
        iError ("Can't open Display '%s' !", DISPLAY_NAME);
        return (-1);
    }

    // Get the list of Windows
    if ((list = getWindowsList (disp, &size)) == NULL)
    {
        XCloseDisplay (disp);
        return (0);
    }

    for (i = 0 ; i < size / sizeof (Window) ; i++)
    {
        if ((pDesktopID = (unsigned long*) getProperty (disp,
                                                        list [i],
                                                        XA_CARDINAL,
                                                        "_NET_WM_DESKTOP",
                                                        NULL)) == NULL)
        {
            pDesktopID = (unsigned long*) getProperty (disp,
                                                       list [i],
                                                       XA_CARDINAL,
                                                       "_WIN_WORKSPACE",
                                                       NULL);
        }
        if (pDesktopID == NULL)
        {
            iDebug ("Cannot get the desktop ID for window %d (closeAllWindows)",
                    i);
            continue;
        }
        if (desktop == -1 || *pDesktopID == desktop)
        {
            clientMsg (disp, list[i], "_NET_CLOSE_WINDOW", 0, 0, 0, 0, 0);
        }
    }

    XCloseDisplay (disp);
    return (0);
}

int XLibErrorHandler (Display * disp, XErrorEvent * event)
{
    //iError ("[%d] XLib Error on Resource 0x%x", getpid (), event->resourceid);
    iError ("[%d] XLib Error on Resource 0x%x (%d)", 
                getpid (), event->resourceid, (int)event->error_code);
    return ((int)event->error_code);
}

void signalHandler (int sig)
{
    switch (sig)
    {
        case SIGINT:
        case SIGTERM:
            // Exit now !
            gStop = 1;
            break;
        default:
            iError ("Uncaught signal : %d", sig);
    }
}
