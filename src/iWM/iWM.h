// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        src/iWM/iWM.h
 * Description: Provide Windows information and management
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com
 * Changes:
 *  - 2009.01.22: Original revision
 *  - 2013.01.13: Use shortkeys to change window's layer (Fluxbox > 1.1)
 *****************************************************************************/

#ifndef VERSION
#define IWM_VERSION     "Version Unknown"
#else
#define IWM_VERSION     VERSION
#endif
#define IWM_APPNAME     "iWM (iPlayer Window Manager)"

#define DISPLAY_NAME    ":0.0"

#define DEFAULT_CONFIGURATIONFILE   "/etc/ipreso/iplayer.conf"
#define PKILL_PATH                  "/usr/bin/pkill"

#define IPRESO_LAYER_ATOM           "IPRESO_LAYER"

#include <X11/Xlib.h>
#include <X11/keysym.h>

char * getProperty      (Display * disp,        Window win,
                         Atom xaPropType,       char * propName,
                         unsigned long * size);
int clientMsg           (Display * disp,        Window win,
                         char * message,        unsigned long data0,
                         unsigned long data1,   unsigned long data2,
                         unsigned long data3,   unsigned long data4);

void getCurrentDesktop  ();
void setCurrentDesktop  (int desktop);

void getScreenResolution();

void launchApp          (char * cmdLine);
void doWindowAction     (char * window, char action, char * params);
void moveWindow         (Display * disp, char * window, char * params);
void centerWindow       (Display * disp, char * window, char * params);
void closeWindow        (Display * disp, char * window);
void fsWindow           (Display * disp, char * window);
void showOrHideWindow   (Display * disp, char * window, char * params);
void sendToDesktop      (Display * disp, char * window, int desktop);
void changeLayer        (Display * disp, char * window, char * params);
int  closeAllWindows    (int desktop);

int getWindowsCountOnDesktop (Display * disp, int desktop);
Window * getWindowsList (Display * disp, unsigned long * size);
Window getWindow (Display * disp, char * name);

int setWindowNameOnDesktop (Display * disp, char * name, int desktop);
int setCardinalAtom (Display * disp, char * window, char * prop, unsigned long value);
void sendEventToWindow (Display * disp, char * window, char * params);

void signalHandler (int sig);
int XLibErrorHandler (Display * disp, XErrorEvent * event);
