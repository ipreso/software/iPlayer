# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# *****************************************************************************
#  Description : Makefile to build the Window Manager component
#  Auteur      : Marc Simonetti
# *****************************************************************************


BIN=iWM

RM=@rm -f
CP=@cp
CC=@gcc

ifdef INCLUDEDIR
INCLUDE=-I $(INCLUDEDIR)
else
INCLUDE=
endif

ifdef LIBDIR
LIBSDIR=-L. -L$(LIBDIR)
else
LIBSDIR=-L.
endif

ifdef VERSION
VER=$(VERSION)
else
VER=Unknown
endif

CFLAGS=-Wall -Wunused -g -O $(INCLUDE) -D_XOPEN_SOURCE=500 -DVERSION="\"$(VER)\"" -I/usr/include/i386-linux-gnu
CC_OPT=-L. 
LIBS=$(LIBSDIR) -liPlayer -lX11 -lpthread -B/usr/lib/i386-linux-gnu

# *****************************************************************************
# Operations
%.o: %.c
	@echo "=== Compiling $@..."
	$(CC) -o $@ -c $< $(CFLAGS)

# *****************************************************************************
# Targets
all: $(BIN)

clean:
	@echo -n "Cleaning... "
	$(RM) *.o $(BIN)
	$(RM) $(BINDIR)/$(BIN)
	@echo "Ok"

install: $(BIN)
	install -d $(DESTDIR)/usr/bin/
	install -m755 $(BIN) $(DESTDIR)/usr/bin/

# *****************************************************************************
# Dependances
iWM.o:              iWM.c

# *****************************************************************************
# Edition de liens
$(BIN):             iWM.o
	                @echo "=== Linking $@..."
	                $(CC) -o $@ $^ $(CC_OPT) $(LIBS)
	                @if test ! -z "$(BINDIR)";  \
					then                        \
						cp $@ $(BINDIR)/$@ ; \
					fi;
	                @echo "Done."
