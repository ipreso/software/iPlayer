# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# *****************************************************************************
#  Description : Makefile to build iPlayer project.
#  Auteur      : Marc Simonetti
# *****************************************************************************

SOURCES="src"
MAKEOPTS="--no-print-directory"

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

all:
	@(cd $(SOURCES) ; $(MAKE) all $(MAKEOPTS))

install:
	# Compile and install binaries
	@(cd $(SOURCES) ; $(MAKE) install $(MAKEOPTS))
	
	# Copy additional scripts
	cp bin/iLaunch-pre $(DESTDIR)/usr/bin
	cp bin/ipreso-reconfigure $(DESTDIR)/usr/bin
	cp bin/ipreso-triggerLastEmission.sh $(DESTDIR)/usr/bin
	
	# Install default configuration files
	mkdir -p $(DESTDIR)/usr/share/iplayer
	cp -R etc $(DESTDIR)/usr/share/iplayer/
	mkdir -p $(DESTDIR)/etc
	ln -sf ../../usr/share/iplayer/etc $(DESTDIR)/etc/ipreso
	
	# Link configuration to third-party software
	# - Sudo:
	mkdir -p $(DESTDIR)/etc/sudoers.d
	ln -sf ../ipreso/sudoers $(DESTDIR)/etc/sudoers.d/ipreso
	# - Rsyslog:
	mkdir -p $(DESTDIR)/etc/rsyslog.d
	ln -sf ../ipreso/rsyslog-10-input.conf $(DESTDIR)/etc/rsyslog.d/10-iplayer-input.conf
	#ln -sf ../ipreso/rsyslog-50-ssl.conf $(DESTDIR)/etc/rsyslog.d/50-iplayer-ssl.conf
	ln -sf ../ipreso/rsyslog-60-local.conf $(DESTDIR)/etc/rsyslog.d/60-iplayer-local.conf
	# - Logrotate:
	mkdir -p $(DESTDIR)/etc/logrotate.d
	ln -sf ../ipreso/logrotate.conf $(DESTDIR)/etc/logrotate.d/iplayer.conf
	
	# Install WP logo
	mkdir -p $(DESTDIR)/usr/share/iplayer/resources
	cp plymouth/Logo.png $(DESTDIR)/usr/share/iplayer/resources/
	# Install Plymouth themes
	mkdir -p $(DESTDIR)/usr/share/plymouth/themes
	cp -R plymouth/ipresoblack $(DESTDIR)/usr/share/plymouth/themes/
	cp -R plymouth/ipresowhite $(DESTDIR)/usr/share/plymouth/themes/
	
	# Create playlist directory
	install -d $(DESTDIR)/var/lib/iplayer/playlists/

clean:
	@(cd $(SOURCES) ; $(MAKE) clean $(MAKEOPTS))
