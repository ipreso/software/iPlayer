#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# This script is executed at startup.
# Its goal is to prepare the environment so that iLaunch
# can start successfully.
#
# Tasks are:
# - Set the hostname (<hash>.ipreso.com)
# - Configure Logs (use correct certif., log to composer or logs.ipreso.com)
# - Allow RTC wakeup
# - Set volume to maximum
# - Update network interface with the detected one
# - Update drive with the detected one
# - Configure iPreos to use detected network card

RUNNING_USER="player"

function failure
{
    echo "Error"
    if [ -n "$1" ] ; then
        echo "$1"
    fi
    exit 1
}

function configureHostname
{
    NET_DEVICE_PATH=$( find /sys/devices/ -type d -name net | grep -v virtual | head -n1 )
    NET_DEVICE=$( ls -1 "${NET_DEVICE_PATH}/" | head -n1 )

    MAC=$( cat "${NET_DEVICE_PATH}/${NET_DEVICE}/address" )

    if [ -e "/dev/sda" ] ; then
        /sbin/hdparm -i /dev/sda 2>/dev/null 1>&2
        if [ "$?" != 0 ] ; then
            HD="Vmware"
        else
            HD=$( /sbin/hdparm -i /dev/sda | grep SerialNo | sed -r "s/(.)+SerialNo=//g" | sed -r  "s/[     ]+$//g" )
        fi
    else
        # Get NVMe HD
        HD_DEVICE_PATH=$( find /sys/devices/ -type d -name nvme?n? | head -n1 )
        HD_DEVICE=$( basename "${HD_DEVICE_PATH}" )
        HD=$( /usr/sbin/nvme id-ctrl /dev/${HD_DEVICE} | grep -Ei '^sn' | cut -d':' -f2 | awk '{print $1 }' )
    fi
    ID=$( echo -n "%$MAC-$HD%" | md5sum | cut -d" " -f1 )
    if [ -z "${ID}" ] ; then
        return 1
    fi

    echo -n "Setting hostname to '${ID}.ipreso.com'... "
    echo "${ID}.ipreso.com" > /etc/hostname
    hostname -F /etc/hostname || failure
    echo "Ok"

    echo -n "Configuring /etc/hosts... "
    sed -ri "s/^127\.0\.0\.1.*.$/127.0.0.1   ${ID}.ipreso.com localhost iBox/g" /etc/hosts || failure
    echo "Ok"

    return 0
}

function configureLogs
{
    # Use certificate/key with hostname for authentication on remote logs server
    echo -n "Configure certificate to use by Rsyslog "
    HOST=$( cat /etc/hostname )
    echo -n "(${HOST})... "
    sed -ri "s/%%HOSTNAME%%/${HOST}/g" /etc/rsyslog.d/50-iplayer-ssl.conf
    echo "Ok"

    # Send logs to the configured composer
    echo -n "Configure centralized logs server "
    COMPOSER=$( grep composer_host /etc/ipreso/ipreso.conf | \
                    cut -d' ' -f3 )
    if [ -z "${COMPOSER}" ] ; then
        COMPOSER=$( grep composer_ip /etc/ipreso/ipreso.conf | \
                        cut -d' ' -f3 )
    fi
    if [ -z "${COMPOSER}" ] ; then
        echo "Failed (empty)"
        return 0
    fi

    echo -n "(${COMPOSER})... "
    sed -ri "s/%%LOGS%%/${COMPOSER}/g" /etc/rsyslog.d/50-iplayer-ssl.conf
    echo "Ok"

    return 0
}

function configureRTCWakeup
{
    echo -n "Configure RTC Wakeup... "
    chown root:${RUNNING_USER} /sys/class/rtc/rtc0/wakealarm || failure
    chmod 664 /sys/class/rtc/rtc0/wakealarm || failure
    echo "Ok"

    return 0
}

function configureVolume
{
    echo -n "Configure Volume to maximum... "
    amixer sset 'Master' 255 &>/dev/null || failure
    #amixer sset 'Front' 255 &>/dev/null
    #amixer sset 'PCM' 255 &>/dev/null
    echo "Ok"

    return 0
}

function configureNetwork
{
    NET_DEVICE_PATH=$( find /sys/devices/ -type d -name net | grep -v virtual | head -n1 )
    NET_DEVICE=$( ls -1 "${NET_DEVICE_PATH}/" | head -n1 )

    echo -n "Configure network interface '${NET_DEVICE}'... "
    
    # Update /etc/network/interfaces with the detected interface
    sed -i '/^allow-hotplug/,/^$/d' /etc/network/interfaces         || failure
    echo "allow-hotplug ${NET_DEVICE}" >> /etc/network/interfaces   || failure
    echo "iface ${NET_DEVICE} inet dhcp" >> /etc/network/interfaces || failure
    
    # Configure /etc/ipreso/iplayer.conf with the detected interface
    sed -i "s/^license_net.*/license_net = ${NET_DEVICE}   # First detected interface/g" /etc/ipreso/iplayer.conf || failure
    echo "Ok"

    return 0
}

function configureHD
{
    # Set an existing hard disk for use of the license hash
    if [ -e "/dev/sda" ] ; then
        HD_DEVICE="/dev/sda"
    else
        # Get NVMe HD
        HD_DEVICE_PATH=$( find /sys/devices/ -type d -name nvme?n? | head -n1 )
        HD_DEVICE="/dev/$( basename ${HD_DEVICE_PATH} )"
    fi
    
    echo -n "Configure ${HD_DEVICE} as Hard Drive... "
    sed -ri "s|^license_hd.*$|license_hd = ${HD_DEVICE} # Hard Disk to generate license key|g" /etc/ipreso/iplayer.conf || failure
    echo "Ok"

    return 0
}

###############################################################################

configureHostname   || exit 1
configureLogs       || exit 1
configureRTCWakeup  || exit 1
configureVolume     || exit 1
configureNetwork    || exit 1
configureHD         || exit 1
