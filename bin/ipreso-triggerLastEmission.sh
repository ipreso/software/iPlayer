#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#==============================================================================
# This script will launch the last emission played by this host, if
# there is no programmation currently running.
#==============================================================================

export DISPLAY=:0.0

CONF="/etc/ipreso/ipreso.conf"
DEFAULTS="/etc/ipreso/iplayer.conf"

CURRENT_EMISSION=$( iZoneMgr -l | \
                        grep "Program: " | \
                        cut -d"'" -f2 )

if [ -n "${CURRENT_EMISSION}" ] ; then
    echo "Current iPlayer emission: '${CURRENT_EMISSION}'" | logger -p local4.info -t iLaunch
else
    echo "No current iPlayer emission." | logger -p local4.info -t iLaunch
    IZONEMGR=$( grep -E '^path_izonemgr' ${DEFAULTS} | \
                    cut -d'=' -f2- | \
                    sed -r -e 's/^[\t ]*//g' \
                            -e 's/[\t ]*$//g' \
                            -e 's/#.*$//g' )
    
    LAST_EMISSION=$( grep -E '^box_program' ${CONF} | \
                    cut -d'=' -f2- | \
                    sed -r -e 's/^[\t ]*//g' \
                            -e 's/[\t ]*$//g' )

    IZONEMGR_PID=$( pidof iZoneMgr )
    if [ -n "${IZONEMGR_PID}" ] ; then
        echo "Stopping current iZoneMgr (PID: ${IZONEMGR_PID})" | logger -p local4.info -t iLaunch

        # Do not call the "iZoneMgr --stop", because there should not be any iZone running...
        kill -9 ${IZONEMGR_PID}
    fi

    echo "Last emission was '${LAST_EMISSION}'. Launching it..." | logger -p local4.info -t iLaunch
    setsid ${IZONEMGR} -c "${DEFAULTS}" -p "${LAST_EMISSION}" 2>&1 | logger -p local4.info -t iLaunch < /dev/null &
fi
